all: md/cmdstan-optimisation-GT_nb.md md/cmdstan-optimisation-GT_nb_ase.md \
	md/cmdstan-optimisation-noGT_nb.md md/cmdstan-optimisation-noGT_nb_ase.md \
	md/cmdstan-vb-GT_nb-1e-2.md md/cmdstan-vb-GT_nb_ase-1e-2.md \
	md/cmdstan-vb-noGT_nb-1e-2.md md/cmdstan-vb-noGT_nb_ase-1e-2.md \
	md/cmdstan-vb-GT_nb-1e-3.md md/cmdstan-vb-GT_nb_ase-1e-3.md \
	md/cmdstan-vb-noGT_nb-1e-3.md md/cmdstan-vb-noGT_nb_ase-1e-3.md \
	md/cmdstan-vb-GT_nb-1e-4.md md/cmdstan-vb-GT_nb_ase-1e-4.md \
	md/cmdstan-vb-noGT_nb-1e-4.md md/cmdstan-vb-noGT_nb_ase-1e-4.md \
	md/full-vb-comparison.md \
	files

md/cmdstan-optimisation-%.md: cmdstan-optimisation.Rmd
	Rscript -e 'rmarkdown::render("$<", output_file="$@", params=list(model="$*", data="$(shell echo $* | sed -e 's/_nb.*//g')"))'

md/cmdstan-vb-%-1e-2.md: cmdstan-vb.Rmd
	Rscript -e 'rmarkdown::render("$<", output_file="$@", params=list(model="$*", data="$(shell echo $* | sed -e 's/_nb.*//g')", tol_rel_obj = 1e-2))'

md/cmdstan-vb-%-1e-3.md: cmdstan-vb.Rmd
	Rscript -e 'rmarkdown::render("$<", output_file="$@", params=list(model="$*", data="$(shell echo $* | sed -e 's/_nb.*//g')", tol_rel_obj = 1e-3))'

md/cmdstan-vb-%-1e-4.md: cmdstan-vb.Rmd
	Rscript -e 'rmarkdown::render("$<", output_file="$@", params=list(model="$*", data="$(shell echo $* | sed -e 's/_nb.*//g')", tol_rel_obj = 1e-4))'

md/%.md: %.Rmd
	Rscript -e 'rmarkdown::render("$<", output_file="$@")'

md/md:
	mkdir -p md/md

INFILES=$(shell find md -maxdepth 1 -name *_files)
OUTFILES=$(patsubst md/%,md/md/%,$(INFILES))

files: $(OUTFILES)

md/md/%_files: md/%_files | md/md/
	rm -rf $@ && mv $< $@

md/md/:
	mkdir $@

# mv md/full-vb-comparison_files/figure-markdown_strict/* md/md/full-vb-comparison_files/figure-markdown_strict/ && rm -r md/full-vb-comparison_files/
