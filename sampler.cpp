#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

template <typename T>
inline void rcout(T x) {
    Rcpp::Rcout << x << std::endl;
}


arma::vec g(double bj, arma::ivec G) {
    arma::vec g = arma::zeros(G.size());
    for (int i = 0; i < G.size(); i++) {
        if (abs(G(i)) == 1) {
            g(i) = log1p(exp(bj)) - log(2);
        } else if (G(i) == 2) {
            g(i) = bj;
        }
    }
    return g;
}

arma::vec calc_mu(
        arma::mat cov, arma::vec gamma, arma::vec g
    ) {
    return g + (cov * gamma);
}

arma::vec calc_mu(
        arma::mat cov, arma::vec gamma, double bj, arma::ivec G
    ) {
    return calc_mu(cov, gamma, g(bj, G));
}

double phi_mh(
        arma::vec counts,
        double phi1,
        double phi0,
        arma::vec mu,
        double a,
        double b
    ) {

    // gamma prior
    double mh = (log(phi1) - log(phi0) * (a - 1)) - (b * (phi1 - phi0));
    // double mh = 0;
    // // nb stuff
    mh -= counts.size() * (lgamma(phi1) - lgamma(phi0));
    mh -= counts.size() * (
        (log(phi1) * phi1)
        - (log(phi0) * phi0)
    );
    arma::vec em = exp(mu);
    for (int i = 0; i < counts.size(); i++) {
        mh += lgamma(phi1 + counts(i))
            - lgamma(phi0 + counts(i));
        mh += (counts(i) + phi0) * log(em(i) + phi0)
            - (counts(i) + phi1) * log(em(i) + phi1);
    }
    return mh;
}

double mu_mh(
        arma::vec counts,
        arma::vec mu1,
        arma::vec mu0,
        double phi
    ) {
    double mh = 0; 
    double lp = log(phi);
    for (int i = 0; i < counts.size(); i++) {
        mh += (mu1(i) - mu0(i)) * counts(i);
        mh += (counts(i) + phi) * (
            log(phi + exp(mu1(i))) / (log(phi + exp(mu0(i))))
        );
    }
    return mh;
}

double logsumexp(arma::vec x) {
    return(log(sum(exp(x - max(x)))) + max(x));
}

// mixture prior (bj)
double normal_mix_prior(
        double bj1,
        double bj0,
        arma::vec means,
        arma::vec sds,
        arma::vec log_props
    ) {
    arma::vec mh = arma::zeros(means.size());
    for (int k = 0; k < means.size(); k++) {
        mh(k) += ((pow(bj0 - means(k), 2) - pow(bj1 - means(k), 2)) / pow(sds(k), 2)) + log_props(k);
    }
    return logsumexp(mh);
}

// normal prior (beta0)
double normal_mh(
        double beta1, double beta0, double mean, double sd
    ) {
    return (pow(beta0 - mean, 2) - pow(beta1 - mean, 2)) / pow(sd, 2);
}
// cauchy prior (beta1)
double cauchy_mh(
        double beta1, double beta0, double location, double scale
    ) {
    return log(
        (1 + pow((beta0 - location) / scale, 2))
        / (1 + pow((beta1 - location) / scale, 2))
    );
}

double bj_mh(
        arma::vec counts, 
        arma::ivec G,
        arma::mat cov,
        double bj1,
        double bj0,
        arma::vec betas,
        double phi,
        arma::vec means,
        arma::vec sds,
        arma::vec log_props
    ) {
    arma::vec mu1 = calc_mu(cov, betas, bj1, G);
    arma::vec mu0 = calc_mu(cov, betas, bj0, G);
    double mh = mu_mh(counts, mu1, mu0, phi);
    mh += normal_mix_prior(
        bj1,
        bj0,
        means,
        sds,
        log_props
    );
    return mh;
}

double beta_mh_cauchy(
        arma::vec counts, 
        arma::ivec G,
        arma::mat cov,
        arma::vec beta1,
        arma::vec beta0,
        double bj,
        double phi,
        double location,
        double scale

    ) {
    double mh = cauchy_mh(
        beta1(1), beta0(1), location, scale
    );
    arma::vec mu1 = calc_mu(cov, beta1, bj, G);
    arma::vec mu0 = calc_mu(cov, beta0, bj, G);
    mh += mu_mh(counts, mu1, mu0, phi);
    return mh;
}

double beta_mh_normal(
        arma::vec counts, 
        arma::ivec G,
        arma::mat cov,
        arma::vec beta1,
        arma::vec beta0,
        double bj,
        double phi,
        double mean,
        double sd
    ) {
    double mh = normal_mh(
        beta1(0), beta0(0), mean, sd
    );
    arma::vec mu1 = calc_mu(cov, beta1, bj, G);
    arma::vec mu0 = calc_mu(cov, beta0, bj, G);
    mh += mu_mh(counts, mu1, mu0, phi);
    return mh;
}

// [[Rcpp::export]]
Rcpp::List sampler(
        arma::vec counts,
        arma::mat cov,
        arma::ivec G,
        arma::vec means,
        arma::vec sds,
        arma::vec log_props,
        arma::vec betas,
        double bj,
        double phi,
        double normal_mean,
        double normal_sd,
        double cauchy_location,
        double cauchy_scale,
        int n,
        int burn,
        int thin
    ) {
    int samples = (n - burn) / thin;
    arma::mat draws = arma::zeros(samples, 4);
    // double phi = 1;
    double phi_prop = 1;
    // double bj = 0;
    double bj_prop = 0;
    int index = 0;
    // arma::vec betas = arma::zeros(2);
    arma::vec mu = arma::zeros(G.size());
    arma::vec betas_prop = arma::zeros(2);
    arma::vec lsds = arma::zeros(4);
    arma::vec ars = arma::zeros(4);
    arma::ivec indicator = arma::ivec(4);
    indicator.zeros();
    double mh = 0;
    for (int i = 0; i < n; i++) {
        Rcpp::checkUserInterrupt();
        phi_prop = exp(R::rnorm(log(phi), exp(lsds(0))));
        mu = calc_mu(cov, betas, bj, G);
        mh  = phi_mh(counts, phi_prop, phi, mu, 1, 0.01);

        if (mh > log(R::runif(0, 1))) {
            ars(0)++;
            phi = phi_prop;
        }

        bj_prop = R::rnorm(bj, exp(lsds(1)));
        mh = bj_mh(
            counts, G, cov, bj_prop, bj, betas, phi,
            means, sds, log_props
        );
        if (mh > log(R::runif(0, 1))) {
            ars(1)++;
            // bj = bj_prop;
        }

        betas_prop = betas;
        betas_prop(0) = R::rnorm(betas(0), exp(lsds(2)));
        mh = beta_mh_normal(
            counts, 
            G,
            cov,
            betas_prop,
            betas,
            bj,
            phi,
            normal_mean,
            normal_sd
        );
        if (mh > log(R::runif(0, 1))) {
            ars(2)++;
            // betas(0) = betas_prop(0);
        }
        betas_prop(1) = R::rnorm(betas(1), exp(lsds(3)));
        mh = beta_mh_cauchy(
            counts, 
            G,
            cov,
            betas_prop,
            betas,
            bj,
            phi,
            cauchy_location,
            cauchy_scale
        );
        if (mh > log(R::runif(0, 1))) {
            ars(3)++;
            // betas(1) = betas_prop(1);
        }
        
        if (i < burn && i % 50 == 0) {
            for (int p = 0; p < draws.n_cols; p++) {
                lsds(p) += ((ars(p) / 50) > 0.44 ? 1:-1) * 0.1;
            }
            ars.zeros();
        }
        if ((i >= burn)) {
            if ((i % thin) == 0) {
                draws(index, 0) = bj;
                draws(index, 1) = betas(0);
                draws(index, 2) = betas(1);
                draws(index, 3) = phi;
                index++;
            }
        }
    }
    Rcpp::List out = Rcpp::List::create(
        Rcpp::Named("draws") = draws,
        Rcpp::Named("ars") = ars,
        Rcpp::Named("lsds") = lsds
    );
    return out;
}
