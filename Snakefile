from re import sub

models = [
    "GT_nb",
    "GT_nb_ase",
    "GT_nb_ase_refbias",
    # "GT2T_nb",
    # "GT2T_nb_ase",
    # "GT2T_nb_ase_refbias",
    "noGT_nb",
    "noGT_nb_ase",
    "noGT_nb_ase_refbias",
    "noGT2T_nb_ase",
    "noGT2T_nb_ase_refbias"
]

rule all: 
    input:
        "md/full-vb-comparison.md",
        "md/full-map-comparison.md",
        "md/full-pf-comparison.md",
        # "md/GT2T-simulation.md",
        expand(
            "md/cmdstan-optimisation_{model}.md",
            model = models
        ),
        expand(
            "md/pathfinder_{model}.md",
            model = models
        ),
        # expand(
        #     "md/cmdstan-map_{model}.md",
        #     model = models
        # ),
        expand(
            "md/cmdstan-vb_{model}_{tols}.md",
            model = models,
            tols = [1e-2, 1e-3]
        )

rule optimisation:
    input:
        script = "cmdstan-optimisation.Rmd",
        functions = "functions.R",
        optmodelfile = "stan/optimised/{model}.stan",
        modelfile = "stan/original/{model}.stan"
    output:
        "md/cmdstan-optimisation_{model}.md"
    shell:
        """
        Rscript -e 'rmarkdown::render(
            \"{input.script}\", \
            output_file = \"{output}\", \
            params = list(model = \"{wildcards.model}\") \
        )'
        mkdir -p md/md/
        rm -rf md/$(echo {output} | sed -e 's/\.md/_files/')
        mv $(echo {output} | sed -e 's/\.md/_files/') md/md/
        """


rule pathfinder:
    input:
        script = "pathfinder.Rmd",
        functions = "functions.R",
        optmodelfile = "stan/optimised/{model}.stan",
        modelfile = "stan/original/{model}.stan"
    output:
        "md/pathfinder_{model}.md"
    shell:
        """
        Rscript -e 'rmarkdown::render(
            \"{input.script}\", \
            output_file = \"{output}\", \
            params = list(model = \"{wildcards.model}\") \
        )'
        mkdir -p md/md/
        rm -rf md/$(echo {output} | sed -e 's/\.md/_files/')
        mv $(echo {output} | sed -e 's/\.md/_files/') md/md/
        """


rule map:
    input:
        script = "cmdstan-map.Rmd",
        functions = "functions.R",
        modelfile = "stan/map/{model}.stan"
    output:
        "md/cmdstan-map_{model}.md"
    shell:
        """
        Rscript -e 'rmarkdown::render(
            \"{input.script}\", \
            output_file = \"{output}\", \
            params = list(model = \"{wildcards.model}\") \
        )'
        mkdir -p md/md/
        rm -rf md/$(echo {output} | sed -e 's/\.md/_files/')
        mv $(echo {output} | sed -e 's/\.md/_files/') md/md/
        """

rule vb:
    input:
        "cmdstan-vb.Rmd"
    output:
        "md/cmdstan-vb_{model}_{tol}.md"
    params:
        data=lambda wildcards: sub(r'_nb.*', "", wildcards.model)
    shell:
        """
        Rscript -e 'rmarkdown::render( \
            \"{input}\", \
            output_file = \"{output}\", \
            params = list( \
                model = \"{wildcards.model}\", \
                tol_rel_obj = {wildcards.tol} \
            ) \
        )'
        mkdir -p md/md/
        rm -rf md/$(echo {output} | sed -e 's/\.md/_files/')
        mv $(echo {output} | sed -e 's/\.md/_files/') md/md/
        """

rule rmd:
    input:
        "{file}.Rmd"
    output:
        "md/{file}.md"
    shell:
        """
        Rscript -e "rmarkdown::render('{input}', output_file='{output}')"
        mkdir -p md/md/
        rm -rf md/$(echo {output} | sed -e 's/\.md/_files/')
        mv $(echo {output} | sed -e 's/\.md/_files/') md/md/
        """
