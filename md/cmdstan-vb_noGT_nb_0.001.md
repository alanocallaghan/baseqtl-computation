VB with noGT\_nb, tolerance: 0.001
==================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

![](md/cmdstan-vb_noGT_nb_0.001_files/figure-markdown_strict/plot-draws-1.png)![](md/cmdstan-vb_noGT_nb_0.001_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_noGT_nb_0.001_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_noGT_nb_0.001_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139770486789952</td>
<td style="text-align: right;">0.393677</td>
<td style="text-align: right;">0.3718490</td>
<td style="text-align: right;">0.0218280</td>
<td style="text-align: right;">3478784</td>
<td style="text-align: right;">2796032</td>
<td style="text-align: right;">16256</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139770486789952</td>
<td style="text-align: right;">0.005803</td>
<td style="text-align: right;">0.0048264</td>
<td style="text-align: right;">0.0009766</td>
<td style="text-align: right;">130048</td>
<td style="text-align: right;">16256</td>
<td style="text-align: right;">16256</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140112259266368</td>
<td style="text-align: right;">0.8519200</td>
<td style="text-align: right;">0.8223960</td>
<td style="text-align: right;">0.0295243</td>
<td style="text-align: right;">4312314</td>
<td style="text-align: right;">3465972</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">21401</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140112259266368</td>
<td style="text-align: right;">0.0136524</td>
<td style="text-align: right;">0.0120917</td>
<td style="text-align: right;">0.0015606</td>
<td style="text-align: right;">161208</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">21401</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139896965039936</td>
<td style="text-align: right;">0.8705530</td>
<td style="text-align: right;">0.839244</td>
<td style="text-align: right;">0.0313094</td>
<td style="text-align: right;">4312314</td>
<td style="text-align: right;">3465972</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">21401</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139896965039936</td>
<td style="text-align: right;">0.0145918</td>
<td style="text-align: right;">0.012945</td>
<td style="text-align: right;">0.0016467</td>
<td style="text-align: right;">161208</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">20151</td>
<td style="text-align: right;">21401</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error and reference bias correction with random intercept model. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Allows for any mixture of gaussians for bj prior (eQTL effect).
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      matrix[N,1+K] cov; 
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      vector[G] log_pNB = log(pNB);
      vector[G] abs_gNB = fabs(gNB);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower=-20,upper=20> bj; // log fold change ASE
      real<lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
    }

    model {
      int pos; // to advance through NB terms (1-G)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
      // local variables and transformed parameters of no interest
      pos = 1;  // to advance on NB terms
      
      profile("likelihood") {
        ebj = exp(bj);
        real l1pebj = log1p(ebj) - log(2);
        lmu1 = cov[, 2:cols(cov)] * betas;
        for(i in 1:N) { // lmu for each individual default to GT=0
          
          for (r in pos:(pos+sNB[i]-1)) { // genotype level, Gi=g
            // print("gNB = ", gNB[r]," r = ", r);
            lmu = lmu1[i];
            lmu = abs_gNB[r] == 1 ? lmu + l1pebj : lmu;
            lmu = gNB[r] == 2 ? lmu + bj : lmu;

            ltmp[r] = neg_binomial_2_log_lpmf(Y[i] | lmu, phi) + log_pNB[r];
          }
          target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          pos += sNB[i];
        }
      }
    }
