Optimising noGT2T\_nb\_ase
==========================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_noGT2T_nb_ase_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">ba</td>
<td style="text-align: right;">-0.0880026</td>
<td style="text-align: right;">-0.0864634</td>
<td style="text-align: right;">0.0309339</td>
<td style="text-align: right;">0.0289175</td>
<td style="text-align: right;">-0.1412998</td>
<td style="text-align: right;">-0.0404995</td>
<td style="text-align: right;">1.0000128</td>
<td style="text-align: right;">23072.42</td>
<td style="text-align: right;">13458.44</td>
</tr>
<tr class="even">
<td style="text-align: left;">bd</td>
<td style="text-align: right;">0.0502691</td>
<td style="text-align: right;">0.0498892</td>
<td style="text-align: right;">0.0271666</td>
<td style="text-align: right;">0.0269112</td>
<td style="text-align: right;">0.0064085</td>
<td style="text-align: right;">0.0949481</td>
<td style="text-align: right;">1.0004428</td>
<td style="text-align: right;">25750.73</td>
<td style="text-align: right;">16540.45</td>
</tr>
<tr class="odd">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">3.8086505</td>
<td style="text-align: right;">3.7708850</td>
<td style="text-align: right;">0.5842258</td>
<td style="text-align: right;">0.5724615</td>
<td style="text-align: right;">2.9109860</td>
<td style="text-align: right;">4.8413980</td>
<td style="text-align: right;">1.0000191</td>
<td style="text-align: right;">24824.37</td>
<td style="text-align: right;">15855.47</td>
</tr>
<tr class="even">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">55.8135868</td>
<td style="text-align: right;">55.5439000</td>
<td style="text-align: right;">5.2543752</td>
<td style="text-align: right;">5.2766475</td>
<td style="text-align: right;">47.6508900</td>
<td style="text-align: right;">64.8548000</td>
<td style="text-align: right;">1.0001917</td>
<td style="text-align: right;">26541.83</td>
<td style="text-align: right;">17001.40</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bp</td>
<td style="text-align: right;">-0.0377334</td>
<td style="text-align: right;">-0.0371416</td>
<td style="text-align: right;">0.0437526</td>
<td style="text-align: right;">0.0427249</td>
<td style="text-align: right;">-0.1099059</td>
<td style="text-align: right;">0.0330666</td>
<td style="text-align: right;">0.9999417</td>
<td style="text-align: right;">22160.57</td>
<td style="text-align: right;">13662.60</td>
</tr>
<tr class="even">
<td style="text-align: left;">bn</td>
<td style="text-align: right;">-0.1382717</td>
<td style="text-align: right;">-0.1375910</td>
<td style="text-align: right;">0.0384131</td>
<td style="text-align: right;">0.0377974</td>
<td style="text-align: right;">-0.2020158</td>
<td style="text-align: right;">-0.0761799</td>
<td style="text-align: right;">1.0002340</td>
<td style="text-align: right;">25362.69</td>
<td style="text-align: right;">15231.62</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">ba</td>
<td style="text-align: right;">-0.0857395</td>
<td style="text-align: right;">-0.0843662</td>
<td style="text-align: right;">0.0308793</td>
<td style="text-align: right;">0.0292123</td>
<td style="text-align: right;">-0.1386487</td>
<td style="text-align: right;">-0.0376632</td>
<td style="text-align: right;">1.0006647</td>
<td style="text-align: right;">26512.34</td>
<td style="text-align: right;">12815.58</td>
</tr>
<tr class="even">
<td style="text-align: left;">bd</td>
<td style="text-align: right;">0.0499433</td>
<td style="text-align: right;">0.0497665</td>
<td style="text-align: right;">0.0272270</td>
<td style="text-align: right;">0.0271020</td>
<td style="text-align: right;">0.0052592</td>
<td style="text-align: right;">0.0947244</td>
<td style="text-align: right;">1.0001108</td>
<td style="text-align: right;">29302.17</td>
<td style="text-align: right;">15620.82</td>
</tr>
<tr class="odd">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">3.5822437</td>
<td style="text-align: right;">3.5539150</td>
<td style="text-align: right;">0.5108474</td>
<td style="text-align: right;">0.5087320</td>
<td style="text-align: right;">2.7856790</td>
<td style="text-align: right;">4.4645890</td>
<td style="text-align: right;">0.9999741</td>
<td style="text-align: right;">27041.17</td>
<td style="text-align: right;">14446.92</td>
</tr>
<tr class="even">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">55.8555293</td>
<td style="text-align: right;">55.6281500</td>
<td style="text-align: right;">5.3898984</td>
<td style="text-align: right;">5.2921407</td>
<td style="text-align: right;">47.4066500</td>
<td style="text-align: right;">65.0842400</td>
<td style="text-align: right;">0.9999748</td>
<td style="text-align: right;">28277.92</td>
<td style="text-align: right;">15446.93</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bp</td>
<td style="text-align: right;">-0.0357963</td>
<td style="text-align: right;">-0.0351960</td>
<td style="text-align: right;">0.0440205</td>
<td style="text-align: right;">0.0435624</td>
<td style="text-align: right;">-0.1092221</td>
<td style="text-align: right;">0.0352126</td>
<td style="text-align: right;">1.0003821</td>
<td style="text-align: right;">27554.52</td>
<td style="text-align: right;">15071.55</td>
</tr>
<tr class="even">
<td style="text-align: left;">bn</td>
<td style="text-align: right;">-0.1356828</td>
<td style="text-align: right;">-0.1350250</td>
<td style="text-align: right;">0.0381035</td>
<td style="text-align: right;">0.0376136</td>
<td style="text-align: right;">-0.1994273</td>
<td style="text-align: right;">-0.0743813</td>
<td style="text-align: right;">1.0002492</td>
<td style="text-align: right;">26524.05</td>
<td style="text-align: right;">14272.26</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140038017210176</td>
<td style="text-align: right;">113.363000</td>
<td style="text-align: right;">103.344000</td>
<td style="text-align: right;">10.0194000</td>
<td style="text-align: right;">443654427</td>
<td style="text-align: right;">72907049</td>
<td style="text-align: right;">62311</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0018193</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140038017210176</td>
<td style="text-align: right;">0.209289</td>
<td style="text-align: right;">0.161920</td>
<td style="text-align: right;">0.0473688</td>
<td style="text-align: right;">1121760</td>
<td style="text-align: right;">124640</td>
<td style="text-align: right;">62311</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000034</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139815961712448</td>
<td style="text-align: right;">113.641000</td>
<td style="text-align: right;">103.670000</td>
<td style="text-align: right;">9.9709700</td>
<td style="text-align: right;">434312965</td>
<td style="text-align: right;">71372006</td>
<td style="text-align: right;">60999</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0018630</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139815961712448</td>
<td style="text-align: right;">0.241655</td>
<td style="text-align: right;">0.191776</td>
<td style="text-align: right;">0.0498795</td>
<td style="text-align: right;">1098144</td>
<td style="text-align: right;">122016</td>
<td style="text-align: right;">60999</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000040</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139959878596416</td>
<td style="text-align: right;">111.339000</td>
<td style="text-align: right;">101.351000</td>
<td style="text-align: right;">9.9874300</td>
<td style="text-align: right;">427627284</td>
<td style="text-align: right;">70274079</td>
<td style="text-align: right;">60060</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0018538</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139959878596416</td>
<td style="text-align: right;">0.264406</td>
<td style="text-align: right;">0.233633</td>
<td style="text-align: right;">0.0307728</td>
<td style="text-align: right;">1081278</td>
<td style="text-align: right;">120142</td>
<td style="text-align: right;">60060</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000044</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140300614141760</td>
<td style="text-align: right;">113.663000</td>
<td style="text-align: right;">103.799000</td>
<td style="text-align: right;">9.8646200</td>
<td style="text-align: right;">435779726</td>
<td style="text-align: right;">71614438</td>
<td style="text-align: right;">61205</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0018571</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140300614141760</td>
<td style="text-align: right;">0.196372</td>
<td style="text-align: right;">0.159877</td>
<td style="text-align: right;">0.0364954</td>
<td style="text-align: right;">1101924</td>
<td style="text-align: right;">122436</td>
<td style="text-align: right;">61205</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000032</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140452386629440</td>
<td style="text-align: right;">137.418000</td>
<td style="text-align: right;">125.856000</td>
<td style="text-align: right;">11.5613000</td>
<td style="text-align: right;">567302720</td>
<td style="text-align: right;">103451400</td>
<td style="text-align: right;">88420</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0015542</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140452386629440</td>
<td style="text-align: right;">0.263910</td>
<td style="text-align: right;">0.217085</td>
<td style="text-align: right;">0.0468258</td>
<td style="text-align: right;">1591560</td>
<td style="text-align: right;">176840</td>
<td style="text-align: right;">88420</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000030</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140286478329664</td>
<td style="text-align: right;">118.859000</td>
<td style="text-align: right;">108.934000</td>
<td style="text-align: right;">9.9255500</td>
<td style="text-align: right;">492986192</td>
<td style="text-align: right;">89899290</td>
<td style="text-align: right;">76837</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0015469</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140286478329664</td>
<td style="text-align: right;">0.208903</td>
<td style="text-align: right;">0.149288</td>
<td style="text-align: right;">0.0596151</td>
<td style="text-align: right;">1383066</td>
<td style="text-align: right;">153674</td>
<td style="text-align: right;">76837</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000027</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139951751591744</td>
<td style="text-align: right;">146.156000</td>
<td style="text-align: right;">134.009000</td>
<td style="text-align: right;">12.1464000</td>
<td style="text-align: right;">611945248</td>
<td style="text-align: right;">111592260</td>
<td style="text-align: right;">95378</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0015324</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139951751591744</td>
<td style="text-align: right;">0.277307</td>
<td style="text-align: right;">0.209395</td>
<td style="text-align: right;">0.0679117</td>
<td style="text-align: right;">1716804</td>
<td style="text-align: right;">190756</td>
<td style="text-align: right;">95378</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000029</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139975616239424</td>
<td style="text-align: right;">146.456000</td>
<td style="text-align: right;">134.018000</td>
<td style="text-align: right;">12.4382000</td>
<td style="text-align: right;">585049376</td>
<td style="text-align: right;">106687620</td>
<td style="text-align: right;">91186</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0016061</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139975616239424</td>
<td style="text-align: right;">0.308393</td>
<td style="text-align: right;">0.265679</td>
<td style="text-align: right;">0.0427145</td>
<td style="text-align: right;">1641348</td>
<td style="text-align: right;">182372</td>
<td style="text-align: right;">91186</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000034</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0018554</td>
<td style="text-align: right;">113.502000</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000037</td>
<td style="text-align: right;">0.225472</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0015505</td>
<td style="text-align: right;">141.7870000</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000029</td>
<td style="text-align: right;">0.2706085</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error, allowing for interaction term normal psoriasis, with or without covariates and ref bias correction version 2. Updated likelihood and mixed prior.
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) and ai0 
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      int I[N]; //indicator for skin all samples: 1=pso, 0=normal
      
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
    }


    parameters {
      real anorm; // mean expression normal tissue
      real apso; // mean expression pso tissues
      real ba; // log average-fold change ASE
      real bd; // log difference-fold change ASE
      real<lower=0> phi; //overdipersion param for neg binom
      real<lower=0> theta; //the overdispersion parameter for beta binomial
      vector[K-1] betas; // regression parameters 
      
    }

    transformed parameters {
      real bp; // parameter of interest for psoriasis
      real bn; // parameter of interest for normal skin
        
      bp = ba + bd;
      bn = ba - bd;
        
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      vector[G] lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood

      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      real esum; // reduce computation inverse logit (bp or bn)
      vector[k] lpsa; // help for mixed gaussians for ba
      vector[k] lpsd; // help for mixed gaussians for bd
      
      vector[K] betasN; //regression parameters for normal skin
      vector[K] betasP; //regression parameters for pso skin
      vector[N] lmuN; //help linear pred normal skin
      vector[N] lmuP; //help linear pred pso skin
      
      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        anorm ~ normal(6,4); // mean expression normal skin, stan normal is mean and sd
        apso ~ normal(6,4); // mean expression pso skin
        for(i in 1:(K-1)) {
          betas[i] ~ cauchy(0,2.5);//prior for the covariates slopes following Gelman 2008
        }

        // mixture of gaussians for ba and bd:
        for(i in 1:k){
          lpsa[i] = normal_lpdf(ba | aveP[i], sdP[i]) + mixP[i];
          lpsd[i] = normal_lpdf(bd | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lpsa);
        target += log_sum_exp(lpsd);
      }
      // transformed parameters of no interest
      pos = 1; // to advance on NB terms
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0,Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */

      profile("likelihood") {
        betasN=append_row(anorm, betas); //betas for normal inds
        betasP=append_row(apso, betas); // betas for pso inds
        lmuN=cov[,2:cols(cov)]*betasN; // will be used for normal inds (based on indicator)
        lmuP=cov[,2:cols(cov)]*betasP; // for pso inds

        
        for(i in 1:N){ // lmu for each individual
          // check skin first
          if (I[i] == 1) { // psoriasis

            for (r in pos:(pos+sNB[i]-1)) { // then genotype
              
              lmu[r] = lmuP[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(1+exp(bp))-log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bp : lmu[r];

              ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu[r]), phi) + log(pNB[r]);

              if (ASEi[i,1] == 1) {  // ASE info
              
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g

                  esum = inv_logit(bp);

                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);
                target +=  log_sum_exp(ltmp[r] , sAse );
              }
          
            }
            if (ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
            
          } else {
            for (r in pos:(pos+sNB[i]-1)){ //normal skin
        
              lmu[r] = lmuN[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(1+exp(bn))-log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bn : lmu[r];
              ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu[r]), phi) + log(pNB[r]);

              if (ASEi[i,1] == 1) {  // ASE info
                for (x in 1:h2g[r]){  // look at the haps compatibles with Gi=g
                  esum = inv_logit(bn);
                  
                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);   
                target +=  log_sum_exp(ltmp[r] , sAse );       
              }
            }
            if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }
            pos += sNB[i];  
          }       
        }
      }
    }
        

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error, allowing for interaction term normal psoriasis, with or without covariates and ref bias correction version 2. Updated likelihood and mixed prior.
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) and ai0 
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      int I[N]; //indicator for skin all samples: 1=pso, 0=normal
      
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
      vector[G] log_pNB = log(pNB);
      vector[L] log_pH = log(pH);
    }

    parameters {
      real anorm; // mean expression normal tissue
      real apso; // mean expression pso tissues
      real <lower=-10, upper=10> ba; // log average-fold change ASE
      real <lower=-10, upper=10> bd; // log difference-fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector[K-1] betas; // regression parameters  
    }

    transformed parameters {
      real <lower=-20, upper=20> bp; // parameter of interest for psoriasis
      real <lower=-20, upper=20> bn; // parameter of interest for normal skin
        
      bp = ba + bd;
      bn = ba - bd;
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      vector[G] lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood

      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      real esum; // reduce computation inverse logit (bp or bn)
      vector[k] lpsa; // help for mixed gaussians for ba
      vector[k] lpsd; // help for mixed gaussians for bd
      
      vector[K] betasN; //regression parameters for normal skin
      vector[K] betasP; //regression parameters for pso skin
      vector[N] lmuN; //help linear pred normal skin
      vector[N] lmuP; //help linear pred pso skin
      
      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        anorm ~ normal(6,4); // mean expression normal skin, stan normal is mean and sd
        apso ~ normal(6,4); // mean expression pso skin
        for(i in 1:(K-1)) {
          betas[i] ~ cauchy(0,2.5);//prior for the covariates slopes following Gelman 2008
        }

        // mixture of gaussians for ba and bd:
        for(i in 1:k){
          lpsa[i] = normal_lpdf(ba | aveP[i], sdP[i]) + mixP[i];
          lpsd[i] = normal_lpdf(bd | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lpsa);
        target += log_sum_exp(lpsd);
      }
      // transformed parameters of no interest
      pos = 1; // to advance on NB terms
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0,Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */

      profile("likelihood") {
        betasN=append_row(anorm, betas); //betas for normal inds
        betasP=append_row(apso, betas); // betas for pso inds
        lmuN = cov[, 2:cols(cov)] * betasN; // will be used for normal inds (based on indicator)
        lmuP = cov[, 2:cols(cov)] * betasP; // for pso inds

        
        for(i in 1:N){ // lmu for each individual
          // check skin first
          if (I[i] == 1) { // psoriasis

            for (r in pos:(pos+sNB[i]-1)) { // then genotype
              
              lmu[r] = lmuP[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(exp(bp)) - log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bp : lmu[r];

              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
              
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g

                  esum = inv_logit(bp);

                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);
                target +=  log_sum_exp(ltmp[r] , sAse );
              }
          
            }
            if (ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
            
          } else {
            for (r in pos:(pos+sNB[i]-1)){ //normal skin
        
              lmu[r] = lmuN[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(exp(bn)) - log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bn : lmu[r];
              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                  esum = inv_logit(bn);
                  
                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);   
                target +=  log_sum_exp(ltmp[r] , sAse );       
              }
            }
            if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }
            pos += sNB[i];  
          }       
        }
      }
    }
        
