Optimising GT\_nb
=================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_GT_nb_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table>
<colgroup>
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7417169</td>
<td style="text-align: right;">6.7414600</td>
<td style="text-align: right;">0.0382523</td>
<td style="text-align: right;">0.0381621</td>
<td style="text-align: right;">6.6791490</td>
<td style="text-align: right;">6.8050115</td>
<td style="text-align: right;">1.000010</td>
<td style="text-align: right;">19475.89</td>
<td style="text-align: right;">15568.68</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2136945</td>
<td style="text-align: right;">0.2134815</td>
<td style="text-align: right;">0.0393520</td>
<td style="text-align: right;">0.0387900</td>
<td style="text-align: right;">0.1486732</td>
<td style="text-align: right;">0.2791450</td>
<td style="text-align: right;">1.000265</td>
<td style="text-align: right;">20009.98</td>
<td style="text-align: right;">14148.34</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0005253</td>
<td style="text-align: right;">0.0002474</td>
<td style="text-align: right;">0.0304601</td>
<td style="text-align: right;">0.0298342</td>
<td style="text-align: right;">-0.0484163</td>
<td style="text-align: right;">0.0497734</td>
<td style="text-align: right;">1.000144</td>
<td style="text-align: right;">21381.86</td>
<td style="text-align: right;">15533.40</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.7264397</td>
<td style="text-align: right;">8.6599900</td>
<td style="text-align: right;">1.3231053</td>
<td style="text-align: right;">1.3176459</td>
<td style="text-align: right;">6.6803885</td>
<td style="text-align: right;">11.0105100</td>
<td style="text-align: right;">1.000123</td>
<td style="text-align: right;">18683.21</td>
<td style="text-align: right;">14588.73</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7415844</td>
<td style="text-align: right;">6.7409750</td>
<td style="text-align: right;">0.0386882</td>
<td style="text-align: right;">0.0386884</td>
<td style="text-align: right;">6.6784495</td>
<td style="text-align: right;">6.8061005</td>
<td style="text-align: right;">0.9999985</td>
<td style="text-align: right;">18104.72</td>
<td style="text-align: right;">14544.36</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2132041</td>
<td style="text-align: right;">0.2130380</td>
<td style="text-align: right;">0.0393952</td>
<td style="text-align: right;">0.0392756</td>
<td style="text-align: right;">0.1492286</td>
<td style="text-align: right;">0.2786925</td>
<td style="text-align: right;">0.9999997</td>
<td style="text-align: right;">19635.60</td>
<td style="text-align: right;">13770.44</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0001346</td>
<td style="text-align: right;">-0.0001415</td>
<td style="text-align: right;">0.0306468</td>
<td style="text-align: right;">0.0297732</td>
<td style="text-align: right;">-0.0492228</td>
<td style="text-align: right;">0.0499137</td>
<td style="text-align: right;">1.0000517</td>
<td style="text-align: right;">20567.58</td>
<td style="text-align: right;">15343.07</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.7267620</td>
<td style="text-align: right;">8.6519050</td>
<td style="text-align: right;">1.3151283</td>
<td style="text-align: right;">1.2827010</td>
<td style="text-align: right;">6.6875080</td>
<td style="text-align: right;">11.0073350</td>
<td style="text-align: right;">1.0000808</td>
<td style="text-align: right;">20863.69</td>
<td style="text-align: right;">15522.58</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140210279491392</td>
<td style="text-align: right;">2.9788200</td>
<td style="text-align: right;">2.7720700</td>
<td style="text-align: right;">0.2067520</td>
<td style="text-align: right;">15695718</td>
<td style="text-align: right;">5037106</td>
<td style="text-align: right;">58566</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">5.09e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140210279491392</td>
<td style="text-align: right;">0.0564358</td>
<td style="text-align: right;">0.0447507</td>
<td style="text-align: right;">0.0116851</td>
<td style="text-align: right;">468568</td>
<td style="text-align: right;">58571</td>
<td style="text-align: right;">58566</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.00e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139926953510720</td>
<td style="text-align: right;">3.1848700</td>
<td style="text-align: right;">2.9794700</td>
<td style="text-align: right;">0.2053990</td>
<td style="text-align: right;">16057250</td>
<td style="text-align: right;">5153120</td>
<td style="text-align: right;">59915</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">5.32e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139926953510720</td>
<td style="text-align: right;">0.0764338</td>
<td style="text-align: right;">0.0657332</td>
<td style="text-align: right;">0.0107006</td>
<td style="text-align: right;">479360</td>
<td style="text-align: right;">59920</td>
<td style="text-align: right;">59915</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.30e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140486725146432</td>
<td style="text-align: right;">3.4685100</td>
<td style="text-align: right;">3.2241300</td>
<td style="text-align: right;">0.2443800</td>
<td style="text-align: right;">16140088</td>
<td style="text-align: right;">5179952</td>
<td style="text-align: right;">60224</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">5.76e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140486725146432</td>
<td style="text-align: right;">0.0550395</td>
<td style="text-align: right;">0.0459746</td>
<td style="text-align: right;">0.0090649</td>
<td style="text-align: right;">481856</td>
<td style="text-align: right;">60232</td>
<td style="text-align: right;">60224</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">9.00e-07</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139663058487104</td>
<td style="text-align: right;">3.3869100</td>
<td style="text-align: right;">3.1493800</td>
<td style="text-align: right;">0.2375290</td>
<td style="text-align: right;">16549844</td>
<td style="text-align: right;">5311274</td>
<td style="text-align: right;">61753</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">5.48e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139663058487104</td>
<td style="text-align: right;">0.0618651</td>
<td style="text-align: right;">0.0458323</td>
<td style="text-align: right;">0.0160329</td>
<td style="text-align: right;">494072</td>
<td style="text-align: right;">61759</td>
<td style="text-align: right;">61753</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.00e-06</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140121158911808</td>
<td style="text-align: right;">1.1398900</td>
<td style="text-align: right;">1.1255500</td>
<td style="text-align: right;">0.0143466</td>
<td style="text-align: right;">248920</td>
<td style="text-align: right;">5351780</td>
<td style="text-align: right;">62230</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.83e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140121158911808</td>
<td style="text-align: right;">0.0753254</td>
<td style="text-align: right;">0.0615234</td>
<td style="text-align: right;">0.0138020</td>
<td style="text-align: right;">497840</td>
<td style="text-align: right;">62230</td>
<td style="text-align: right;">62230</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.20e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140455557797696</td>
<td style="text-align: right;">1.0002800</td>
<td style="text-align: right;">0.9831380</td>
<td style="text-align: right;">0.0171388</td>
<td style="text-align: right;">250044</td>
<td style="text-align: right;">5375946</td>
<td style="text-align: right;">62511</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.60e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140455557797696</td>
<td style="text-align: right;">0.0597880</td>
<td style="text-align: right;">0.0454330</td>
<td style="text-align: right;">0.0143549</td>
<td style="text-align: right;">500088</td>
<td style="text-align: right;">62511</td>
<td style="text-align: right;">62511</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.00e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140367192667968</td>
<td style="text-align: right;">0.8483170</td>
<td style="text-align: right;">0.8350280</td>
<td style="text-align: right;">0.0132889</td>
<td style="text-align: right;">228252</td>
<td style="text-align: right;">4907418</td>
<td style="text-align: right;">57063</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.49e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140367192667968</td>
<td style="text-align: right;">0.0465169</td>
<td style="text-align: right;">0.0330606</td>
<td style="text-align: right;">0.0134562</td>
<td style="text-align: right;">456504</td>
<td style="text-align: right;">57063</td>
<td style="text-align: right;">57063</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">8.00e-07</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139928385025856</td>
<td style="text-align: right;">0.9925250</td>
<td style="text-align: right;">0.9774230</td>
<td style="text-align: right;">0.0151022</td>
<td style="text-align: right;">239568</td>
<td style="text-align: right;">5150712</td>
<td style="text-align: right;">59892</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.66e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139928385025856</td>
<td style="text-align: right;">0.0613565</td>
<td style="text-align: right;">0.0422127</td>
<td style="text-align: right;">0.0191438</td>
<td style="text-align: right;">479136</td>
<td style="text-align: right;">59892</td>
<td style="text-align: right;">59892</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1.00e-06</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">5.4e-05</td>
<td style="text-align: right;">3.2858900</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">1.0e-06</td>
<td style="text-align: right;">0.0591504</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">1.63e-05</td>
<td style="text-align: right;">0.9964025</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">1.00e-06</td>
<td style="text-align: right;">0.0605722</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative  binomial for  eQTL with fixed genotypes. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    parameters {
      vector[K] betas; // regression param
      real<lower=-10,upper=10> bj; // log fold change ASE
      real<lower=1e-5> phi; //overdipersion param for neg binom
    }


    model {
      profile("priors") {
        // include transformed parameters of no interest
        vector[k] lps; // help for mixed gaussians

        // Priors
        phi ~ gamma(1, 0.01);

        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      profile("likelihood") {
        // Likelihood
        real ebj = exp(bj);
        vector[N] lmu;//the linear predictor
        lmu = cov[,2:cols(cov)]*betas;
        for(i in 1:N){ // neg binomial
          lmu[i] = fabs(g[i])==1 ? lmu[i] + log1p(ebj)-log(2) : lmu[i];
          lmu[i] = g[i]==2 ? lmu[i] + bj : lmu[i];
          target += neg_binomial_2_lpmf(Y[i] | exp(lmu[i]),phi);
        }
      }
    }

Optimised model:

    // negative  binomial for  eQTL with fixed genotypes. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      // simplex[k] expMixP; // log of mixing proportions for eQTL effect prior
    }

    parameters {
      vector[K] betas; // regression param
      real <lower=-10,upper=10> bj; // log fold change ASE
      // real bj; // log fold change ASE
      real <lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
    }


    model {
      profile("priors") {
        // include transformed parameters of no interest
        vector[k] lps; // help for mixed gaussians

        // Priors
        phi ~ gamma(1, 0.01);

        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5); //prior for the slopes following Gelman 2008   
        }

        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
          // lps[i] = normal_lpdf(bj | aveP[i], sdP[i]);
        }
        target += log_sum_exp(lps);
        // suggestion from stan team discussions that one of these should be faster,
        // but they don't seem to be right now
        // target += log_sum_exp(lps + mixP);
        // target += log_mix(expMixP, lps);
      }
      profile("likelihood") {
        real l1pebj = log1p(exp(bj)) - log(2);
        vector[N] intercept = rep_vector(1e-5, N); // the genetic effect; 0 if hom ref
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj; // log1p(exp(b_afc)) - log(2) if he
          }
          if (g[i] == 2) {
            intercept[i] = bj; // b_afc if hom alt
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);
      }
    }
