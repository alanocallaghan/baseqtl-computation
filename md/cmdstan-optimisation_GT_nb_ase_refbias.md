Optimising GT\_nb\_ase\_refbias
===============================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

    ## Warning: Chain 1 finished unexpectedly!

    ## Warning: Chain 2 finished unexpectedly!

    ## Warning: 2 chain(s) finished unexpectedly!

    ## Warning: The returned fit object will only read in results of successful
    ## chains. Please use read_cmdstan_csv() to read the results of the failed chains
    ## separately.Use the $output(chain_id) method for more output of the failed
    ## chains.

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_GT_nb_ase_refbias_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7429109</td>
<td style="text-align: right;">6.7430400</td>
<td style="text-align: right;">0.0381365</td>
<td style="text-align: right;">0.0382066</td>
<td style="text-align: right;">6.6803660</td>
<td style="text-align: right;">6.8058225</td>
<td style="text-align: right;">0.9998673</td>
<td style="text-align: right;">13558.28</td>
<td style="text-align: right;">7551.901</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2132511</td>
<td style="text-align: right;">0.2125830</td>
<td style="text-align: right;">0.0402063</td>
<td style="text-align: right;">0.0402474</td>
<td style="text-align: right;">0.1483386</td>
<td style="text-align: right;">0.2799046</td>
<td style="text-align: right;">0.9999611</td>
<td style="text-align: right;">15067.00</td>
<td style="text-align: right;">6894.274</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0020199</td>
<td style="text-align: right;">-0.0019400</td>
<td style="text-align: right;">0.0284100</td>
<td style="text-align: right;">0.0285785</td>
<td style="text-align: right;">-0.0494255</td>
<td style="text-align: right;">0.0445031</td>
<td style="text-align: right;">1.0005357</td>
<td style="text-align: right;">15125.02</td>
<td style="text-align: right;">7557.089</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.5836368</td>
<td style="text-align: right;">8.4975800</td>
<td style="text-align: right;">1.3145986</td>
<td style="text-align: right;">1.2976975</td>
<td style="text-align: right;">6.5340155</td>
<td style="text-align: right;">10.8740600</td>
<td style="text-align: right;">1.0002405</td>
<td style="text-align: right;">14403.47</td>
<td style="text-align: right;">7951.798</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">40.6952008</td>
<td style="text-align: right;">39.5248000</td>
<td style="text-align: right;">10.9291977</td>
<td style="text-align: right;">10.4196387</td>
<td style="text-align: right;">24.8915500</td>
<td style="text-align: right;">60.3738850</td>
<td style="text-align: right;">1.0010913</td>
<td style="text-align: right;">14839.97</td>
<td style="text-align: right;">6196.474</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[1]</td>
<td style="text-align: right;">-0.0486430</td>
<td style="text-align: right;">-0.0486315</td>
<td style="text-align: right;">0.0100391</td>
<td style="text-align: right;">0.0100451</td>
<td style="text-align: right;">-0.0651736</td>
<td style="text-align: right;">-0.0322454</td>
<td style="text-align: right;">0.9998586</td>
<td style="text-align: right;">14986.02</td>
<td style="text-align: right;">7953.910</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[2]</td>
<td style="text-align: right;">0.0485072</td>
<td style="text-align: right;">0.0484242</td>
<td style="text-align: right;">0.0101714</td>
<td style="text-align: right;">0.0103957</td>
<td style="text-align: right;">0.0319971</td>
<td style="text-align: right;">0.0653776</td>
<td style="text-align: right;">1.0004124</td>
<td style="text-align: right;">15616.28</td>
<td style="text-align: right;">7768.711</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[3]</td>
<td style="text-align: right;">-0.0486227</td>
<td style="text-align: right;">-0.0486788</td>
<td style="text-align: right;">0.0102316</td>
<td style="text-align: right;">0.0103396</td>
<td style="text-align: right;">-0.0654834</td>
<td style="text-align: right;">-0.0317163</td>
<td style="text-align: right;">0.9999417</td>
<td style="text-align: right;">15668.75</td>
<td style="text-align: right;">7465.294</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[4]</td>
<td style="text-align: right;">-0.0488113</td>
<td style="text-align: right;">-0.0489244</td>
<td style="text-align: right;">0.0101369</td>
<td style="text-align: right;">0.0100907</td>
<td style="text-align: right;">-0.0654851</td>
<td style="text-align: right;">-0.0320311</td>
<td style="text-align: right;">1.0002463</td>
<td style="text-align: right;">14098.32</td>
<td style="text-align: right;">7611.362</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[5]</td>
<td style="text-align: right;">0.0486104</td>
<td style="text-align: right;">0.0486010</td>
<td style="text-align: right;">0.0100958</td>
<td style="text-align: right;">0.0101210</td>
<td style="text-align: right;">0.0319437</td>
<td style="text-align: right;">0.0651886</td>
<td style="text-align: right;">0.9999680</td>
<td style="text-align: right;">14920.32</td>
<td style="text-align: right;">6771.520</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[6]</td>
<td style="text-align: right;">0.0487230</td>
<td style="text-align: right;">0.0488304</td>
<td style="text-align: right;">0.0102241</td>
<td style="text-align: right;">0.0101340</td>
<td style="text-align: right;">0.0318863</td>
<td style="text-align: right;">0.0655432</td>
<td style="text-align: right;">1.0001531</td>
<td style="text-align: right;">13463.28</td>
<td style="text-align: right;">7522.319</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[7]</td>
<td style="text-align: right;">-0.0486239</td>
<td style="text-align: right;">-0.0485124</td>
<td style="text-align: right;">0.0101849</td>
<td style="text-align: right;">0.0100368</td>
<td style="text-align: right;">-0.0657392</td>
<td style="text-align: right;">-0.0318422</td>
<td style="text-align: right;">0.9999770</td>
<td style="text-align: right;">15122.86</td>
<td style="text-align: right;">7057.998</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[8]</td>
<td style="text-align: right;">0.0480771</td>
<td style="text-align: right;">0.0481141</td>
<td style="text-align: right;">0.0103035</td>
<td style="text-align: right;">0.0102898</td>
<td style="text-align: right;">0.0312189</td>
<td style="text-align: right;">0.0649496</td>
<td style="text-align: right;">1.0001534</td>
<td style="text-align: right;">16257.37</td>
<td style="text-align: right;">7216.301</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[9]</td>
<td style="text-align: right;">-0.0485370</td>
<td style="text-align: right;">-0.0484342</td>
<td style="text-align: right;">0.0102571</td>
<td style="text-align: right;">0.0102878</td>
<td style="text-align: right;">-0.0653743</td>
<td style="text-align: right;">-0.0316215</td>
<td style="text-align: right;">1.0000479</td>
<td style="text-align: right;">14619.75</td>
<td style="text-align: right;">7569.752</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[10]</td>
<td style="text-align: right;">-0.0480727</td>
<td style="text-align: right;">-0.0481447</td>
<td style="text-align: right;">0.0101317</td>
<td style="text-align: right;">0.0102282</td>
<td style="text-align: right;">-0.0646501</td>
<td style="text-align: right;">-0.0315822</td>
<td style="text-align: right;">1.0012001</td>
<td style="text-align: right;">15272.44</td>
<td style="text-align: right;">7191.082</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[11]</td>
<td style="text-align: right;">0.0485005</td>
<td style="text-align: right;">0.0485406</td>
<td style="text-align: right;">0.0103062</td>
<td style="text-align: right;">0.0103441</td>
<td style="text-align: right;">0.0314864</td>
<td style="text-align: right;">0.0656656</td>
<td style="text-align: right;">0.9998904</td>
<td style="text-align: right;">14967.83</td>
<td style="text-align: right;">6402.055</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[12]</td>
<td style="text-align: right;">-0.0486267</td>
<td style="text-align: right;">-0.0485966</td>
<td style="text-align: right;">0.0100518</td>
<td style="text-align: right;">0.0098850</td>
<td style="text-align: right;">-0.0656347</td>
<td style="text-align: right;">-0.0320857</td>
<td style="text-align: right;">0.9999434</td>
<td style="text-align: right;">15543.34</td>
<td style="text-align: right;">7440.325</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[13]</td>
<td style="text-align: right;">0.0482341</td>
<td style="text-align: right;">0.0482302</td>
<td style="text-align: right;">0.0101730</td>
<td style="text-align: right;">0.0100674</td>
<td style="text-align: right;">0.0316275</td>
<td style="text-align: right;">0.0649274</td>
<td style="text-align: right;">1.0002434</td>
<td style="text-align: right;">16103.96</td>
<td style="text-align: right;">6590.788</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[14]</td>
<td style="text-align: right;">-0.0483799</td>
<td style="text-align: right;">-0.0483614</td>
<td style="text-align: right;">0.0101145</td>
<td style="text-align: right;">0.0101119</td>
<td style="text-align: right;">-0.0650002</td>
<td style="text-align: right;">-0.0316882</td>
<td style="text-align: right;">1.0003866</td>
<td style="text-align: right;">16133.30</td>
<td style="text-align: right;">7237.422</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[15]</td>
<td style="text-align: right;">-0.0486610</td>
<td style="text-align: right;">-0.0486084</td>
<td style="text-align: right;">0.0101978</td>
<td style="text-align: right;">0.0101676</td>
<td style="text-align: right;">-0.0654965</td>
<td style="text-align: right;">-0.0319490</td>
<td style="text-align: right;">1.0002512</td>
<td style="text-align: right;">15078.98</td>
<td style="text-align: right;">7151.413</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[16]</td>
<td style="text-align: right;">0.0484706</td>
<td style="text-align: right;">0.0485234</td>
<td style="text-align: right;">0.0102728</td>
<td style="text-align: right;">0.0101331</td>
<td style="text-align: right;">0.0317810</td>
<td style="text-align: right;">0.0654281</td>
<td style="text-align: right;">1.0006959</td>
<td style="text-align: right;">13574.02</td>
<td style="text-align: right;">7503.143</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[17]</td>
<td style="text-align: right;">0.0482028</td>
<td style="text-align: right;">0.0481403</td>
<td style="text-align: right;">0.0099410</td>
<td style="text-align: right;">0.0100247</td>
<td style="text-align: right;">0.0319081</td>
<td style="text-align: right;">0.0645451</td>
<td style="text-align: right;">0.9999398</td>
<td style="text-align: right;">15127.51</td>
<td style="text-align: right;">7743.163</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[18]</td>
<td style="text-align: right;">0.0484060</td>
<td style="text-align: right;">0.0484038</td>
<td style="text-align: right;">0.0101374</td>
<td style="text-align: right;">0.0101090</td>
<td style="text-align: right;">0.0314960</td>
<td style="text-align: right;">0.0650364</td>
<td style="text-align: right;">1.0002613</td>
<td style="text-align: right;">15867.65</td>
<td style="text-align: right;">6796.034</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[19]</td>
<td style="text-align: right;">-0.0487930</td>
<td style="text-align: right;">-0.0487034</td>
<td style="text-align: right;">0.0101851</td>
<td style="text-align: right;">0.0102654</td>
<td style="text-align: right;">-0.0655669</td>
<td style="text-align: right;">-0.0321895</td>
<td style="text-align: right;">1.0002983</td>
<td style="text-align: right;">14909.29</td>
<td style="text-align: right;">7247.677</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[20]</td>
<td style="text-align: right;">0.0483163</td>
<td style="text-align: right;">0.0484119</td>
<td style="text-align: right;">0.0100487</td>
<td style="text-align: right;">0.0099602</td>
<td style="text-align: right;">0.0314850</td>
<td style="text-align: right;">0.0648124</td>
<td style="text-align: right;">0.9998753</td>
<td style="text-align: right;">15783.54</td>
<td style="text-align: right;">7293.652</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[21]</td>
<td style="text-align: right;">-0.0484675</td>
<td style="text-align: right;">-0.0484686</td>
<td style="text-align: right;">0.0100505</td>
<td style="text-align: right;">0.0100335</td>
<td style="text-align: right;">-0.0650661</td>
<td style="text-align: right;">-0.0317117</td>
<td style="text-align: right;">1.0002994</td>
<td style="text-align: right;">15689.06</td>
<td style="text-align: right;">7940.034</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[22]</td>
<td style="text-align: right;">-0.0486351</td>
<td style="text-align: right;">-0.0486928</td>
<td style="text-align: right;">0.0102141</td>
<td style="text-align: right;">0.0102921</td>
<td style="text-align: right;">-0.0652753</td>
<td style="text-align: right;">-0.0317806</td>
<td style="text-align: right;">1.0001924</td>
<td style="text-align: right;">13980.27</td>
<td style="text-align: right;">6889.922</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[23]</td>
<td style="text-align: right;">0.0486263</td>
<td style="text-align: right;">0.0485600</td>
<td style="text-align: right;">0.0101488</td>
<td style="text-align: right;">0.0099774</td>
<td style="text-align: right;">0.0320112</td>
<td style="text-align: right;">0.0652989</td>
<td style="text-align: right;">0.9998753</td>
<td style="text-align: right;">16055.43</td>
<td style="text-align: right;">7480.521</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[24]</td>
<td style="text-align: right;">-0.0476807</td>
<td style="text-align: right;">-0.0475386</td>
<td style="text-align: right;">0.0100847</td>
<td style="text-align: right;">0.0101435</td>
<td style="text-align: right;">-0.0643687</td>
<td style="text-align: right;">-0.0311052</td>
<td style="text-align: right;">1.0014469</td>
<td style="text-align: right;">14552.02</td>
<td style="text-align: right;">7951.991</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[25]</td>
<td style="text-align: right;">-0.0484512</td>
<td style="text-align: right;">-0.0485224</td>
<td style="text-align: right;">0.0101556</td>
<td style="text-align: right;">0.0103162</td>
<td style="text-align: right;">-0.0652004</td>
<td style="text-align: right;">-0.0317634</td>
<td style="text-align: right;">1.0000282</td>
<td style="text-align: right;">16731.72</td>
<td style="text-align: right;">7609.462</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[26]</td>
<td style="text-align: right;">0.0485437</td>
<td style="text-align: right;">0.0485668</td>
<td style="text-align: right;">0.0102530</td>
<td style="text-align: right;">0.0102626</td>
<td style="text-align: right;">0.0316520</td>
<td style="text-align: right;">0.0654106</td>
<td style="text-align: right;">0.9999758</td>
<td style="text-align: right;">15678.90</td>
<td style="text-align: right;">7586.274</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[27]</td>
<td style="text-align: right;">-0.0485113</td>
<td style="text-align: right;">-0.0485231</td>
<td style="text-align: right;">0.0100371</td>
<td style="text-align: right;">0.0100572</td>
<td style="text-align: right;">-0.0652430</td>
<td style="text-align: right;">-0.0321611</td>
<td style="text-align: right;">1.0000827</td>
<td style="text-align: right;">15461.09</td>
<td style="text-align: right;">7053.051</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[28]</td>
<td style="text-align: right;">0.0486111</td>
<td style="text-align: right;">0.0485229</td>
<td style="text-align: right;">0.0098795</td>
<td style="text-align: right;">0.0100951</td>
<td style="text-align: right;">0.0324895</td>
<td style="text-align: right;">0.0646911</td>
<td style="text-align: right;">1.0000043</td>
<td style="text-align: right;">14545.66</td>
<td style="text-align: right;">7399.156</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[29]</td>
<td style="text-align: right;">-0.0487873</td>
<td style="text-align: right;">-0.0488258</td>
<td style="text-align: right;">0.0101264</td>
<td style="text-align: right;">0.0102356</td>
<td style="text-align: right;">-0.0656031</td>
<td style="text-align: right;">-0.0321144</td>
<td style="text-align: right;">1.0000703</td>
<td style="text-align: right;">13765.80</td>
<td style="text-align: right;">7762.117</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[30]</td>
<td style="text-align: right;">-0.0483085</td>
<td style="text-align: right;">-0.0483677</td>
<td style="text-align: right;">0.0101780</td>
<td style="text-align: right;">0.0102392</td>
<td style="text-align: right;">-0.0651315</td>
<td style="text-align: right;">-0.0317047</td>
<td style="text-align: right;">0.9999162</td>
<td style="text-align: right;">14343.22</td>
<td style="text-align: right;">7594.708</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[31]</td>
<td style="text-align: right;">0.0486831</td>
<td style="text-align: right;">0.0486566</td>
<td style="text-align: right;">0.0103538</td>
<td style="text-align: right;">0.0100668</td>
<td style="text-align: right;">0.0314734</td>
<td style="text-align: right;">0.0658331</td>
<td style="text-align: right;">1.0001115</td>
<td style="text-align: right;">14707.21</td>
<td style="text-align: right;">7421.038</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[32]</td>
<td style="text-align: right;">-0.0482307</td>
<td style="text-align: right;">-0.0482824</td>
<td style="text-align: right;">0.0102435</td>
<td style="text-align: right;">0.0102677</td>
<td style="text-align: right;">-0.0650472</td>
<td style="text-align: right;">-0.0311246</td>
<td style="text-align: right;">1.0001593</td>
<td style="text-align: right;">16240.47</td>
<td style="text-align: right;">7201.420</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[33]</td>
<td style="text-align: right;">-0.0482345</td>
<td style="text-align: right;">-0.0482729</td>
<td style="text-align: right;">0.0102457</td>
<td style="text-align: right;">0.0103920</td>
<td style="text-align: right;">-0.0649900</td>
<td style="text-align: right;">-0.0315200</td>
<td style="text-align: right;">1.0001364</td>
<td style="text-align: right;">15551.99</td>
<td style="text-align: right;">7293.764</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[34]</td>
<td style="text-align: right;">-0.0486479</td>
<td style="text-align: right;">-0.0486632</td>
<td style="text-align: right;">0.0099697</td>
<td style="text-align: right;">0.0097918</td>
<td style="text-align: right;">-0.0652274</td>
<td style="text-align: right;">-0.0323175</td>
<td style="text-align: right;">1.0007882</td>
<td style="text-align: right;">15049.08</td>
<td style="text-align: right;">7305.302</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[35]</td>
<td style="text-align: right;">-0.0484752</td>
<td style="text-align: right;">-0.0484796</td>
<td style="text-align: right;">0.0102016</td>
<td style="text-align: right;">0.0102609</td>
<td style="text-align: right;">-0.0653620</td>
<td style="text-align: right;">-0.0315226</td>
<td style="text-align: right;">1.0001008</td>
<td style="text-align: right;">14402.32</td>
<td style="text-align: right;">7671.781</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[36]</td>
<td style="text-align: right;">0.0485200</td>
<td style="text-align: right;">0.0486004</td>
<td style="text-align: right;">0.0101120</td>
<td style="text-align: right;">0.0101209</td>
<td style="text-align: right;">0.0318164</td>
<td style="text-align: right;">0.0651541</td>
<td style="text-align: right;">1.0000870</td>
<td style="text-align: right;">17704.12</td>
<td style="text-align: right;">7179.279</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[37]</td>
<td style="text-align: right;">0.0485257</td>
<td style="text-align: right;">0.0484495</td>
<td style="text-align: right;">0.0100834</td>
<td style="text-align: right;">0.0099889</td>
<td style="text-align: right;">0.0319016</td>
<td style="text-align: right;">0.0651192</td>
<td style="text-align: right;">1.0009850</td>
<td style="text-align: right;">17204.98</td>
<td style="text-align: right;">7582.347</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[38]</td>
<td style="text-align: right;">-0.0483178</td>
<td style="text-align: right;">-0.0483595</td>
<td style="text-align: right;">0.0103263</td>
<td style="text-align: right;">0.0103339</td>
<td style="text-align: right;">-0.0654763</td>
<td style="text-align: right;">-0.0314033</td>
<td style="text-align: right;">1.0003644</td>
<td style="text-align: right;">13871.25</td>
<td style="text-align: right;">7493.367</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[39]</td>
<td style="text-align: right;">-0.0487498</td>
<td style="text-align: right;">-0.0486853</td>
<td style="text-align: right;">0.0101893</td>
<td style="text-align: right;">0.0100471</td>
<td style="text-align: right;">-0.0657793</td>
<td style="text-align: right;">-0.0320582</td>
<td style="text-align: right;">1.0001976</td>
<td style="text-align: right;">14982.53</td>
<td style="text-align: right;">6552.030</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[40]</td>
<td style="text-align: right;">0.0488088</td>
<td style="text-align: right;">0.0488230</td>
<td style="text-align: right;">0.0100092</td>
<td style="text-align: right;">0.0100041</td>
<td style="text-align: right;">0.0322324</td>
<td style="text-align: right;">0.0653269</td>
<td style="text-align: right;">1.0001481</td>
<td style="text-align: right;">13699.34</td>
<td style="text-align: right;">7438.413</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[41]</td>
<td style="text-align: right;">-0.0481372</td>
<td style="text-align: right;">-0.0481640</td>
<td style="text-align: right;">0.0102012</td>
<td style="text-align: right;">0.0103113</td>
<td style="text-align: right;">-0.0649248</td>
<td style="text-align: right;">-0.0314944</td>
<td style="text-align: right;">1.0003454</td>
<td style="text-align: right;">14590.07</td>
<td style="text-align: right;">7831.584</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[42]</td>
<td style="text-align: right;">0.0487777</td>
<td style="text-align: right;">0.0487764</td>
<td style="text-align: right;">0.0099736</td>
<td style="text-align: right;">0.0098548</td>
<td style="text-align: right;">0.0323527</td>
<td style="text-align: right;">0.0652775</td>
<td style="text-align: right;">1.0000028</td>
<td style="text-align: right;">15594.87</td>
<td style="text-align: right;">6995.857</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[43]</td>
<td style="text-align: right;">-0.0485845</td>
<td style="text-align: right;">-0.0485494</td>
<td style="text-align: right;">0.0103285</td>
<td style="text-align: right;">0.0104308</td>
<td style="text-align: right;">-0.0653210</td>
<td style="text-align: right;">-0.0317013</td>
<td style="text-align: right;">1.0000841</td>
<td style="text-align: right;">13927.01</td>
<td style="text-align: right;">7209.461</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[44]</td>
<td style="text-align: right;">-0.0484921</td>
<td style="text-align: right;">-0.0484038</td>
<td style="text-align: right;">0.0103067</td>
<td style="text-align: right;">0.0102882</td>
<td style="text-align: right;">-0.0654531</td>
<td style="text-align: right;">-0.0317462</td>
<td style="text-align: right;">1.0000822</td>
<td style="text-align: right;">13494.07</td>
<td style="text-align: right;">7492.905</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[45]</td>
<td style="text-align: right;">0.0486629</td>
<td style="text-align: right;">0.0485862</td>
<td style="text-align: right;">0.0102475</td>
<td style="text-align: right;">0.0102994</td>
<td style="text-align: right;">0.0318106</td>
<td style="text-align: right;">0.0655765</td>
<td style="text-align: right;">1.0001020</td>
<td style="text-align: right;">15364.50</td>
<td style="text-align: right;">7132.746</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[46]</td>
<td style="text-align: right;">-0.0485874</td>
<td style="text-align: right;">-0.0485246</td>
<td style="text-align: right;">0.0101250</td>
<td style="text-align: right;">0.0102029</td>
<td style="text-align: right;">-0.0654236</td>
<td style="text-align: right;">-0.0319797</td>
<td style="text-align: right;">0.9999339</td>
<td style="text-align: right;">14410.72</td>
<td style="text-align: right;">7940.629</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[47]</td>
<td style="text-align: right;">0.0485449</td>
<td style="text-align: right;">0.0486576</td>
<td style="text-align: right;">0.0101794</td>
<td style="text-align: right;">0.0103387</td>
<td style="text-align: right;">0.0318383</td>
<td style="text-align: right;">0.0651700</td>
<td style="text-align: right;">1.0002585</td>
<td style="text-align: right;">15680.78</td>
<td style="text-align: right;">7274.309</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[48]</td>
<td style="text-align: right;">0.0480224</td>
<td style="text-align: right;">0.0479996</td>
<td style="text-align: right;">0.0101984</td>
<td style="text-align: right;">0.0101308</td>
<td style="text-align: right;">0.0312259</td>
<td style="text-align: right;">0.0648860</td>
<td style="text-align: right;">1.0007287</td>
<td style="text-align: right;">15045.56</td>
<td style="text-align: right;">6821.052</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[49]</td>
<td style="text-align: right;">0.0484471</td>
<td style="text-align: right;">0.0483982</td>
<td style="text-align: right;">0.0103018</td>
<td style="text-align: right;">0.0103654</td>
<td style="text-align: right;">0.0317851</td>
<td style="text-align: right;">0.0654425</td>
<td style="text-align: right;">1.0001754</td>
<td style="text-align: right;">15148.57</td>
<td style="text-align: right;">7610.794</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7425214</td>
<td style="text-align: right;">6.7421700</td>
<td style="text-align: right;">0.0381998</td>
<td style="text-align: right;">0.0379546</td>
<td style="text-align: right;">6.6796600</td>
<td style="text-align: right;">6.8059305</td>
<td style="text-align: right;">1.0002024</td>
<td style="text-align: right;">21818.80</td>
<td style="text-align: right;">15421.85</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2127742</td>
<td style="text-align: right;">0.2122690</td>
<td style="text-align: right;">0.0393503</td>
<td style="text-align: right;">0.0387478</td>
<td style="text-align: right;">0.1484810</td>
<td style="text-align: right;">0.2774732</td>
<td style="text-align: right;">1.0002199</td>
<td style="text-align: right;">23890.41</td>
<td style="text-align: right;">15438.37</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0018444</td>
<td style="text-align: right;">-0.0018524</td>
<td style="text-align: right;">0.0287242</td>
<td style="text-align: right;">0.0284497</td>
<td style="text-align: right;">-0.0489421</td>
<td style="text-align: right;">0.0448858</td>
<td style="text-align: right;">1.0003170</td>
<td style="text-align: right;">24671.40</td>
<td style="text-align: right;">14116.93</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.5940411</td>
<td style="text-align: right;">8.5255400</td>
<td style="text-align: right;">1.3005549</td>
<td style="text-align: right;">1.2941541</td>
<td style="text-align: right;">6.5699665</td>
<td style="text-align: right;">10.8490200</td>
<td style="text-align: right;">1.0000700</td>
<td style="text-align: right;">22394.50</td>
<td style="text-align: right;">14090.40</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">40.7366511</td>
<td style="text-align: right;">39.5630000</td>
<td style="text-align: right;">10.8164227</td>
<td style="text-align: right;">10.3804239</td>
<td style="text-align: right;">25.1847950</td>
<td style="text-align: right;">60.2144700</td>
<td style="text-align: right;">1.0001231</td>
<td style="text-align: right;">25094.92</td>
<td style="text-align: right;">14407.19</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[1]</td>
<td style="text-align: right;">-0.0486317</td>
<td style="text-align: right;">-0.0486544</td>
<td style="text-align: right;">0.0100039</td>
<td style="text-align: right;">0.0100382</td>
<td style="text-align: right;">-0.0650107</td>
<td style="text-align: right;">-0.0320531</td>
<td style="text-align: right;">1.0010948</td>
<td style="text-align: right;">37101.07</td>
<td style="text-align: right;">14743.73</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[2]</td>
<td style="text-align: right;">0.0484684</td>
<td style="text-align: right;">0.0484698</td>
<td style="text-align: right;">0.0100924</td>
<td style="text-align: right;">0.0100477</td>
<td style="text-align: right;">0.0317894</td>
<td style="text-align: right;">0.0650634</td>
<td style="text-align: right;">0.9999036</td>
<td style="text-align: right;">36947.23</td>
<td style="text-align: right;">14829.66</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[3]</td>
<td style="text-align: right;">-0.0486385</td>
<td style="text-align: right;">-0.0486062</td>
<td style="text-align: right;">0.0102275</td>
<td style="text-align: right;">0.0100643</td>
<td style="text-align: right;">-0.0655673</td>
<td style="text-align: right;">-0.0317092</td>
<td style="text-align: right;">1.0000911</td>
<td style="text-align: right;">33519.26</td>
<td style="text-align: right;">14207.10</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[4]</td>
<td style="text-align: right;">-0.0487422</td>
<td style="text-align: right;">-0.0487950</td>
<td style="text-align: right;">0.0101447</td>
<td style="text-align: right;">0.0101078</td>
<td style="text-align: right;">-0.0653540</td>
<td style="text-align: right;">-0.0319760</td>
<td style="text-align: right;">1.0000775</td>
<td style="text-align: right;">35184.87</td>
<td style="text-align: right;">14327.18</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[5]</td>
<td style="text-align: right;">0.0485750</td>
<td style="text-align: right;">0.0485679</td>
<td style="text-align: right;">0.0101835</td>
<td style="text-align: right;">0.0102343</td>
<td style="text-align: right;">0.0317820</td>
<td style="text-align: right;">0.0654863</td>
<td style="text-align: right;">1.0000142</td>
<td style="text-align: right;">36374.45</td>
<td style="text-align: right;">14992.26</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[6]</td>
<td style="text-align: right;">0.0486053</td>
<td style="text-align: right;">0.0486602</td>
<td style="text-align: right;">0.0102811</td>
<td style="text-align: right;">0.0102113</td>
<td style="text-align: right;">0.0315862</td>
<td style="text-align: right;">0.0654116</td>
<td style="text-align: right;">0.9999815</td>
<td style="text-align: right;">34639.92</td>
<td style="text-align: right;">15552.37</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[7]</td>
<td style="text-align: right;">-0.0485990</td>
<td style="text-align: right;">-0.0485823</td>
<td style="text-align: right;">0.0101995</td>
<td style="text-align: right;">0.0101924</td>
<td style="text-align: right;">-0.0653670</td>
<td style="text-align: right;">-0.0318766</td>
<td style="text-align: right;">1.0000989</td>
<td style="text-align: right;">34106.13</td>
<td style="text-align: right;">14547.00</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[8]</td>
<td style="text-align: right;">0.0481936</td>
<td style="text-align: right;">0.0481860</td>
<td style="text-align: right;">0.0101825</td>
<td style="text-align: right;">0.0101496</td>
<td style="text-align: right;">0.0312541</td>
<td style="text-align: right;">0.0649418</td>
<td style="text-align: right;">1.0001080</td>
<td style="text-align: right;">33421.63</td>
<td style="text-align: right;">15268.78</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[9]</td>
<td style="text-align: right;">-0.0486978</td>
<td style="text-align: right;">-0.0486999</td>
<td style="text-align: right;">0.0101526</td>
<td style="text-align: right;">0.0101656</td>
<td style="text-align: right;">-0.0653612</td>
<td style="text-align: right;">-0.0319346</td>
<td style="text-align: right;">1.0000377</td>
<td style="text-align: right;">35858.21</td>
<td style="text-align: right;">14344.84</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[10]</td>
<td style="text-align: right;">-0.0482292</td>
<td style="text-align: right;">-0.0481656</td>
<td style="text-align: right;">0.0101327</td>
<td style="text-align: right;">0.0102172</td>
<td style="text-align: right;">-0.0649367</td>
<td style="text-align: right;">-0.0316552</td>
<td style="text-align: right;">1.0000946</td>
<td style="text-align: right;">36322.77</td>
<td style="text-align: right;">14873.89</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[11]</td>
<td style="text-align: right;">0.0483171</td>
<td style="text-align: right;">0.0483642</td>
<td style="text-align: right;">0.0101051</td>
<td style="text-align: right;">0.0099302</td>
<td style="text-align: right;">0.0316710</td>
<td style="text-align: right;">0.0651715</td>
<td style="text-align: right;">1.0001992</td>
<td style="text-align: right;">35898.11</td>
<td style="text-align: right;">14257.08</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[12]</td>
<td style="text-align: right;">-0.0484577</td>
<td style="text-align: right;">-0.0484644</td>
<td style="text-align: right;">0.0102722</td>
<td style="text-align: right;">0.0100222</td>
<td style="text-align: right;">-0.0652338</td>
<td style="text-align: right;">-0.0314264</td>
<td style="text-align: right;">0.9999191</td>
<td style="text-align: right;">35593.42</td>
<td style="text-align: right;">14104.11</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[13]</td>
<td style="text-align: right;">0.0482516</td>
<td style="text-align: right;">0.0483164</td>
<td style="text-align: right;">0.0102321</td>
<td style="text-align: right;">0.0101679</td>
<td style="text-align: right;">0.0313675</td>
<td style="text-align: right;">0.0648508</td>
<td style="text-align: right;">1.0003762</td>
<td style="text-align: right;">34988.83</td>
<td style="text-align: right;">14531.73</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[14]</td>
<td style="text-align: right;">-0.0483548</td>
<td style="text-align: right;">-0.0483022</td>
<td style="text-align: right;">0.0101816</td>
<td style="text-align: right;">0.0103257</td>
<td style="text-align: right;">-0.0650142</td>
<td style="text-align: right;">-0.0316216</td>
<td style="text-align: right;">1.0005525</td>
<td style="text-align: right;">34937.58</td>
<td style="text-align: right;">14854.66</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[15]</td>
<td style="text-align: right;">-0.0487602</td>
<td style="text-align: right;">-0.0487626</td>
<td style="text-align: right;">0.0103041</td>
<td style="text-align: right;">0.0102660</td>
<td style="text-align: right;">-0.0656301</td>
<td style="text-align: right;">-0.0318606</td>
<td style="text-align: right;">1.0001670</td>
<td style="text-align: right;">36214.62</td>
<td style="text-align: right;">14606.05</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[16]</td>
<td style="text-align: right;">0.0485133</td>
<td style="text-align: right;">0.0485470</td>
<td style="text-align: right;">0.0102585</td>
<td style="text-align: right;">0.0102815</td>
<td style="text-align: right;">0.0314435</td>
<td style="text-align: right;">0.0652469</td>
<td style="text-align: right;">1.0000577</td>
<td style="text-align: right;">35040.64</td>
<td style="text-align: right;">15455.36</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[17]</td>
<td style="text-align: right;">0.0483733</td>
<td style="text-align: right;">0.0482344</td>
<td style="text-align: right;">0.0101299</td>
<td style="text-align: right;">0.0101469</td>
<td style="text-align: right;">0.0318235</td>
<td style="text-align: right;">0.0651327</td>
<td style="text-align: right;">1.0004529</td>
<td style="text-align: right;">34049.55</td>
<td style="text-align: right;">14908.70</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[18]</td>
<td style="text-align: right;">0.0482670</td>
<td style="text-align: right;">0.0482841</td>
<td style="text-align: right;">0.0100837</td>
<td style="text-align: right;">0.0101628</td>
<td style="text-align: right;">0.0316807</td>
<td style="text-align: right;">0.0647908</td>
<td style="text-align: right;">1.0002366</td>
<td style="text-align: right;">34058.74</td>
<td style="text-align: right;">15414.21</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[19]</td>
<td style="text-align: right;">-0.0486570</td>
<td style="text-align: right;">-0.0486937</td>
<td style="text-align: right;">0.0101173</td>
<td style="text-align: right;">0.0100768</td>
<td style="text-align: right;">-0.0654073</td>
<td style="text-align: right;">-0.0319692</td>
<td style="text-align: right;">1.0002755</td>
<td style="text-align: right;">35212.36</td>
<td style="text-align: right;">14863.97</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[20]</td>
<td style="text-align: right;">0.0482636</td>
<td style="text-align: right;">0.0481954</td>
<td style="text-align: right;">0.0101297</td>
<td style="text-align: right;">0.0102179</td>
<td style="text-align: right;">0.0317197</td>
<td style="text-align: right;">0.0649967</td>
<td style="text-align: right;">0.9999508</td>
<td style="text-align: right;">36703.45</td>
<td style="text-align: right;">15163.51</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[21]</td>
<td style="text-align: right;">-0.0485090</td>
<td style="text-align: right;">-0.0485386</td>
<td style="text-align: right;">0.0102722</td>
<td style="text-align: right;">0.0102311</td>
<td style="text-align: right;">-0.0652116</td>
<td style="text-align: right;">-0.0314834</td>
<td style="text-align: right;">1.0003646</td>
<td style="text-align: right;">34236.56</td>
<td style="text-align: right;">13707.15</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[22]</td>
<td style="text-align: right;">-0.0484989</td>
<td style="text-align: right;">-0.0485089</td>
<td style="text-align: right;">0.0101720</td>
<td style="text-align: right;">0.0102080</td>
<td style="text-align: right;">-0.0651454</td>
<td style="text-align: right;">-0.0316035</td>
<td style="text-align: right;">1.0001234</td>
<td style="text-align: right;">34397.60</td>
<td style="text-align: right;">13767.54</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[23]</td>
<td style="text-align: right;">0.0486227</td>
<td style="text-align: right;">0.0486018</td>
<td style="text-align: right;">0.0103119</td>
<td style="text-align: right;">0.0101752</td>
<td style="text-align: right;">0.0317969</td>
<td style="text-align: right;">0.0656726</td>
<td style="text-align: right;">1.0003651</td>
<td style="text-align: right;">36373.72</td>
<td style="text-align: right;">13815.01</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[24]</td>
<td style="text-align: right;">-0.0477282</td>
<td style="text-align: right;">-0.0476718</td>
<td style="text-align: right;">0.0099869</td>
<td style="text-align: right;">0.0098373</td>
<td style="text-align: right;">-0.0643242</td>
<td style="text-align: right;">-0.0312823</td>
<td style="text-align: right;">1.0000691</td>
<td style="text-align: right;">33318.18</td>
<td style="text-align: right;">14027.03</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[25]</td>
<td style="text-align: right;">-0.0482464</td>
<td style="text-align: right;">-0.0482198</td>
<td style="text-align: right;">0.0100780</td>
<td style="text-align: right;">0.0101561</td>
<td style="text-align: right;">-0.0649013</td>
<td style="text-align: right;">-0.0319395</td>
<td style="text-align: right;">1.0002548</td>
<td style="text-align: right;">33663.97</td>
<td style="text-align: right;">15105.28</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[26]</td>
<td style="text-align: right;">0.0486237</td>
<td style="text-align: right;">0.0487197</td>
<td style="text-align: right;">0.0103522</td>
<td style="text-align: right;">0.0103844</td>
<td style="text-align: right;">0.0315979</td>
<td style="text-align: right;">0.0655406</td>
<td style="text-align: right;">1.0003619</td>
<td style="text-align: right;">33567.18</td>
<td style="text-align: right;">14995.74</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[27]</td>
<td style="text-align: right;">-0.0485823</td>
<td style="text-align: right;">-0.0485234</td>
<td style="text-align: right;">0.0102487</td>
<td style="text-align: right;">0.0102882</td>
<td style="text-align: right;">-0.0655823</td>
<td style="text-align: right;">-0.0319271</td>
<td style="text-align: right;">1.0001170</td>
<td style="text-align: right;">37624.96</td>
<td style="text-align: right;">14885.10</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[28]</td>
<td style="text-align: right;">0.0485977</td>
<td style="text-align: right;">0.0485741</td>
<td style="text-align: right;">0.0102037</td>
<td style="text-align: right;">0.0102525</td>
<td style="text-align: right;">0.0318174</td>
<td style="text-align: right;">0.0654205</td>
<td style="text-align: right;">1.0000045</td>
<td style="text-align: right;">36958.99</td>
<td style="text-align: right;">14826.73</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[29]</td>
<td style="text-align: right;">-0.0485588</td>
<td style="text-align: right;">-0.0485334</td>
<td style="text-align: right;">0.0100732</td>
<td style="text-align: right;">0.0100732</td>
<td style="text-align: right;">-0.0651820</td>
<td style="text-align: right;">-0.0320601</td>
<td style="text-align: right;">0.9999456</td>
<td style="text-align: right;">35650.94</td>
<td style="text-align: right;">15187.44</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[30]</td>
<td style="text-align: right;">-0.0483076</td>
<td style="text-align: right;">-0.0482488</td>
<td style="text-align: right;">0.0102010</td>
<td style="text-align: right;">0.0102413</td>
<td style="text-align: right;">-0.0650652</td>
<td style="text-align: right;">-0.0316114</td>
<td style="text-align: right;">1.0002786</td>
<td style="text-align: right;">36484.97</td>
<td style="text-align: right;">14692.69</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[31]</td>
<td style="text-align: right;">0.0486614</td>
<td style="text-align: right;">0.0486127</td>
<td style="text-align: right;">0.0101297</td>
<td style="text-align: right;">0.0101523</td>
<td style="text-align: right;">0.0321244</td>
<td style="text-align: right;">0.0653645</td>
<td style="text-align: right;">1.0007396</td>
<td style="text-align: right;">33264.93</td>
<td style="text-align: right;">15308.41</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[32]</td>
<td style="text-align: right;">-0.0482204</td>
<td style="text-align: right;">-0.0482639</td>
<td style="text-align: right;">0.0102664</td>
<td style="text-align: right;">0.0100720</td>
<td style="text-align: right;">-0.0650060</td>
<td style="text-align: right;">-0.0312338</td>
<td style="text-align: right;">1.0002639</td>
<td style="text-align: right;">36228.26</td>
<td style="text-align: right;">14676.26</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[33]</td>
<td style="text-align: right;">-0.0482857</td>
<td style="text-align: right;">-0.0482067</td>
<td style="text-align: right;">0.0101932</td>
<td style="text-align: right;">0.0101774</td>
<td style="text-align: right;">-0.0651026</td>
<td style="text-align: right;">-0.0316248</td>
<td style="text-align: right;">1.0002073</td>
<td style="text-align: right;">34629.61</td>
<td style="text-align: right;">14639.88</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[34]</td>
<td style="text-align: right;">-0.0485838</td>
<td style="text-align: right;">-0.0485939</td>
<td style="text-align: right;">0.0100828</td>
<td style="text-align: right;">0.0099608</td>
<td style="text-align: right;">-0.0652077</td>
<td style="text-align: right;">-0.0320570</td>
<td style="text-align: right;">1.0002236</td>
<td style="text-align: right;">34638.00</td>
<td style="text-align: right;">14914.96</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[35]</td>
<td style="text-align: right;">-0.0483460</td>
<td style="text-align: right;">-0.0482975</td>
<td style="text-align: right;">0.0101408</td>
<td style="text-align: right;">0.0101199</td>
<td style="text-align: right;">-0.0651109</td>
<td style="text-align: right;">-0.0318036</td>
<td style="text-align: right;">1.0002873</td>
<td style="text-align: right;">34165.13</td>
<td style="text-align: right;">14202.72</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[36]</td>
<td style="text-align: right;">0.0486135</td>
<td style="text-align: right;">0.0486041</td>
<td style="text-align: right;">0.0100760</td>
<td style="text-align: right;">0.0101162</td>
<td style="text-align: right;">0.0320343</td>
<td style="text-align: right;">0.0653628</td>
<td style="text-align: right;">1.0003357</td>
<td style="text-align: right;">32971.58</td>
<td style="text-align: right;">14557.78</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[37]</td>
<td style="text-align: right;">0.0487265</td>
<td style="text-align: right;">0.0487052</td>
<td style="text-align: right;">0.0101987</td>
<td style="text-align: right;">0.0102484</td>
<td style="text-align: right;">0.0320969</td>
<td style="text-align: right;">0.0654841</td>
<td style="text-align: right;">1.0000533</td>
<td style="text-align: right;">35226.76</td>
<td style="text-align: right;">14678.94</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[38]</td>
<td style="text-align: right;">-0.0482994</td>
<td style="text-align: right;">-0.0482725</td>
<td style="text-align: right;">0.0103096</td>
<td style="text-align: right;">0.0102170</td>
<td style="text-align: right;">-0.0651888</td>
<td style="text-align: right;">-0.0314013</td>
<td style="text-align: right;">1.0002719</td>
<td style="text-align: right;">36437.03</td>
<td style="text-align: right;">14219.96</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[39]</td>
<td style="text-align: right;">-0.0488295</td>
<td style="text-align: right;">-0.0487938</td>
<td style="text-align: right;">0.0102651</td>
<td style="text-align: right;">0.0102405</td>
<td style="text-align: right;">-0.0657257</td>
<td style="text-align: right;">-0.0321609</td>
<td style="text-align: right;">1.0001546</td>
<td style="text-align: right;">35647.86</td>
<td style="text-align: right;">15463.64</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[40]</td>
<td style="text-align: right;">0.0487208</td>
<td style="text-align: right;">0.0487476</td>
<td style="text-align: right;">0.0101901</td>
<td style="text-align: right;">0.0101649</td>
<td style="text-align: right;">0.0320231</td>
<td style="text-align: right;">0.0653429</td>
<td style="text-align: right;">1.0002971</td>
<td style="text-align: right;">33809.58</td>
<td style="text-align: right;">13267.06</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[41]</td>
<td style="text-align: right;">-0.0484556</td>
<td style="text-align: right;">-0.0484339</td>
<td style="text-align: right;">0.0101049</td>
<td style="text-align: right;">0.0100485</td>
<td style="text-align: right;">-0.0651339</td>
<td style="text-align: right;">-0.0318119</td>
<td style="text-align: right;">0.9999356</td>
<td style="text-align: right;">36993.38</td>
<td style="text-align: right;">15622.10</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[42]</td>
<td style="text-align: right;">0.0485873</td>
<td style="text-align: right;">0.0487228</td>
<td style="text-align: right;">0.0102075</td>
<td style="text-align: right;">0.0103561</td>
<td style="text-align: right;">0.0317725</td>
<td style="text-align: right;">0.0652381</td>
<td style="text-align: right;">0.9999504</td>
<td style="text-align: right;">36625.18</td>
<td style="text-align: right;">13991.73</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[43]</td>
<td style="text-align: right;">-0.0486081</td>
<td style="text-align: right;">-0.0486147</td>
<td style="text-align: right;">0.0101814</td>
<td style="text-align: right;">0.0101359</td>
<td style="text-align: right;">-0.0652895</td>
<td style="text-align: right;">-0.0315720</td>
<td style="text-align: right;">1.0000057</td>
<td style="text-align: right;">34406.00</td>
<td style="text-align: right;">15288.69</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[44]</td>
<td style="text-align: right;">-0.0485701</td>
<td style="text-align: right;">-0.0485488</td>
<td style="text-align: right;">0.0100225</td>
<td style="text-align: right;">0.0100726</td>
<td style="text-align: right;">-0.0649922</td>
<td style="text-align: right;">-0.0320270</td>
<td style="text-align: right;">1.0004328</td>
<td style="text-align: right;">34042.67</td>
<td style="text-align: right;">15316.80</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[45]</td>
<td style="text-align: right;">0.0485344</td>
<td style="text-align: right;">0.0485438</td>
<td style="text-align: right;">0.0102051</td>
<td style="text-align: right;">0.0101780</td>
<td style="text-align: right;">0.0316367</td>
<td style="text-align: right;">0.0652651</td>
<td style="text-align: right;">1.0004807</td>
<td style="text-align: right;">35608.27</td>
<td style="text-align: right;">14692.85</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[46]</td>
<td style="text-align: right;">-0.0484740</td>
<td style="text-align: right;">-0.0484860</td>
<td style="text-align: right;">0.0102779</td>
<td style="text-align: right;">0.0102554</td>
<td style="text-align: right;">-0.0654153</td>
<td style="text-align: right;">-0.0316021</td>
<td style="text-align: right;">1.0006165</td>
<td style="text-align: right;">38342.91</td>
<td style="text-align: right;">15155.89</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[47]</td>
<td style="text-align: right;">0.0487330</td>
<td style="text-align: right;">0.0487904</td>
<td style="text-align: right;">0.0102031</td>
<td style="text-align: right;">0.0102041</td>
<td style="text-align: right;">0.0319029</td>
<td style="text-align: right;">0.0656194</td>
<td style="text-align: right;">1.0006208</td>
<td style="text-align: right;">36313.71</td>
<td style="text-align: right;">14803.70</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[48]</td>
<td style="text-align: right;">0.0479863</td>
<td style="text-align: right;">0.0478875</td>
<td style="text-align: right;">0.0102433</td>
<td style="text-align: right;">0.0101818</td>
<td style="text-align: right;">0.0310807</td>
<td style="text-align: right;">0.0649679</td>
<td style="text-align: right;">1.0004502</td>
<td style="text-align: right;">36437.63</td>
<td style="text-align: right;">15264.76</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[49]</td>
<td style="text-align: right;">0.0486026</td>
<td style="text-align: right;">0.0486367</td>
<td style="text-align: right;">0.0102027</td>
<td style="text-align: right;">0.0101937</td>
<td style="text-align: right;">0.0318841</td>
<td style="text-align: right;">0.0653363</td>
<td style="text-align: right;">1.0000349</td>
<td style="text-align: right;">33778.07</td>
<td style="text-align: right;">15000.63</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140366373369664</td>
<td style="text-align: right;">23.722000</td>
<td style="text-align: right;">22.226400</td>
<td style="text-align: right;">1.4956300</td>
<td style="text-align: right;">54114405</td>
<td style="text-align: right;">14307649</td>
<td style="text-align: right;">84158</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0002819</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140366373369664</td>
<td style="text-align: right;">0.866895</td>
<td style="text-align: right;">0.788547</td>
<td style="text-align: right;">0.0783477</td>
<td style="text-align: right;">4881628</td>
<td style="text-align: right;">84166</td>
<td style="text-align: right;">84158</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000103</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140247541200704</td>
<td style="text-align: right;">23.543900</td>
<td style="text-align: right;">22.194800</td>
<td style="text-align: right;">1.3491600</td>
<td style="text-align: right;">53886877</td>
<td style="text-align: right;">14247566</td>
<td style="text-align: right;">83804</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0002809</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140247541200704</td>
<td style="text-align: right;">0.842121</td>
<td style="text-align: right;">0.734858</td>
<td style="text-align: right;">0.1072630</td>
<td style="text-align: right;">4861154</td>
<td style="text-align: right;">83813</td>
<td style="text-align: right;">83804</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000100</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140353185490752</td>
<td style="text-align: right;">14.577300</td>
<td style="text-align: right;">13.646800</td>
<td style="text-align: right;">0.9305580</td>
<td style="text-align: right;">30003978</td>
<td style="text-align: right;">5339691</td>
<td style="text-align: right;">84757</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001720</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140353185490752</td>
<td style="text-align: right;">0.533813</td>
<td style="text-align: right;">0.495018</td>
<td style="text-align: right;">0.0387951</td>
<td style="text-align: right;">847570</td>
<td style="text-align: right;">84757</td>
<td style="text-align: right;">84757</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000063</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140258520233792</td>
<td style="text-align: right;">14.895200</td>
<td style="text-align: right;">14.137400</td>
<td style="text-align: right;">0.7578780</td>
<td style="text-align: right;">30527544</td>
<td style="text-align: right;">5432868</td>
<td style="text-align: right;">86236</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001727</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140258520233792</td>
<td style="text-align: right;">0.549621</td>
<td style="text-align: right;">0.427549</td>
<td style="text-align: right;">0.1220720</td>
<td style="text-align: right;">862360</td>
<td style="text-align: right;">86236</td>
<td style="text-align: right;">86236</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000064</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140329854715712</td>
<td style="text-align: right;">16.628100</td>
<td style="text-align: right;">15.816200</td>
<td style="text-align: right;">0.8118820</td>
<td style="text-align: right;">32907132</td>
<td style="text-align: right;">5856354</td>
<td style="text-align: right;">92958</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001789</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140329854715712</td>
<td style="text-align: right;">0.704858</td>
<td style="text-align: right;">0.637800</td>
<td style="text-align: right;">0.0670580</td>
<td style="text-align: right;">929580</td>
<td style="text-align: right;">92958</td>
<td style="text-align: right;">92958</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000076</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140123170555712</td>
<td style="text-align: right;">15.182000</td>
<td style="text-align: right;">14.056900</td>
<td style="text-align: right;">1.1251000</td>
<td style="text-align: right;">30709854</td>
<td style="text-align: right;">5465313</td>
<td style="text-align: right;">86751</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001750</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140123170555712</td>
<td style="text-align: right;">0.606105</td>
<td style="text-align: right;">0.506220</td>
<td style="text-align: right;">0.0998850</td>
<td style="text-align: right;">867510</td>
<td style="text-align: right;">86751</td>
<td style="text-align: right;">86751</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000070</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0002814</td>
<td style="text-align: right;">23.632950</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000102</td>
<td style="text-align: right;">0.854508</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001739</td>
<td style="text-align: right;">15.038600</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000067</td>
<td style="text-align: right;">0.577863</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance and reference panel bias correction including uncertainty in estimates (version2). Allows for any mixture of priors for eQTL estimate, including single normal
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype in each sample in log scale
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      
    }



    parameters {
      vector[K] betas; // regression param
      real<lower=-10,upper=10> bj; // log fold change ASE
      real<lower=1e-5> phi; //overdipersion param for neg binom
      real<lower=1e-5> theta; //the overdispersion parameter for beta binomial
      vector[L] rai0; // random intercept AI
    }

    model {
      // include transformed parameters of no interest
      vector[N] lmu;//the linear predictor
      vector[L] p; // ASE proportion
      real ebj;
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[L] esum; // reduce computation inverse logit (rai0 + bj)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        // Priors
        theta ~ gamma(1,0.1); //  mean 10 
        phi ~ gamma(1,0.1);  // mean 10

        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
        
        // mean expression and covariates
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }
        for(i in 1:L) {
          rai0[i] ~ normal(ai0[i], sdai0[i]);
        }
      }
      profile("likelihood") {
        // Likelihood
        ebj=exp(bj); // avoid repeating same calculation

        lmu = cov[, 2:cols(cov)] * betas;
        for(i in 1:N){ // neg binomial
          lmu[i] = fabs(g[i])==1 ? lmu[i] + log1p(ebj)-log(2) : lmu[i];
          lmu[i] = g[i]==2 ? lmu[i] + bj : lmu[i];
          target += neg_binomial_2_lpmf(Y[i] | exp(lmu[i]),phi);
        }
        
        pos = 1;
        esum = inv_logit(rai0 + bj);
        esum0 = inv_logit(rai0);
        
        for(i in 1:A) { // ASE
          for (r in pos:(pos+s[i]-1)) {
            
            p[r]= gase[i]==1 ? esum[r] : esum0[r];
            p[r]= gase[i]==-1 ? 1-esum[r] : p[r];  // haplotype swap
            
            ltmp[r]=beta_binomial_lpmf(n[r] | m[i], p[r]*theta , (1-p[r])*theta) + log(pH[r]);
          }
          target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          pos=pos + s[i];
        }
      }
    }

Optimised model:

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance and reference panel bias correction including uncertainty in estimates (version2). Allows for any mixture of priors for eQTL estimate, including single normal
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype in each sample in log scale
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior  
    }

    transformed data {
      vector[L] logpH = log(pH);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower=-10,upper=10> bj; // log fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector<lower=-10,upper=10> [L] rai0; // random intercept AI
    }

    model {
      // include transformed parameters of no interest
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[L] esum; // reduce computation inverse logit (rai0 + bj)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lps; // help for mixed gaussians
      vector[L] p = rep_vector(1e-5, L); // ASE proportion

      // nb lik stuff
      real ebj = exp(bj); // avoid repeating same calculation
      real debj = ebj / (1 + ebj);
      real l1pebj = log1p(ebj) - log(2);
      vector[N] intercept = rep_vector(1e-5, N); // the genetic effect

      profile("priors") {
        // Priors
        theta ~ gamma(1, 0.1); //  mean 10 
        phi ~ gamma(1, 0.1);  // mean 10

        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
        
        // mean expression and covariates
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }
        rai0 ~ normal(ai0, sdai0);
      }
      
      profile("likelihood") {
        // Likelihood
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj;
          }
          if (g[i] == 2) {
            intercept[i] = bj;
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);

        pos = 1;
        esum = inv_logit(rai0 + bj);
        esum0 = inv_logit(rai0);
        
        for(i in 1:A) { // ASE
          if (s[i] == 1) {

            p[pos] = gase[i]==1 ? esum[pos] : esum0[pos];
            p[pos] = gase[i]==-1 ? 1-esum[pos] : p[pos];  // haplotype swap
            n[pos] ~ beta_binomial(m[i], p[pos] * theta, (1 - p[pos]) * theta);
          } else {
            for (r in pos:(pos+s[i]-1)) {
              
              p[r] = gase[i]==1 ? esum[r] : esum0[r];
              p[r] = gase[i]==-1 ? 1-esum[r] : p[r];  // haplotype swap

              ltmp[r] = beta_binomial_lpmf(
                n[r] | m[i], p[r] * theta, (1 - p[r]) * theta
              ) + logpH[r];
            }
            target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          }
          pos=pos + s[i];
        }
      }
    }
