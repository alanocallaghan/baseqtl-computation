Optimising noGT\_nb\_ase\_refbias
=================================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_noGT_nb_ase_refbias_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7246495</td>
<td style="text-align: right;">6.7242250</td>
<td style="text-align: right;">0.0486505</td>
<td style="text-align: right;">0.0482438</td>
<td style="text-align: right;">6.6453285</td>
<td style="text-align: right;">6.8052115</td>
<td style="text-align: right;">1.0001177</td>
<td style="text-align: right;">36181.96</td>
<td style="text-align: right;">15705.66</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2117680</td>
<td style="text-align: right;">0.2109980</td>
<td style="text-align: right;">0.0482011</td>
<td style="text-align: right;">0.0484032</td>
<td style="text-align: right;">0.1334241</td>
<td style="text-align: right;">0.2915393</td>
<td style="text-align: right;">1.0000949</td>
<td style="text-align: right;">31915.27</td>
<td style="text-align: right;">15089.90</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0126473</td>
<td style="text-align: right;">0.0125608</td>
<td style="text-align: right;">0.0271672</td>
<td style="text-align: right;">0.0268040</td>
<td style="text-align: right;">-0.0318572</td>
<td style="text-align: right;">0.0571622</td>
<td style="text-align: right;">0.9999670</td>
<td style="text-align: right;">32785.94</td>
<td style="text-align: right;">14882.88</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.4692979</td>
<td style="text-align: right;">8.3524650</td>
<td style="text-align: right;">1.6206536</td>
<td style="text-align: right;">1.6041213</td>
<td style="text-align: right;">5.9904525</td>
<td style="text-align: right;">11.2702300</td>
<td style="text-align: right;">1.0002381</td>
<td style="text-align: right;">34656.42</td>
<td style="text-align: right;">15881.43</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">61.1241774</td>
<td style="text-align: right;">58.8885000</td>
<td style="text-align: right;">18.0491918</td>
<td style="text-align: right;">17.0898561</td>
<td style="text-align: right;">35.6832550</td>
<td style="text-align: right;">94.0368150</td>
<td style="text-align: right;">1.0002152</td>
<td style="text-align: right;">36754.77</td>
<td style="text-align: right;">14030.97</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[1]</td>
<td style="text-align: right;">0.0426196</td>
<td style="text-align: right;">0.0426350</td>
<td style="text-align: right;">0.0102968</td>
<td style="text-align: right;">0.0103492</td>
<td style="text-align: right;">0.0256618</td>
<td style="text-align: right;">0.0596555</td>
<td style="text-align: right;">0.9999679</td>
<td style="text-align: right;">35610.83</td>
<td style="text-align: right;">15956.85</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[2]</td>
<td style="text-align: right;">0.0430436</td>
<td style="text-align: right;">0.0430646</td>
<td style="text-align: right;">0.0102483</td>
<td style="text-align: right;">0.0102685</td>
<td style="text-align: right;">0.0263797</td>
<td style="text-align: right;">0.0599125</td>
<td style="text-align: right;">1.0005742</td>
<td style="text-align: right;">35274.28</td>
<td style="text-align: right;">14488.87</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[3]</td>
<td style="text-align: right;">-0.0425471</td>
<td style="text-align: right;">-0.0424867</td>
<td style="text-align: right;">0.0102914</td>
<td style="text-align: right;">0.0102611</td>
<td style="text-align: right;">-0.0595400</td>
<td style="text-align: right;">-0.0255151</td>
<td style="text-align: right;">1.0001680</td>
<td style="text-align: right;">36815.46</td>
<td style="text-align: right;">14587.68</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[4]</td>
<td style="text-align: right;">-0.0428941</td>
<td style="text-align: right;">-0.0428285</td>
<td style="text-align: right;">0.0103114</td>
<td style="text-align: right;">0.0103530</td>
<td style="text-align: right;">-0.0598963</td>
<td style="text-align: right;">-0.0258939</td>
<td style="text-align: right;">1.0004830</td>
<td style="text-align: right;">32131.23</td>
<td style="text-align: right;">14286.56</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[5]</td>
<td style="text-align: right;">-0.0424184</td>
<td style="text-align: right;">-0.0424359</td>
<td style="text-align: right;">0.0102852</td>
<td style="text-align: right;">0.0103193</td>
<td style="text-align: right;">-0.0591452</td>
<td style="text-align: right;">-0.0256318</td>
<td style="text-align: right;">1.0006534</td>
<td style="text-align: right;">35157.17</td>
<td style="text-align: right;">14563.61</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[6]</td>
<td style="text-align: right;">0.0432640</td>
<td style="text-align: right;">0.0432224</td>
<td style="text-align: right;">0.0100892</td>
<td style="text-align: right;">0.0100429</td>
<td style="text-align: right;">0.0267504</td>
<td style="text-align: right;">0.0599037</td>
<td style="text-align: right;">1.0002196</td>
<td style="text-align: right;">33893.25</td>
<td style="text-align: right;">13818.25</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[7]</td>
<td style="text-align: right;">-0.0424663</td>
<td style="text-align: right;">-0.0425394</td>
<td style="text-align: right;">0.0102184</td>
<td style="text-align: right;">0.0102571</td>
<td style="text-align: right;">-0.0591364</td>
<td style="text-align: right;">-0.0255910</td>
<td style="text-align: right;">0.9999876</td>
<td style="text-align: right;">36166.09</td>
<td style="text-align: right;">13593.53</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[8]</td>
<td style="text-align: right;">-0.0426428</td>
<td style="text-align: right;">-0.0426494</td>
<td style="text-align: right;">0.0102833</td>
<td style="text-align: right;">0.0103022</td>
<td style="text-align: right;">-0.0593679</td>
<td style="text-align: right;">-0.0256753</td>
<td style="text-align: right;">1.0001549</td>
<td style="text-align: right;">36841.37</td>
<td style="text-align: right;">14678.63</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[9]</td>
<td style="text-align: right;">-0.0426128</td>
<td style="text-align: right;">-0.0426978</td>
<td style="text-align: right;">0.0101595</td>
<td style="text-align: right;">0.0101630</td>
<td style="text-align: right;">-0.0591475</td>
<td style="text-align: right;">-0.0258401</td>
<td style="text-align: right;">1.0001422</td>
<td style="text-align: right;">37366.80</td>
<td style="text-align: right;">13402.10</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[10]</td>
<td style="text-align: right;">0.0430691</td>
<td style="text-align: right;">0.0431038</td>
<td style="text-align: right;">0.0103099</td>
<td style="text-align: right;">0.0102175</td>
<td style="text-align: right;">0.0260398</td>
<td style="text-align: right;">0.0600701</td>
<td style="text-align: right;">1.0004852</td>
<td style="text-align: right;">34773.13</td>
<td style="text-align: right;">14863.10</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[11]</td>
<td style="text-align: right;">0.0428856</td>
<td style="text-align: right;">0.0428173</td>
<td style="text-align: right;">0.0104557</td>
<td style="text-align: right;">0.0105498</td>
<td style="text-align: right;">0.0257944</td>
<td style="text-align: right;">0.0599712</td>
<td style="text-align: right;">1.0006119</td>
<td style="text-align: right;">37685.42</td>
<td style="text-align: right;">15301.89</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[12]</td>
<td style="text-align: right;">0.0432589</td>
<td style="text-align: right;">0.0432454</td>
<td style="text-align: right;">0.0102409</td>
<td style="text-align: right;">0.0100476</td>
<td style="text-align: right;">0.0264284</td>
<td style="text-align: right;">0.0601256</td>
<td style="text-align: right;">1.0003047</td>
<td style="text-align: right;">36639.20</td>
<td style="text-align: right;">15185.05</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[13]</td>
<td style="text-align: right;">0.0432066</td>
<td style="text-align: right;">0.0431276</td>
<td style="text-align: right;">0.0103149</td>
<td style="text-align: right;">0.0103081</td>
<td style="text-align: right;">0.0262848</td>
<td style="text-align: right;">0.0601501</td>
<td style="text-align: right;">1.0001806</td>
<td style="text-align: right;">35992.87</td>
<td style="text-align: right;">13682.43</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[14]</td>
<td style="text-align: right;">0.0429606</td>
<td style="text-align: right;">0.0429892</td>
<td style="text-align: right;">0.0103233</td>
<td style="text-align: right;">0.0103466</td>
<td style="text-align: right;">0.0258751</td>
<td style="text-align: right;">0.0599524</td>
<td style="text-align: right;">1.0000112</td>
<td style="text-align: right;">39857.17</td>
<td style="text-align: right;">14651.88</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[15]</td>
<td style="text-align: right;">0.0427474</td>
<td style="text-align: right;">0.0427714</td>
<td style="text-align: right;">0.0102640</td>
<td style="text-align: right;">0.0102519</td>
<td style="text-align: right;">0.0258273</td>
<td style="text-align: right;">0.0598005</td>
<td style="text-align: right;">1.0002018</td>
<td style="text-align: right;">35539.10</td>
<td style="text-align: right;">14436.73</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[16]</td>
<td style="text-align: right;">0.0424351</td>
<td style="text-align: right;">0.0424376</td>
<td style="text-align: right;">0.0100639</td>
<td style="text-align: right;">0.0101319</td>
<td style="text-align: right;">0.0260184</td>
<td style="text-align: right;">0.0588198</td>
<td style="text-align: right;">1.0003057</td>
<td style="text-align: right;">34011.13</td>
<td style="text-align: right;">14949.23</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[17]</td>
<td style="text-align: right;">-0.0429002</td>
<td style="text-align: right;">-0.0428501</td>
<td style="text-align: right;">0.0101382</td>
<td style="text-align: right;">0.0100995</td>
<td style="text-align: right;">-0.0596423</td>
<td style="text-align: right;">-0.0262874</td>
<td style="text-align: right;">0.9999758</td>
<td style="text-align: right;">38349.09</td>
<td style="text-align: right;">13519.80</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[18]</td>
<td style="text-align: right;">0.0426899</td>
<td style="text-align: right;">0.0426388</td>
<td style="text-align: right;">0.0102376</td>
<td style="text-align: right;">0.0102117</td>
<td style="text-align: right;">0.0256897</td>
<td style="text-align: right;">0.0596189</td>
<td style="text-align: right;">1.0003330</td>
<td style="text-align: right;">36794.49</td>
<td style="text-align: right;">14718.98</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[19]</td>
<td style="text-align: right;">-0.0430579</td>
<td style="text-align: right;">-0.0430570</td>
<td style="text-align: right;">0.0100340</td>
<td style="text-align: right;">0.0099983</td>
<td style="text-align: right;">-0.0594931</td>
<td style="text-align: right;">-0.0267260</td>
<td style="text-align: right;">1.0007527</td>
<td style="text-align: right;">36369.94</td>
<td style="text-align: right;">14811.12</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[20]</td>
<td style="text-align: right;">0.0431250</td>
<td style="text-align: right;">0.0431060</td>
<td style="text-align: right;">0.0102712</td>
<td style="text-align: right;">0.0102634</td>
<td style="text-align: right;">0.0261775</td>
<td style="text-align: right;">0.0600348</td>
<td style="text-align: right;">1.0001340</td>
<td style="text-align: right;">35559.73</td>
<td style="text-align: right;">14525.06</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[21]</td>
<td style="text-align: right;">-0.0424469</td>
<td style="text-align: right;">-0.0424994</td>
<td style="text-align: right;">0.0103565</td>
<td style="text-align: right;">0.0104042</td>
<td style="text-align: right;">-0.0594987</td>
<td style="text-align: right;">-0.0254292</td>
<td style="text-align: right;">1.0004955</td>
<td style="text-align: right;">37333.92</td>
<td style="text-align: right;">15483.08</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[22]</td>
<td style="text-align: right;">-0.0430546</td>
<td style="text-align: right;">-0.0430518</td>
<td style="text-align: right;">0.0101290</td>
<td style="text-align: right;">0.0100091</td>
<td style="text-align: right;">-0.0595978</td>
<td style="text-align: right;">-0.0265827</td>
<td style="text-align: right;">1.0001075</td>
<td style="text-align: right;">36473.84</td>
<td style="text-align: right;">14365.38</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[23]</td>
<td style="text-align: right;">-0.0430297</td>
<td style="text-align: right;">-0.0430333</td>
<td style="text-align: right;">0.0102427</td>
<td style="text-align: right;">0.0103094</td>
<td style="text-align: right;">-0.0598368</td>
<td style="text-align: right;">-0.0262411</td>
<td style="text-align: right;">1.0005690</td>
<td style="text-align: right;">38919.87</td>
<td style="text-align: right;">15251.60</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[24]</td>
<td style="text-align: right;">-0.0429339</td>
<td style="text-align: right;">-0.0429932</td>
<td style="text-align: right;">0.0102342</td>
<td style="text-align: right;">0.0101706</td>
<td style="text-align: right;">-0.0597211</td>
<td style="text-align: right;">-0.0261176</td>
<td style="text-align: right;">1.0003923</td>
<td style="text-align: right;">37228.71</td>
<td style="text-align: right;">15040.80</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[25]</td>
<td style="text-align: right;">-0.0425653</td>
<td style="text-align: right;">-0.0425640</td>
<td style="text-align: right;">0.0101834</td>
<td style="text-align: right;">0.0101924</td>
<td style="text-align: right;">-0.0592378</td>
<td style="text-align: right;">-0.0257990</td>
<td style="text-align: right;">1.0002921</td>
<td style="text-align: right;">34678.00</td>
<td style="text-align: right;">14465.39</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[26]</td>
<td style="text-align: right;">-0.0431819</td>
<td style="text-align: right;">-0.0431775</td>
<td style="text-align: right;">0.0102489</td>
<td style="text-align: right;">0.0101471</td>
<td style="text-align: right;">-0.0599630</td>
<td style="text-align: right;">-0.0263974</td>
<td style="text-align: right;">1.0003417</td>
<td style="text-align: right;">36298.76</td>
<td style="text-align: right;">13335.45</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[27]</td>
<td style="text-align: right;">-0.0429931</td>
<td style="text-align: right;">-0.0429732</td>
<td style="text-align: right;">0.0101423</td>
<td style="text-align: right;">0.0100228</td>
<td style="text-align: right;">-0.0597389</td>
<td style="text-align: right;">-0.0261402</td>
<td style="text-align: right;">1.0003363</td>
<td style="text-align: right;">35497.31</td>
<td style="text-align: right;">14650.13</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[28]</td>
<td style="text-align: right;">0.0424681</td>
<td style="text-align: right;">0.0424398</td>
<td style="text-align: right;">0.0102544</td>
<td style="text-align: right;">0.0102187</td>
<td style="text-align: right;">0.0256976</td>
<td style="text-align: right;">0.0593235</td>
<td style="text-align: right;">1.0002867</td>
<td style="text-align: right;">34438.34</td>
<td style="text-align: right;">14640.97</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[29]</td>
<td style="text-align: right;">-0.0428787</td>
<td style="text-align: right;">-0.0429180</td>
<td style="text-align: right;">0.0104048</td>
<td style="text-align: right;">0.0104986</td>
<td style="text-align: right;">-0.0601390</td>
<td style="text-align: right;">-0.0259443</td>
<td style="text-align: right;">1.0000518</td>
<td style="text-align: right;">33809.92</td>
<td style="text-align: right;">15503.82</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[30]</td>
<td style="text-align: right;">-0.0428251</td>
<td style="text-align: right;">-0.0428931</td>
<td style="text-align: right;">0.0101686</td>
<td style="text-align: right;">0.0101040</td>
<td style="text-align: right;">-0.0595119</td>
<td style="text-align: right;">-0.0259911</td>
<td style="text-align: right;">1.0002871</td>
<td style="text-align: right;">36046.97</td>
<td style="text-align: right;">14200.57</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[31]</td>
<td style="text-align: right;">0.0427545</td>
<td style="text-align: right;">0.0427639</td>
<td style="text-align: right;">0.0101396</td>
<td style="text-align: right;">0.0103135</td>
<td style="text-align: right;">0.0259586</td>
<td style="text-align: right;">0.0594288</td>
<td style="text-align: right;">1.0002199</td>
<td style="text-align: right;">37331.88</td>
<td style="text-align: right;">15499.43</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[32]</td>
<td style="text-align: right;">-0.0428265</td>
<td style="text-align: right;">-0.0428610</td>
<td style="text-align: right;">0.0101546</td>
<td style="text-align: right;">0.0101640</td>
<td style="text-align: right;">-0.0595241</td>
<td style="text-align: right;">-0.0260513</td>
<td style="text-align: right;">1.0002109</td>
<td style="text-align: right;">37544.74</td>
<td style="text-align: right;">14544.74</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[33]</td>
<td style="text-align: right;">-0.0423753</td>
<td style="text-align: right;">-0.0422852</td>
<td style="text-align: right;">0.0103030</td>
<td style="text-align: right;">0.0102780</td>
<td style="text-align: right;">-0.0594944</td>
<td style="text-align: right;">-0.0254085</td>
<td style="text-align: right;">1.0001115</td>
<td style="text-align: right;">36192.54</td>
<td style="text-align: right;">14828.34</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[34]</td>
<td style="text-align: right;">-0.0428663</td>
<td style="text-align: right;">-0.0428535</td>
<td style="text-align: right;">0.0102914</td>
<td style="text-align: right;">0.0103772</td>
<td style="text-align: right;">-0.0598944</td>
<td style="text-align: right;">-0.0259637</td>
<td style="text-align: right;">1.0003422</td>
<td style="text-align: right;">37799.56</td>
<td style="text-align: right;">14870.50</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7254570</td>
<td style="text-align: right;">6.7245450</td>
<td style="text-align: right;">0.0481334</td>
<td style="text-align: right;">0.0474210</td>
<td style="text-align: right;">6.6476375</td>
<td style="text-align: right;">6.8049405</td>
<td style="text-align: right;">1.0000531</td>
<td style="text-align: right;">26276.72</td>
<td style="text-align: right;">15152.63</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2118197</td>
<td style="text-align: right;">0.2111350</td>
<td style="text-align: right;">0.0487797</td>
<td style="text-align: right;">0.0481363</td>
<td style="text-align: right;">0.1331652</td>
<td style="text-align: right;">0.2928034</td>
<td style="text-align: right;">1.0009949</td>
<td style="text-align: right;">25406.31</td>
<td style="text-align: right;">15380.82</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0124875</td>
<td style="text-align: right;">0.0122718</td>
<td style="text-align: right;">0.0273882</td>
<td style="text-align: right;">0.0267286</td>
<td style="text-align: right;">-0.0320898</td>
<td style="text-align: right;">0.0567809</td>
<td style="text-align: right;">0.9999995</td>
<td style="text-align: right;">28998.30</td>
<td style="text-align: right;">14898.80</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.4723432</td>
<td style="text-align: right;">8.3696750</td>
<td style="text-align: right;">1.6287175</td>
<td style="text-align: right;">1.5857519</td>
<td style="text-align: right;">5.9900500</td>
<td style="text-align: right;">11.3362200</td>
<td style="text-align: right;">1.0002611</td>
<td style="text-align: right;">27039.02</td>
<td style="text-align: right;">14585.13</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">60.8934520</td>
<td style="text-align: right;">58.7999000</td>
<td style="text-align: right;">17.6110839</td>
<td style="text-align: right;">16.6729490</td>
<td style="text-align: right;">35.9812400</td>
<td style="text-align: right;">93.0417500</td>
<td style="text-align: right;">1.0002150</td>
<td style="text-align: right;">27012.52</td>
<td style="text-align: right;">14647.39</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[1]</td>
<td style="text-align: right;">0.0425537</td>
<td style="text-align: right;">0.0425264</td>
<td style="text-align: right;">0.0102811</td>
<td style="text-align: right;">0.0103285</td>
<td style="text-align: right;">0.0258348</td>
<td style="text-align: right;">0.0593537</td>
<td style="text-align: right;">1.0003944</td>
<td style="text-align: right;">36522.62</td>
<td style="text-align: right;">14418.35</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[2]</td>
<td style="text-align: right;">0.0429644</td>
<td style="text-align: right;">0.0429324</td>
<td style="text-align: right;">0.0102126</td>
<td style="text-align: right;">0.0102672</td>
<td style="text-align: right;">0.0262131</td>
<td style="text-align: right;">0.0597174</td>
<td style="text-align: right;">0.9999960</td>
<td style="text-align: right;">38366.99</td>
<td style="text-align: right;">14286.57</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[3]</td>
<td style="text-align: right;">-0.0424695</td>
<td style="text-align: right;">-0.0424541</td>
<td style="text-align: right;">0.0101445</td>
<td style="text-align: right;">0.0100566</td>
<td style="text-align: right;">-0.0590441</td>
<td style="text-align: right;">-0.0258070</td>
<td style="text-align: right;">1.0002273</td>
<td style="text-align: right;">37540.95</td>
<td style="text-align: right;">14520.17</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[4]</td>
<td style="text-align: right;">-0.0428726</td>
<td style="text-align: right;">-0.0429077</td>
<td style="text-align: right;">0.0102843</td>
<td style="text-align: right;">0.0103283</td>
<td style="text-align: right;">-0.0596259</td>
<td style="text-align: right;">-0.0258982</td>
<td style="text-align: right;">0.9999811</td>
<td style="text-align: right;">40819.17</td>
<td style="text-align: right;">15177.58</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[5]</td>
<td style="text-align: right;">-0.0424249</td>
<td style="text-align: right;">-0.0424644</td>
<td style="text-align: right;">0.0102848</td>
<td style="text-align: right;">0.0102974</td>
<td style="text-align: right;">-0.0593634</td>
<td style="text-align: right;">-0.0255105</td>
<td style="text-align: right;">1.0004820</td>
<td style="text-align: right;">37100.17</td>
<td style="text-align: right;">13795.86</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[6]</td>
<td style="text-align: right;">0.0432000</td>
<td style="text-align: right;">0.0431663</td>
<td style="text-align: right;">0.0103633</td>
<td style="text-align: right;">0.0103563</td>
<td style="text-align: right;">0.0260690</td>
<td style="text-align: right;">0.0601721</td>
<td style="text-align: right;">1.0000769</td>
<td style="text-align: right;">41100.90</td>
<td style="text-align: right;">13383.88</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[7]</td>
<td style="text-align: right;">-0.0425023</td>
<td style="text-align: right;">-0.0425654</td>
<td style="text-align: right;">0.0102612</td>
<td style="text-align: right;">0.0102086</td>
<td style="text-align: right;">-0.0595456</td>
<td style="text-align: right;">-0.0255600</td>
<td style="text-align: right;">1.0002953</td>
<td style="text-align: right;">40633.78</td>
<td style="text-align: right;">15072.13</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[8]</td>
<td style="text-align: right;">-0.0426736</td>
<td style="text-align: right;">-0.0426939</td>
<td style="text-align: right;">0.0102724</td>
<td style="text-align: right;">0.0102772</td>
<td style="text-align: right;">-0.0596836</td>
<td style="text-align: right;">-0.0258757</td>
<td style="text-align: right;">0.9999634</td>
<td style="text-align: right;">36178.27</td>
<td style="text-align: right;">14959.56</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[9]</td>
<td style="text-align: right;">-0.0426589</td>
<td style="text-align: right;">-0.0426486</td>
<td style="text-align: right;">0.0102510</td>
<td style="text-align: right;">0.0101812</td>
<td style="text-align: right;">-0.0595906</td>
<td style="text-align: right;">-0.0258919</td>
<td style="text-align: right;">1.0008585</td>
<td style="text-align: right;">39408.44</td>
<td style="text-align: right;">15000.44</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[10]</td>
<td style="text-align: right;">0.0429894</td>
<td style="text-align: right;">0.0429780</td>
<td style="text-align: right;">0.0101270</td>
<td style="text-align: right;">0.0099173</td>
<td style="text-align: right;">0.0261764</td>
<td style="text-align: right;">0.0598473</td>
<td style="text-align: right;">1.0002595</td>
<td style="text-align: right;">37603.26</td>
<td style="text-align: right;">15158.31</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[11]</td>
<td style="text-align: right;">0.0428872</td>
<td style="text-align: right;">0.0428852</td>
<td style="text-align: right;">0.0101672</td>
<td style="text-align: right;">0.0101365</td>
<td style="text-align: right;">0.0260827</td>
<td style="text-align: right;">0.0596607</td>
<td style="text-align: right;">1.0002554</td>
<td style="text-align: right;">36902.23</td>
<td style="text-align: right;">14724.66</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[12]</td>
<td style="text-align: right;">0.0432638</td>
<td style="text-align: right;">0.0432380</td>
<td style="text-align: right;">0.0102416</td>
<td style="text-align: right;">0.0102674</td>
<td style="text-align: right;">0.0264165</td>
<td style="text-align: right;">0.0601272</td>
<td style="text-align: right;">1.0001621</td>
<td style="text-align: right;">42780.62</td>
<td style="text-align: right;">14766.69</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[13]</td>
<td style="text-align: right;">0.0432160</td>
<td style="text-align: right;">0.0432379</td>
<td style="text-align: right;">0.0103590</td>
<td style="text-align: right;">0.0103884</td>
<td style="text-align: right;">0.0262998</td>
<td style="text-align: right;">0.0602600</td>
<td style="text-align: right;">1.0000389</td>
<td style="text-align: right;">37237.11</td>
<td style="text-align: right;">14487.13</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[14]</td>
<td style="text-align: right;">0.0430957</td>
<td style="text-align: right;">0.0430752</td>
<td style="text-align: right;">0.0100595</td>
<td style="text-align: right;">0.0100849</td>
<td style="text-align: right;">0.0265956</td>
<td style="text-align: right;">0.0599801</td>
<td style="text-align: right;">1.0000958</td>
<td style="text-align: right;">37147.72</td>
<td style="text-align: right;">14817.76</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[15]</td>
<td style="text-align: right;">0.0426466</td>
<td style="text-align: right;">0.0426712</td>
<td style="text-align: right;">0.0101819</td>
<td style="text-align: right;">0.0101506</td>
<td style="text-align: right;">0.0259060</td>
<td style="text-align: right;">0.0593848</td>
<td style="text-align: right;">1.0007204</td>
<td style="text-align: right;">38775.29</td>
<td style="text-align: right;">15020.40</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[16]</td>
<td style="text-align: right;">0.0424228</td>
<td style="text-align: right;">0.0424131</td>
<td style="text-align: right;">0.0099871</td>
<td style="text-align: right;">0.0099314</td>
<td style="text-align: right;">0.0257710</td>
<td style="text-align: right;">0.0588712</td>
<td style="text-align: right;">1.0000345</td>
<td style="text-align: right;">39922.51</td>
<td style="text-align: right;">15435.98</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[17]</td>
<td style="text-align: right;">-0.0428946</td>
<td style="text-align: right;">-0.0429229</td>
<td style="text-align: right;">0.0102839</td>
<td style="text-align: right;">0.0101968</td>
<td style="text-align: right;">-0.0599232</td>
<td style="text-align: right;">-0.0259565</td>
<td style="text-align: right;">1.0006012</td>
<td style="text-align: right;">39542.62</td>
<td style="text-align: right;">14413.29</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[18]</td>
<td style="text-align: right;">0.0428065</td>
<td style="text-align: right;">0.0428636</td>
<td style="text-align: right;">0.0101965</td>
<td style="text-align: right;">0.0103264</td>
<td style="text-align: right;">0.0260505</td>
<td style="text-align: right;">0.0595610</td>
<td style="text-align: right;">1.0006969</td>
<td style="text-align: right;">40195.48</td>
<td style="text-align: right;">14821.50</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[19]</td>
<td style="text-align: right;">-0.0430829</td>
<td style="text-align: right;">-0.0430487</td>
<td style="text-align: right;">0.0101177</td>
<td style="text-align: right;">0.0101439</td>
<td style="text-align: right;">-0.0596707</td>
<td style="text-align: right;">-0.0265125</td>
<td style="text-align: right;">1.0001086</td>
<td style="text-align: right;">39270.22</td>
<td style="text-align: right;">14527.23</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[20]</td>
<td style="text-align: right;">0.0432323</td>
<td style="text-align: right;">0.0432330</td>
<td style="text-align: right;">0.0102343</td>
<td style="text-align: right;">0.0101920</td>
<td style="text-align: right;">0.0263608</td>
<td style="text-align: right;">0.0602674</td>
<td style="text-align: right;">1.0009162</td>
<td style="text-align: right;">40924.95</td>
<td style="text-align: right;">14791.25</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[21]</td>
<td style="text-align: right;">-0.0425580</td>
<td style="text-align: right;">-0.0425173</td>
<td style="text-align: right;">0.0103001</td>
<td style="text-align: right;">0.0103149</td>
<td style="text-align: right;">-0.0593856</td>
<td style="text-align: right;">-0.0255913</td>
<td style="text-align: right;">1.0002936</td>
<td style="text-align: right;">38642.88</td>
<td style="text-align: right;">14955.84</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[22]</td>
<td style="text-align: right;">-0.0429866</td>
<td style="text-align: right;">-0.0430031</td>
<td style="text-align: right;">0.0102670</td>
<td style="text-align: right;">0.0101704</td>
<td style="text-align: right;">-0.0599883</td>
<td style="text-align: right;">-0.0259540</td>
<td style="text-align: right;">1.0001290</td>
<td style="text-align: right;">37809.80</td>
<td style="text-align: right;">14405.28</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[23]</td>
<td style="text-align: right;">-0.0429645</td>
<td style="text-align: right;">-0.0429713</td>
<td style="text-align: right;">0.0101674</td>
<td style="text-align: right;">0.0101809</td>
<td style="text-align: right;">-0.0597084</td>
<td style="text-align: right;">-0.0260806</td>
<td style="text-align: right;">0.9999936</td>
<td style="text-align: right;">39119.00</td>
<td style="text-align: right;">15456.15</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[24]</td>
<td style="text-align: right;">-0.0429604</td>
<td style="text-align: right;">-0.0429477</td>
<td style="text-align: right;">0.0101437</td>
<td style="text-align: right;">0.0100857</td>
<td style="text-align: right;">-0.0596896</td>
<td style="text-align: right;">-0.0261607</td>
<td style="text-align: right;">1.0003723</td>
<td style="text-align: right;">38981.28</td>
<td style="text-align: right;">14251.07</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[25]</td>
<td style="text-align: right;">-0.0426187</td>
<td style="text-align: right;">-0.0426350</td>
<td style="text-align: right;">0.0100600</td>
<td style="text-align: right;">0.0099703</td>
<td style="text-align: right;">-0.0592119</td>
<td style="text-align: right;">-0.0260179</td>
<td style="text-align: right;">1.0002647</td>
<td style="text-align: right;">39657.68</td>
<td style="text-align: right;">13577.92</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[26]</td>
<td style="text-align: right;">-0.0432010</td>
<td style="text-align: right;">-0.0432874</td>
<td style="text-align: right;">0.0102811</td>
<td style="text-align: right;">0.0102229</td>
<td style="text-align: right;">-0.0599536</td>
<td style="text-align: right;">-0.0261499</td>
<td style="text-align: right;">1.0000910</td>
<td style="text-align: right;">39860.25</td>
<td style="text-align: right;">14286.72</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[27]</td>
<td style="text-align: right;">-0.0430208</td>
<td style="text-align: right;">-0.0430391</td>
<td style="text-align: right;">0.0101154</td>
<td style="text-align: right;">0.0102325</td>
<td style="text-align: right;">-0.0597169</td>
<td style="text-align: right;">-0.0261957</td>
<td style="text-align: right;">1.0000586</td>
<td style="text-align: right;">40820.91</td>
<td style="text-align: right;">14538.25</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[28]</td>
<td style="text-align: right;">0.0423693</td>
<td style="text-align: right;">0.0424001</td>
<td style="text-align: right;">0.0102281</td>
<td style="text-align: right;">0.0103569</td>
<td style="text-align: right;">0.0254706</td>
<td style="text-align: right;">0.0590785</td>
<td style="text-align: right;">1.0001006</td>
<td style="text-align: right;">40527.80</td>
<td style="text-align: right;">14915.28</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[29]</td>
<td style="text-align: right;">-0.0427788</td>
<td style="text-align: right;">-0.0427910</td>
<td style="text-align: right;">0.0102047</td>
<td style="text-align: right;">0.0101831</td>
<td style="text-align: right;">-0.0594741</td>
<td style="text-align: right;">-0.0259781</td>
<td style="text-align: right;">1.0002799</td>
<td style="text-align: right;">39734.34</td>
<td style="text-align: right;">15609.34</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[30]</td>
<td style="text-align: right;">-0.0428221</td>
<td style="text-align: right;">-0.0427970</td>
<td style="text-align: right;">0.0101815</td>
<td style="text-align: right;">0.0102902</td>
<td style="text-align: right;">-0.0595153</td>
<td style="text-align: right;">-0.0262200</td>
<td style="text-align: right;">1.0004803</td>
<td style="text-align: right;">36900.33</td>
<td style="text-align: right;">14392.19</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[31]</td>
<td style="text-align: right;">0.0427073</td>
<td style="text-align: right;">0.0427843</td>
<td style="text-align: right;">0.0103035</td>
<td style="text-align: right;">0.0102640</td>
<td style="text-align: right;">0.0256650</td>
<td style="text-align: right;">0.0595592</td>
<td style="text-align: right;">1.0002215</td>
<td style="text-align: right;">39286.10</td>
<td style="text-align: right;">14292.71</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[32]</td>
<td style="text-align: right;">-0.0428166</td>
<td style="text-align: right;">-0.0427777</td>
<td style="text-align: right;">0.0101821</td>
<td style="text-align: right;">0.0103365</td>
<td style="text-align: right;">-0.0593766</td>
<td style="text-align: right;">-0.0262209</td>
<td style="text-align: right;">1.0007992</td>
<td style="text-align: right;">40088.55</td>
<td style="text-align: right;">15148.49</td>
</tr>
<tr class="even">
<td style="text-align: left;">rai0[33]</td>
<td style="text-align: right;">-0.0423570</td>
<td style="text-align: right;">-0.0423159</td>
<td style="text-align: right;">0.0103480</td>
<td style="text-align: right;">0.0103561</td>
<td style="text-align: right;">-0.0592766</td>
<td style="text-align: right;">-0.0253840</td>
<td style="text-align: right;">1.0002218</td>
<td style="text-align: right;">39620.84</td>
<td style="text-align: right;">15274.05</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rai0[34]</td>
<td style="text-align: right;">-0.0430136</td>
<td style="text-align: right;">-0.0429290</td>
<td style="text-align: right;">0.0101391</td>
<td style="text-align: right;">0.0102359</td>
<td style="text-align: right;">-0.0596751</td>
<td style="text-align: right;">-0.0262952</td>
<td style="text-align: right;">1.0005156</td>
<td style="text-align: right;">42072.52</td>
<td style="text-align: right;">15692.46</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139851117811520</td>
<td style="text-align: right;">7.565630</td>
<td style="text-align: right;">7.036030</td>
<td style="text-align: right;">0.5295970</td>
<td style="text-align: right;">51275139</td>
<td style="text-align: right;">17147744</td>
<td style="text-align: right;">82834</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">9.13e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139851117811520</td>
<td style="text-align: right;">0.257895</td>
<td style="text-align: right;">0.230701</td>
<td style="text-align: right;">0.0271948</td>
<td style="text-align: right;">3562249</td>
<td style="text-align: right;">82843</td>
<td style="text-align: right;">82834</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">3.10e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140334797764416</td>
<td style="text-align: right;">7.424410</td>
<td style="text-align: right;">6.862880</td>
<td style="text-align: right;">0.5615320</td>
<td style="text-align: right;">51438901</td>
<td style="text-align: right;">17202461</td>
<td style="text-align: right;">83099</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">8.93e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140334797764416</td>
<td style="text-align: right;">0.224653</td>
<td style="text-align: right;">0.191785</td>
<td style="text-align: right;">0.0328685</td>
<td style="text-align: right;">3573601</td>
<td style="text-align: right;">83107</td>
<td style="text-align: right;">83099</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.70e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140141721311040</td>
<td style="text-align: right;">7.601950</td>
<td style="text-align: right;">7.007730</td>
<td style="text-align: right;">0.5942200</td>
<td style="text-align: right;">50994109</td>
<td style="text-align: right;">17053766</td>
<td style="text-align: right;">82380</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">9.23e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140141721311040</td>
<td style="text-align: right;">0.261756</td>
<td style="text-align: right;">0.220361</td>
<td style="text-align: right;">0.0413954</td>
<td style="text-align: right;">3542727</td>
<td style="text-align: right;">82389</td>
<td style="text-align: right;">82380</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">3.20e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140599730521920</td>
<td style="text-align: right;">7.672670</td>
<td style="text-align: right;">7.093410</td>
<td style="text-align: right;">0.5792600</td>
<td style="text-align: right;">51208203</td>
<td style="text-align: right;">17125381</td>
<td style="text-align: right;">82726</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">9.27e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140599730521920</td>
<td style="text-align: right;">0.232157</td>
<td style="text-align: right;">0.202641</td>
<td style="text-align: right;">0.0295168</td>
<td style="text-align: right;">3557605</td>
<td style="text-align: right;">82735</td>
<td style="text-align: right;">82726</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.80e-06</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140241703880512</td>
<td style="text-align: right;">7.354420</td>
<td style="text-align: right;">6.835560</td>
<td style="text-align: right;">0.5188550</td>
<td style="text-align: right;">45302335</td>
<td style="text-align: right;">17593965</td>
<td style="text-align: right;">84995</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">8.65e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140241703880512</td>
<td style="text-align: right;">0.247401</td>
<td style="text-align: right;">0.216949</td>
<td style="text-align: right;">0.0304519</td>
<td style="text-align: right;">3654785</td>
<td style="text-align: right;">84995</td>
<td style="text-align: right;">84995</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.90e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140474894239552</td>
<td style="text-align: right;">7.570000</td>
<td style="text-align: right;">7.058220</td>
<td style="text-align: right;">0.5117850</td>
<td style="text-align: right;">45139770</td>
<td style="text-align: right;">17530830</td>
<td style="text-align: right;">84690</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">8.94e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140474894239552</td>
<td style="text-align: right;">0.248441</td>
<td style="text-align: right;">0.214857</td>
<td style="text-align: right;">0.0335837</td>
<td style="text-align: right;">3641670</td>
<td style="text-align: right;">84690</td>
<td style="text-align: right;">84690</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.90e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140615144204096</td>
<td style="text-align: right;">7.626000</td>
<td style="text-align: right;">7.052750</td>
<td style="text-align: right;">0.5732520</td>
<td style="text-align: right;">45128577</td>
<td style="text-align: right;">17526483</td>
<td style="text-align: right;">84669</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">9.01e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140615144204096</td>
<td style="text-align: right;">0.232908</td>
<td style="text-align: right;">0.203782</td>
<td style="text-align: right;">0.0291264</td>
<td style="text-align: right;">3640767</td>
<td style="text-align: right;">84669</td>
<td style="text-align: right;">84669</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.80e-06</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140658668263232</td>
<td style="text-align: right;">7.595230</td>
<td style="text-align: right;">7.038140</td>
<td style="text-align: right;">0.5570950</td>
<td style="text-align: right;">45500611</td>
<td style="text-align: right;">17670969</td>
<td style="text-align: right;">85367</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">8.90e-05</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140658668263232</td>
<td style="text-align: right;">0.250964</td>
<td style="text-align: right;">0.222241</td>
<td style="text-align: right;">0.0287233</td>
<td style="text-align: right;">3670781</td>
<td style="text-align: right;">85367</td>
<td style="text-align: right;">85367</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2.90e-06</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">9.18e-05</td>
<td style="text-align: right;">7.583790</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">3.00e-06</td>
<td style="text-align: right;">0.245026</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">8.92e-05</td>
<td style="text-align: right;">7.582615</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">2.90e-06</td>
<td style="text-align: right;">0.247921</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error and reference bias correction with random intercept model. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Allows for any mixture of gaussians for bj prior (eQTL effect).
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) , ai0 and sdai0
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype for each sample in log scale
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
     
      
    }


    transformed data {

      int Max; // maximun number of elements in h2g
      Max = max(h2g);

    }


    parameters {
      vector[K] betas; // regression param
      real bj; // log fold change ASE
      real<lower=0> phi; //overdipersion param for neg binom
      real<lower=0> theta; //the overdispersion parameter for beta binomial
      vector[L] rai0; // random intercept AI
      

    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      vector[L] esum; // reduce computation inverse logit (rai0 + bj)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        betas[1] ~ normal(6,4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0,2.5);//prior for the slopes following Gelman 2008
        }
        for(i in 1:L) {
          rai0[i] ~ normal(ai0[i], sdai0[i]);
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      profile("likelihood") {
        // local variables and transformed parameters of no interest
        pos = 1;  // to advance on NB terms
        posl = 1; // to advance on ASE terms
        ase = rep_vector(0,Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */
        
        ebj = exp(bj);
        lmu1 = cov[,2:cols(cov)]*betas;
        esum = inv_logit(rai0 + bj);
        esum0 = inv_logit(rai0);

        for(i in 1:N){ // lmu for each individual default to GT=0
          //print("i = ", i);
          
          for (r in pos:(pos+sNB[i]-1)) { // genotype level, Gi=g
            // print("gNB = ", gNB[r]," r = ", r);
            lmu = lmu1[i];

            lmu = fabs(gNB[r])==1 ? lmu + log1p(ebj)-log(2) : lmu;

            lmu = gNB[r]==2 ? lmu + bj : lmu;

            ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu), phi) + log(pNB[r]);

        
            if (ASEi[i, 1] == 1) {  // ASE info
              //print( " ase info ");
              for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                // print("x = ",x);
                //print("gase = ", gase[posl], " ph = ", pH[posl], " m = ", m[ASEi[i,2]]);
            
                p = gase[posl]==1 ? esum[posl] : esum0[posl];
                p = gase[posl]==-1 ? 1-esum[posl] : p;  // haplotype swap
                ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);

                posl += 1;
              }
              sAse = log_sum_exp(ase[1:h2g[r]]);
              target +=  log_sum_exp(ltmp[r] , sAse );
              
            }
          }
          if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            //print("i = ", i, " no ase info");
          }

          pos += sNB[i];
        }
      }
    }

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error and reference bias correction with random intercept model. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Allows for any mixture of gaussians for bj prior (eQTL effect).
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) , ai0 and sdai0
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype for each sample in log scale
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      vector[G] log_pNB = log(pNB);
      vector[L] log_pH = log(pH);
      Max = max(h2g);
    }

    parameters {
      vector[K] betas; // regression param
      real <lower=-10, upper=10> bj; // log fold change ASE
      real <lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
      real <lower=1e-5,upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector <lower=-10,upper=10> [L] rai0; // random intercept AI
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      vector[L] esum; // reduce computation inverse logit (rai0 + bj)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        theta ~ gamma(1, 0.1); //  based on stan code example
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008
        }
        for(i in 1:L) {
          rai0[i] ~ normal(ai0[i], sdai0[i]);
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      profile("likelihood") {
        // local variables and transformed parameters of no interest
        pos = 1;  // to advance on NB terms
        posl = 1; // to advance on ASE terms
        ase = rep_vector(0, Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */
        
        ebj = exp(bj);
        lmu1 = cov[, 2:cols(cov)] * betas;
        esum = inv_logit(rai0 + bj);
        esum0 = inv_logit(rai0);

        for(i in 1:N){ // lmu for each individual default to GT=0
          //print("i = ", i);
          
          for (r in pos:(pos+sNB[i]-1)) { // genotype level, Gi=g
            // print("gNB = ", gNB[r]," r = ", r);
            lmu = lmu1[i];

            lmu = fabs(gNB[r])==1 ? lmu + log1p(ebj) - log(2) : lmu;
            lmu = gNB[r]==2 ? lmu + bj : lmu;
            ltmp[r] = neg_binomial_2_log_lpmf(Y[i] | lmu, phi) + log_pNB[r];

            if (ASEi[i, 1] == 1) {  // ASE info
              //print( " ase info ");
              for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                // print("x = ",x);
                //print("gase = ", gase[posl], " ph = ", pH[posl], " m = ", m[ASEi[i,2]]);
            
                p = gase[posl]==1 ? esum[posl] : esum0[posl];
                p = gase[posl]==-1 ? 1-esum[posl] : p;  // haplotype swap
                ase[x] = beta_binomial_lpmf(
                  n[posl] | m[ASEi[i,2]], p * theta, (1 - p) * theta
                ) + log_pH[posl];

                posl += 1;
              }
              sAse = log_sum_exp(ase[1:h2g[r]]);
              target +=  log_sum_exp(ltmp[r] , sAse );
              
            }
          }
          if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            //print("i = ", i, " no ase info");
          }

          pos += sNB[i];
        }
      }
    }
