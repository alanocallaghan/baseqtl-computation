VB with noGT2T\_nb\_ase, tolerance: 0.01
========================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

![](md/cmdstan-vb_noGT2T_nb_ase_0.01_files/figure-markdown_strict/plot-draws-1.png)![](md/cmdstan-vb_noGT2T_nb_ase_0.01_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_noGT2T_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_noGT2T_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140433607653184</td>
<td style="text-align: right;">7.3624900</td>
<td style="text-align: right;">6.6819900</td>
<td style="text-align: right;">0.6805000</td>
<td style="text-align: right;">92682707</td>
<td style="text-align: right;">16905685</td>
<td style="text-align: right;">14445</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140433607653184</td>
<td style="text-align: right;">0.0138377</td>
<td style="text-align: right;">0.0104735</td>
<td style="text-align: right;">0.0033641</td>
<td style="text-align: right;">260244</td>
<td style="text-align: right;">28916</td>
<td style="text-align: right;">14445</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140049611786048</td>
<td style="text-align: right;">1.239340</td>
<td style="text-align: right;">1.1856200</td>
<td style="text-align: right;">0.0537282</td>
<td style="text-align: right;">7743574</td>
<td style="text-align: right;">1756975</td>
<td style="text-align: right;">1203</td>
<td style="text-align: right;">2601</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140049611786048</td>
<td style="text-align: right;">0.003027</td>
<td style="text-align: right;">0.0027455</td>
<td style="text-align: right;">0.0002814</td>
<td style="text-align: right;">39348</td>
<td style="text-align: right;">4372</td>
<td style="text-align: right;">1203</td>
<td style="text-align: right;">2601</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140633210615616</td>
<td style="text-align: right;">1.2071100</td>
<td style="text-align: right;">1.1553800</td>
<td style="text-align: right;">0.0517330</td>
<td style="text-align: right;">7193454</td>
<td style="text-align: right;">1497912</td>
<td style="text-align: right;">1055</td>
<td style="text-align: right;">2439</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140633210615616</td>
<td style="text-align: right;">0.0026592</td>
<td style="text-align: right;">0.0024143</td>
<td style="text-align: right;">0.0002449</td>
<td style="text-align: right;">29700</td>
<td style="text-align: right;">3300</td>
<td style="text-align: right;">1055</td>
<td style="text-align: right;">2439</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error, allowing for interaction term normal psoriasis, with or without covariates and ref bias correction version 2. Updated likelihood and mixed prior.
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) and ai0 
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      int I[N]; //indicator for skin all samples: 1=pso, 0=normal
      
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
      vector[G] log_pNB = log(pNB);
      vector[L] log_pH = log(pH);
    }

    parameters {
      real anorm; // mean expression normal tissue
      real apso; // mean expression pso tissues
      real ba; // log average-fold change ASE
      real bd; // log difference-fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector[K-1] betas; // regression parameters  
    }

    transformed parameters {
      real bp; // parameter of interest for psoriasis
      real bn; // parameter of interest for normal skin
        
      bp = ba + bd;
      bn = ba - bd;
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      vector[G] lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood

      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      real esum; // reduce computation inverse logit (bp or bn)
      vector[k] lpsa; // help for mixed gaussians for ba
      vector[k] lpsd; // help for mixed gaussians for bd
      
      vector[K] betasN; //regression parameters for normal skin
      vector[K] betasP; //regression parameters for pso skin
      vector[N] lmuN; //help linear pred normal skin
      vector[N] lmuP; //help linear pred pso skin
      
      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        anorm ~ normal(6,4); // mean expression normal skin, stan normal is mean and sd
        apso ~ normal(6,4); // mean expression pso skin
        for(i in 1:(K-1)) {
          betas[i] ~ cauchy(0,2.5);//prior for the covariates slopes following Gelman 2008
        }

        // mixture of gaussians for ba and bd:
        for(i in 1:k){
          lpsa[i] = normal_lpdf(ba | aveP[i], sdP[i]) + mixP[i];
          lpsd[i] = normal_lpdf(bd | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lpsa);
        target += log_sum_exp(lpsd);
      }
      // transformed parameters of no interest
      pos = 1; // to advance on NB terms
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0,Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */

      profile("likelihood") {
        betasN=append_row(anorm, betas); //betas for normal inds
        betasP=append_row(apso, betas); // betas for pso inds
        lmuN = cov[, 2:cols(cov)] * betasN; // will be used for normal inds (based on indicator)
        lmuP = cov[, 2:cols(cov)] * betasP; // for pso inds

        
        for(i in 1:N){ // lmu for each individual
          // check skin first
          if (I[i] == 1) { // psoriasis

            for (r in pos:(pos+sNB[i]-1)) { // then genotype
              
              lmu[r] = lmuP[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(exp(bp)) - log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bp : lmu[r];

              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
              
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g

                  esum = inv_logit(bp);

                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);
                target +=  log_sum_exp(ltmp[r] , sAse );
              }
          
            }
            if (ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
            
          } else {
            for (r in pos:(pos+sNB[i]-1)){ //normal skin
        
              lmu[r] = lmuN[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(exp(bn)) - log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bn : lmu[r];
              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                  esum = inv_logit(bn);
                  
                  p = gase[posl]==1 ? esum : 0.5;
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);   
                target +=  log_sum_exp(ltmp[r] , sAse );       
              }
            }
            if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }
            pos += sNB[i];  
          }       
        }
      }
    }
        
