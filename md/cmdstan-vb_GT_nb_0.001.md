VB with GT\_nb, tolerance: 0.001
================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

![](md/cmdstan-vb_GT_nb_0.001_files/figure-markdown_strict/plot-draws-1.png)![](md/cmdstan-vb_GT_nb_0.001_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_GT_nb_0.001_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_GT_nb_0.001_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140394204096320</td>
<td style="text-align: right;">0.1124170</td>
<td style="text-align: right;">0.1106390</td>
<td style="text-align: right;">0.0017780</td>
<td style="text-align: right;">60060</td>
<td style="text-align: right;">1291290</td>
<td style="text-align: right;">15015</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140394204096320</td>
<td style="text-align: right;">0.0052132</td>
<td style="text-align: right;">0.0043563</td>
<td style="text-align: right;">0.0008569</td>
<td style="text-align: right;">120120</td>
<td style="text-align: right;">15015</td>
<td style="text-align: right;">15015</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140568385763136</td>
<td style="text-align: right;">0.2172480</td>
<td style="text-align: right;">0.2153550</td>
<td style="text-align: right;">0.0018935</td>
<td style="text-align: right;">54404</td>
<td style="text-align: right;">1169686</td>
<td style="text-align: right;">13601</td>
<td style="text-align: right;">14901</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140568385763136</td>
<td style="text-align: right;">0.0109282</td>
<td style="text-align: right;">0.0099495</td>
<td style="text-align: right;">0.0009787</td>
<td style="text-align: right;">108808</td>
<td style="text-align: right;">13601</td>
<td style="text-align: right;">13601</td>
<td style="text-align: right;">14901</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140089503196992</td>
<td style="text-align: right;">0.317077</td>
<td style="text-align: right;">0.3140950</td>
<td style="text-align: right;">0.0029820</td>
<td style="text-align: right;">80932</td>
<td style="text-align: right;">1740038</td>
<td style="text-align: right;">20233</td>
<td style="text-align: right;">21501</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140089503196992</td>
<td style="text-align: right;">0.015877</td>
<td style="text-align: right;">0.0141629</td>
<td style="text-align: right;">0.0017141</td>
<td style="text-align: right;">161864</td>
<td style="text-align: right;">20233</td>
<td style="text-align: right;">20233</td>
<td style="text-align: right;">21501</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative  binomial for  eQTL with fixed genotypes. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      // simplex[k] expMixP; // log of mixing proportions for eQTL effect prior
    }

    parameters {
      vector[K] betas; // regression param
      real <lower=-10,upper=10> bj; // log fold change ASE
      // real bj; // log fold change ASE
      real <lower=1e-5> phi; //overdipersion param for neg binom
    }


    model {
      profile("priors") {
        // include transformed parameters of no interest
        vector[k] lps; // help for mixed gaussians

        // Priors
        phi ~ gamma(1, 0.01);

        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5); //prior for the slopes following Gelman 2008   
        }

        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
          // lps[i] = normal_lpdf(bj | aveP[i], sdP[i]);
        }
        target += log_sum_exp(lps);
        // suggestion from stan team discussions that one of these should be faster,
        // but they don't seem to be right now
        // target += log_sum_exp(lps + mixP);
        // target += log_mix(expMixP, lps);
      }
      profile("likelihood") {
        real l1pebj = log1p(exp(bj)) - log(2);
        vector[N] intercept = rep_vector(1e-5, N); // the genetic effect; 0 if hom ref
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj; // log1p(exp(b_afc)) - log(2) if he
          }
          if (g[i] == 2) {
            intercept[i] = bj; // b_afc if hom alt
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);
      }
    }
