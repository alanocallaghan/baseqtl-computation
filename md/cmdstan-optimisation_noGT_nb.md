Optimising noGT\_nb
===================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_noGT_nb_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7432167</td>
<td style="text-align: right;">6.7432100</td>
<td style="text-align: right;">0.0382828</td>
<td style="text-align: right;">0.0376432</td>
<td style="text-align: right;">6.6810190</td>
<td style="text-align: right;">6.8062520</td>
<td style="text-align: right;">1.000211</td>
<td style="text-align: right;">19612.07</td>
<td style="text-align: right;">15111.90</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2137598</td>
<td style="text-align: right;">0.2135150</td>
<td style="text-align: right;">0.0393236</td>
<td style="text-align: right;">0.0393912</td>
<td style="text-align: right;">0.1495224</td>
<td style="text-align: right;">0.2792479</td>
<td style="text-align: right;">1.000115</td>
<td style="text-align: right;">20813.90</td>
<td style="text-align: right;">15197.50</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0061061</td>
<td style="text-align: right;">-0.0054051</td>
<td style="text-align: right;">0.0341970</td>
<td style="text-align: right;">0.0307706</td>
<td style="text-align: right;">-0.0564821</td>
<td style="text-align: right;">0.0447901</td>
<td style="text-align: right;">1.000073</td>
<td style="text-align: right;">19808.18</td>
<td style="text-align: right;">11233.71</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.6020907</td>
<td style="text-align: right;">8.5440200</td>
<td style="text-align: right;">1.3024878</td>
<td style="text-align: right;">1.2903364</td>
<td style="text-align: right;">6.5664365</td>
<td style="text-align: right;">10.8389600</td>
<td style="text-align: right;">1.000086</td>
<td style="text-align: right;">19916.48</td>
<td style="text-align: right;">14555.10</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7437375</td>
<td style="text-align: right;">6.7436200</td>
<td style="text-align: right;">0.0382882</td>
<td style="text-align: right;">0.0377989</td>
<td style="text-align: right;">6.6811795</td>
<td style="text-align: right;">6.8066805</td>
<td style="text-align: right;">1.0002995</td>
<td style="text-align: right;">17783.63</td>
<td style="text-align: right;">13825.85</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2130531</td>
<td style="text-align: right;">0.2126750</td>
<td style="text-align: right;">0.0396116</td>
<td style="text-align: right;">0.0395098</td>
<td style="text-align: right;">0.1485194</td>
<td style="text-align: right;">0.2788722</td>
<td style="text-align: right;">1.0001819</td>
<td style="text-align: right;">18325.93</td>
<td style="text-align: right;">13934.85</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0069041</td>
<td style="text-align: right;">-0.0057285</td>
<td style="text-align: right;">0.0349035</td>
<td style="text-align: right;">0.0308821</td>
<td style="text-align: right;">-0.0578680</td>
<td style="text-align: right;">0.0439077</td>
<td style="text-align: right;">1.0001442</td>
<td style="text-align: right;">20149.74</td>
<td style="text-align: right;">10854.70</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.5992292</td>
<td style="text-align: right;">8.5275650</td>
<td style="text-align: right;">1.3022316</td>
<td style="text-align: right;">1.3144287</td>
<td style="text-align: right;">6.5887645</td>
<td style="text-align: right;">10.8479450</td>
<td style="text-align: right;">0.9999768</td>
<td style="text-align: right;">18507.39</td>
<td style="text-align: right;">14366.06</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139920848406336</td>
<td style="text-align: right;">11.695300</td>
<td style="text-align: right;">10.894400</td>
<td style="text-align: right;">0.8009130</td>
<td style="text-align: right;">23522202</td>
<td style="text-align: right;">11054965</td>
<td style="text-align: right;">64268</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001820</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139920848406336</td>
<td style="text-align: right;">0.168780</td>
<td style="text-align: right;">0.129827</td>
<td style="text-align: right;">0.0389535</td>
<td style="text-align: right;">514224</td>
<td style="text-align: right;">64278</td>
<td style="text-align: right;">64268</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000026</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139632738604864</td>
<td style="text-align: right;">13.111900</td>
<td style="text-align: right;">12.293100</td>
<td style="text-align: right;">0.8187880</td>
<td style="text-align: right;">25046160</td>
<td style="text-align: right;">11770992</td>
<td style="text-align: right;">68432</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001916</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139632738604864</td>
<td style="text-align: right;">0.205583</td>
<td style="text-align: right;">0.167159</td>
<td style="text-align: right;">0.0384239</td>
<td style="text-align: right;">547520</td>
<td style="text-align: right;">68440</td>
<td style="text-align: right;">68432</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000030</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140611858896704</td>
<td style="text-align: right;">11.103100</td>
<td style="text-align: right;">10.590200</td>
<td style="text-align: right;">0.5129140</td>
<td style="text-align: right;">22740726</td>
<td style="text-align: right;">10687564</td>
<td style="text-align: right;">62133</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001787</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140611858896704</td>
<td style="text-align: right;">0.136970</td>
<td style="text-align: right;">0.129293</td>
<td style="text-align: right;">0.0076770</td>
<td style="text-align: right;">497128</td>
<td style="text-align: right;">62141</td>
<td style="text-align: right;">62133</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000022</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140087479080768</td>
<td style="text-align: right;">13.180100</td>
<td style="text-align: right;">11.951600</td>
<td style="text-align: right;">1.2284500</td>
<td style="text-align: right;">22609338</td>
<td style="text-align: right;">10625902</td>
<td style="text-align: right;">61774</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0002134</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140087479080768</td>
<td style="text-align: right;">0.232494</td>
<td style="text-align: right;">0.165151</td>
<td style="text-align: right;">0.0673429</td>
<td style="text-align: right;">494264</td>
<td style="text-align: right;">61783</td>
<td style="text-align: right;">61774</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000038</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139974411798336</td>
<td style="text-align: right;">6.605700</td>
<td style="text-align: right;">6.3842400</td>
<td style="text-align: right;">0.2214650</td>
<td style="text-align: right;">8182784</td>
<td style="text-align: right;">5497808</td>
<td style="text-align: right;">63928</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001033</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139974411798336</td>
<td style="text-align: right;">0.145021</td>
<td style="text-align: right;">0.0975620</td>
<td style="text-align: right;">0.0474594</td>
<td style="text-align: right;">511424</td>
<td style="text-align: right;">63928</td>
<td style="text-align: right;">63928</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000023</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140001268795200</td>
<td style="text-align: right;">5.814770</td>
<td style="text-align: right;">5.5728400</td>
<td style="text-align: right;">0.2419360</td>
<td style="text-align: right;">7901440</td>
<td style="text-align: right;">5308780</td>
<td style="text-align: right;">61730</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000942</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140001268795200</td>
<td style="text-align: right;">0.122039</td>
<td style="text-align: right;">0.0991858</td>
<td style="text-align: right;">0.0228531</td>
<td style="text-align: right;">493840</td>
<td style="text-align: right;">61730</td>
<td style="text-align: right;">61730</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000020</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140226872969024</td>
<td style="text-align: right;">8.111100</td>
<td style="text-align: right;">7.8057900</td>
<td style="text-align: right;">0.3053090</td>
<td style="text-align: right;">10792960</td>
<td style="text-align: right;">7251520</td>
<td style="text-align: right;">84320</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000962</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140226872969024</td>
<td style="text-align: right;">0.138221</td>
<td style="text-align: right;">0.1189530</td>
<td style="text-align: right;">0.0192687</td>
<td style="text-align: right;">674560</td>
<td style="text-align: right;">84320</td>
<td style="text-align: right;">84320</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000016</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140599271016256</td>
<td style="text-align: right;">6.284580</td>
<td style="text-align: right;">6.0728400</td>
<td style="text-align: right;">0.2117400</td>
<td style="text-align: right;">8875392</td>
<td style="text-align: right;">5963154</td>
<td style="text-align: right;">69339</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000906</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140599271016256</td>
<td style="text-align: right;">0.101832</td>
<td style="text-align: right;">0.0782376</td>
<td style="text-align: right;">0.0235942</td>
<td style="text-align: right;">554712</td>
<td style="text-align: right;">69339</td>
<td style="text-align: right;">69339</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000015</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001868</td>
<td style="text-align: right;">12.4036000</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000028</td>
<td style="text-align: right;">0.1871815</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">9.52e-05</td>
<td style="text-align: right;">6.44514</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">1.80e-06</td>
<td style="text-align: right;">0.13013</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error and reference bias correction with random intercept model. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Allows for any mixture of gaussians for bj prior (eQTL effect).
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      matrix[N,1+K] cov; 
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }


    parameters {
      vector[K] betas; // regression param
      real<lower=-20,upper=20> bj; // log fold change ASE
      real<lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
    }

    model {
      int pos; // to advance through NB terms (1-G)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
      // local variables and transformed parameters of no interest
      pos = 1;  // to advance on NB terms
      
      profile("likelihood") {  
        ebj = exp(bj);
        lmu1 = cov[,2:cols(cov)]*betas;
        for(i in 1:N) { // lmu for each individual default to GT=0
          
          for (r in pos:(pos+sNB[i]-1)) { // genotype level, Gi=g
            // print("gNB = ", gNB[r]," r = ", r);
            lmu = lmu1[i];
            lmu = fabs(gNB[r])==1 ? lmu + log1p(ebj)-log(2) : lmu;
            lmu = gNB[r]==2 ? lmu + bj : lmu;

            ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu), phi) + log(pNB[r]);
          }
          target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          pos += sNB[i];
        }
      }
    }

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error and reference bias correction with random intercept model. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Allows for any mixture of gaussians for bj prior (eQTL effect).
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      matrix[N,1+K] cov; 
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      vector[G] log_pNB = log(pNB);
      vector[G] abs_gNB = fabs(gNB);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower=-20,upper=20> bj; // log fold change ASE
      real<lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
    }

    model {
      int pos; // to advance through NB terms (1-G)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008
        }

        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
      // local variables and transformed parameters of no interest
      pos = 1;  // to advance on NB terms
      
      profile("likelihood") {
        ebj = exp(bj);
        real l1pebj = log1p(ebj) - log(2);
        lmu1 = cov[, 2:cols(cov)] * betas;
        for(i in 1:N) { // lmu for each individual default to GT=0
          
          if (sNB[i] == 1) {
            lmu = lmu1[i];
            lmu = abs_gNB[pos] == 1 ? lmu + l1pebj : lmu;
            lmu = gNB[pos] == 2 ? lmu + bj : lmu;

            target += neg_binomial_2_log_lpmf(Y[i] | lmu, phi);
          } else {
            for (r in pos:(pos+sNB[i]-1)) { // genotype level, Gi=g
              // print("gNB = ", gNB[r]," r = ", r);
              lmu = lmu1[i];
              lmu = abs_gNB[r] == 1 ? lmu + l1pebj : lmu;
              lmu = gNB[r] == 2 ? lmu + bj : lmu;

              ltmp[r] = neg_binomial_2_log_lpmf(Y[i] | lmu, phi) + log_pNB[r];
            }
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          }
          pos += sNB[i];
        }
      }
    }
