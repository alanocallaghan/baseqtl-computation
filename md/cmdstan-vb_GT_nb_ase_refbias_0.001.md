VB with GT\_nb\_ase\_refbias, tolerance: 0.001
==============================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

    ## Warning: The ESS has been capped to avoid negative values.

    ## Warning: The ESS has been capped to avoid negative values.

    ## Warning: The ESS has been capped to avoid negative values.

![](md/cmdstan-vb_GT_nb_ase_refbias_0.001_files/figure-markdown_strict/plot-draws-1.png)

    ## Warning: The ESS has been capped to avoid negative values.

    ## Warning: The ESS has been capped to avoid negative values.

    ## Warning: The ESS has been capped to avoid negative values.

![](md/cmdstan-vb_GT_nb_ase_refbias_0.001_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_GT_nb_ase_refbias_0.001_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_GT_nb_ase_refbias_0.001_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140487320680256</td>
<td style="text-align: right;">0.5915970</td>
<td style="text-align: right;">0.5550070</td>
<td style="text-align: right;">0.0365895</td>
<td style="text-align: right;">6634272</td>
<td style="text-align: right;">1180939</td>
<td style="text-align: right;">18738</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140487320680256</td>
<td style="text-align: right;">0.0205192</td>
<td style="text-align: right;">0.0178853</td>
<td style="text-align: right;">0.0026339</td>
<td style="text-align: right;">187470</td>
<td style="text-align: right;">18747</td>
<td style="text-align: right;">18738</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139699799283520</td>
<td style="text-align: right;">0.9886990</td>
<td style="text-align: right;">0.9539520</td>
<td style="text-align: right;">0.0347475</td>
<td style="text-align: right;">7224018</td>
<td style="text-align: right;">1314533</td>
<td style="text-align: right;">20103</td>
<td style="text-align: right;">21501</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139699799283520</td>
<td style="text-align: right;">0.0391583</td>
<td style="text-align: right;">0.0364996</td>
<td style="text-align: right;">0.0026588</td>
<td style="text-align: right;">210830</td>
<td style="text-align: right;">21083</td>
<td style="text-align: right;">20103</td>
<td style="text-align: right;">21501</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140569018627904</td>
<td style="text-align: right;">1.0819800</td>
<td style="text-align: right;">1.0441700</td>
<td style="text-align: right;">0.0378034</td>
<td style="text-align: right;">7252340</td>
<td style="text-align: right;">1318827</td>
<td style="text-align: right;">20154</td>
<td style="text-align: right;">21606</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140569018627904</td>
<td style="text-align: right;">0.0450047</td>
<td style="text-align: right;">0.0421391</td>
<td style="text-align: right;">0.0028655</td>
<td style="text-align: right;">211380</td>
<td style="text-align: right;">21138</td>
<td style="text-align: right;">20154</td>
<td style="text-align: right;">21606</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance and reference panel bias correction including uncertainty in estimates (version2). Allows for any mixture of priors for eQTL estimate, including single normal
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype in each sample in log scale
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior  
    }

    transformed data {
      vector[L] logpH = log(pH);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower=-10,upper=10> bj; // log fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector[L] rai0; // random intercept AI
    }

    model {
      // include transformed parameters of no interest
      vector[L] p; // ASE proportion
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[L] esum; // reduce computation inverse logit (rai0 + bj)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lps; // help for mixed gaussians

      // nb lik stuff
      real ebj = exp(bj); // avoid repeating same calculation
      real debj = ebj / (1 + ebj);
      real l1pebj = log1p(ebj) - log(2);
      vector[N] intercept = rep_vector(0, N); // the genetic effect

      profile("priors") {
        // Priors
        theta ~ gamma(1,0.1); //  mean 10 
        phi ~ gamma(1,0.1);  // mean 10

        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
        
        // mean expression and covariates
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }
        rai0 ~ normal(ai0, sdai0);
      }
      
      profile("likelihood") {
        // Likelihood
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj;
          }
          if (g[i] == 2) {
            intercept[i] = bj;
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);

        pos = 1;
        esum = inv_logit(rai0 + bj);
        esum0 = inv_logit(rai0);
        
        for(i in 1:A) { // ASE
          if (s[i] == 1) {

            p[pos] = gase[i]==1 ? esum[pos] : esum0[pos];
            p[pos] = gase[i]==-1 ? 1-esum[pos] : p[pos];  // haplotype swap

            n[pos] ~ beta_binomial(m[i], p[pos] * theta, (1 - p[pos]) * theta);
          } else {
            for (r in pos:(pos+s[i]-1)) {
              
              p[r] = gase[i]==1 ? esum[r] : esum0[r];
              p[r] = gase[i]==-1 ? 1-esum[r] : p[r];  // haplotype swap
              
              ltmp[r] = beta_binomial_lpmf(
                n[r] | m[i], p[r] * theta, (1 - p[r]) * theta
              ) + logpH[r];
            }
            target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          }
          pos=pos + s[i];
        }
      }
    }
