Optimising GT\_nb\_ase
======================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

    ## Warning: Chain 1 finished unexpectedly!

    ## Warning: Chain 2 finished unexpectedly!

    ## Warning: 2 chain(s) finished unexpectedly!

    ## Warning: The returned fit object will only read in results of successful
    ## chains. Please use read_cmdstan_csv() to read the results of the failed chains
    ## separately.Use the $output(chain_id) method for more output of the failed
    ## chains.

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_GT_nb_ase_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7422766</td>
<td style="text-align: right;">6.7423250</td>
<td style="text-align: right;">0.0385805</td>
<td style="text-align: right;">0.0385031</td>
<td style="text-align: right;">6.6787390</td>
<td style="text-align: right;">6.8057820</td>
<td style="text-align: right;">1.0001488</td>
<td style="text-align: right;">10984.51</td>
<td style="text-align: right;">7267.493</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2134956</td>
<td style="text-align: right;">0.2128425</td>
<td style="text-align: right;">0.0402624</td>
<td style="text-align: right;">0.0397878</td>
<td style="text-align: right;">0.1485992</td>
<td style="text-align: right;">0.2804606</td>
<td style="text-align: right;">1.0004004</td>
<td style="text-align: right;">11944.08</td>
<td style="text-align: right;">8187.835</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0026111</td>
<td style="text-align: right;">-0.0024972</td>
<td style="text-align: right;">0.0285015</td>
<td style="text-align: right;">0.0281733</td>
<td style="text-align: right;">-0.0487057</td>
<td style="text-align: right;">0.0442911</td>
<td style="text-align: right;">1.0012826</td>
<td style="text-align: right;">11740.23</td>
<td style="text-align: right;">7893.483</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.5780823</td>
<td style="text-align: right;">8.5038000</td>
<td style="text-align: right;">1.2866683</td>
<td style="text-align: right;">1.2988095</td>
<td style="text-align: right;">6.5610785</td>
<td style="text-align: right;">10.7953150</td>
<td style="text-align: right;">0.9999628</td>
<td style="text-align: right;">11205.25</td>
<td style="text-align: right;">6959.809</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">46.3937806</td>
<td style="text-align: right;">45.0981000</td>
<td style="text-align: right;">12.4473470</td>
<td style="text-align: right;">12.1098768</td>
<td style="text-align: right;">28.4154550</td>
<td style="text-align: right;">69.1647000</td>
<td style="text-align: right;">1.0000750</td>
<td style="text-align: right;">11097.13</td>
<td style="text-align: right;">7327.013</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7426442</td>
<td style="text-align: right;">6.7424400</td>
<td style="text-align: right;">0.0381980</td>
<td style="text-align: right;">0.0380435</td>
<td style="text-align: right;">6.6810870</td>
<td style="text-align: right;">6.8054320</td>
<td style="text-align: right;">1.000268</td>
<td style="text-align: right;">23790.44</td>
<td style="text-align: right;">15488.26</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2134706</td>
<td style="text-align: right;">0.2128565</td>
<td style="text-align: right;">0.0401225</td>
<td style="text-align: right;">0.0403668</td>
<td style="text-align: right;">0.1476797</td>
<td style="text-align: right;">0.2802122</td>
<td style="text-align: right;">1.000315</td>
<td style="text-align: right;">24392.57</td>
<td style="text-align: right;">14393.16</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">-0.0028440</td>
<td style="text-align: right;">-0.0026743</td>
<td style="text-align: right;">0.0281601</td>
<td style="text-align: right;">0.0278025</td>
<td style="text-align: right;">-0.0491273</td>
<td style="text-align: right;">0.0433175</td>
<td style="text-align: right;">1.000002</td>
<td style="text-align: right;">26506.28</td>
<td style="text-align: right;">16040.95</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.5777357</td>
<td style="text-align: right;">8.5076000</td>
<td style="text-align: right;">1.2992407</td>
<td style="text-align: right;">1.2880236</td>
<td style="text-align: right;">6.5623955</td>
<td style="text-align: right;">10.8280300</td>
<td style="text-align: right;">1.000007</td>
<td style="text-align: right;">24984.10</td>
<td style="text-align: right;">15725.01</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">46.4964917</td>
<td style="text-align: right;">45.1079000</td>
<td style="text-align: right;">12.6181004</td>
<td style="text-align: right;">12.0645092</td>
<td style="text-align: right;">28.1459650</td>
<td style="text-align: right;">69.2554650</td>
<td style="text-align: right;">1.000209</td>
<td style="text-align: right;">25747.05</td>
<td style="text-align: right;">14854.54</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140432211203904</td>
<td style="text-align: right;">11.2114000</td>
<td style="text-align: right;">10.6040000</td>
<td style="text-align: right;">0.6074440</td>
<td style="text-align: right;">34244025</td>
<td style="text-align: right;">9566038</td>
<td style="text-align: right;">63769</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001758</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140432211203904</td>
<td style="text-align: right;">0.0795971</td>
<td style="text-align: right;">0.0624395</td>
<td style="text-align: right;">0.0171576</td>
<td style="text-align: right;">573993</td>
<td style="text-align: right;">63777</td>
<td style="text-align: right;">63769</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000012</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139684749268800</td>
<td style="text-align: right;">12.1919000</td>
<td style="text-align: right;">11.5377000</td>
<td style="text-align: right;">0.6541710</td>
<td style="text-align: right;">35393193</td>
<td style="text-align: right;">9886952</td>
<td style="text-align: right;">65909</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001850</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139684749268800</td>
<td style="text-align: right;">0.1119130</td>
<td style="text-align: right;">0.0978597</td>
<td style="text-align: right;">0.0140537</td>
<td style="text-align: right;">593244</td>
<td style="text-align: right;">65916</td>
<td style="text-align: right;">65909</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000017</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139873697486656</td>
<td style="text-align: right;">8.623220</td>
<td style="text-align: right;">8.153740</td>
<td style="text-align: right;">0.4694850</td>
<td style="text-align: right;">16539367</td>
<td style="text-align: right;">2879323</td>
<td style="text-align: right;">66961</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001288</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139873697486656</td>
<td style="text-align: right;">0.224408</td>
<td style="text-align: right;">0.199999</td>
<td style="text-align: right;">0.0244097</td>
<td style="text-align: right;">602649</td>
<td style="text-align: right;">66961</td>
<td style="text-align: right;">66961</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000034</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140221882177344</td>
<td style="text-align: right;">8.410900</td>
<td style="text-align: right;">7.966650</td>
<td style="text-align: right;">0.4442520</td>
<td style="text-align: right;">16174548</td>
<td style="text-align: right;">2815812</td>
<td style="text-align: right;">65484</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001284</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140221882177344</td>
<td style="text-align: right;">0.185440</td>
<td style="text-align: right;">0.175129</td>
<td style="text-align: right;">0.0103110</td>
<td style="text-align: right;">589356</td>
<td style="text-align: right;">65484</td>
<td style="text-align: right;">65484</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000028</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139654705583936</td>
<td style="text-align: right;">8.562230</td>
<td style="text-align: right;">8.182930</td>
<td style="text-align: right;">0.3792990</td>
<td style="text-align: right;">15685982</td>
<td style="text-align: right;">2730758</td>
<td style="text-align: right;">63506</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001348</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139654705583936</td>
<td style="text-align: right;">0.142156</td>
<td style="text-align: right;">0.122478</td>
<td style="text-align: right;">0.0196774</td>
<td style="text-align: right;">571554</td>
<td style="text-align: right;">63506</td>
<td style="text-align: right;">63506</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000022</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139787143362368</td>
<td style="text-align: right;">8.533050</td>
<td style="text-align: right;">8.051470</td>
<td style="text-align: right;">0.4815850</td>
<td style="text-align: right;">15288065</td>
<td style="text-align: right;">2661485</td>
<td style="text-align: right;">61895</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001379</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139787143362368</td>
<td style="text-align: right;">0.189161</td>
<td style="text-align: right;">0.150271</td>
<td style="text-align: right;">0.0388896</td>
<td style="text-align: right;">557055</td>
<td style="text-align: right;">61895</td>
<td style="text-align: right;">61895</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000031</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001804</td>
<td style="text-align: right;">11.7016500</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000015</td>
<td style="text-align: right;">0.0957551</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001318</td>
<td style="text-align: right;">8.5476400</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000029</td>
<td style="text-align: right;">0.1873005</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance, version 2. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior

      }

    parameters {
      vector[K] betas; // regression param
      real<lower=-10, upper=10> bj; // log fold change ASE
      real<lower=1e-5> phi; //overdipersion param for neg binom
      real<lower=1e-5> theta; //the overdispersion parameter for beta binomial
    }

    model {
      // include transformed parameters of no interest
      vector[N] lmu;//the linear predictor
      vector[A] p; // ASE proportion
      real ebj;
      real debj;
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        // Priors
        theta ~ gamma(1,0.1); //  mean 10 
        phi ~ gamma(1,0.1);  // mean 10
        betas[1] ~ normal(6,4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0,2.5);//prior for the slopes following Gelman 2008   
        }
        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      profile("likelihood") {
        // Likelihood
        ebj=exp(bj); // avoid repeating same calculation
        debj=ebj/(1+ebj);
      
        lmu = cov[,2:cols(cov)]*betas;
        for(i in 1:N){ // neg binomial
          lmu[i] = fabs(g[i])==1 ? lmu[i] + log1p(ebj)-log(2) : lmu[i];
          lmu[i] = g[i]==2 ? lmu[i] + bj : lmu[i];
          target += neg_binomial_2_lpmf(Y[i] | exp(lmu[i]),phi);
        }
        
        pos = 1;
        for(i in 1:A) { // ASE
          p[i]= gase[i]==1 ? debj : 0.5;
          p[i]= gase[i]==-1 ? 1-debj : p[i];  // haplotype swap
          for (r in pos:(pos+s[i]-1)){
            ltmp[r]=beta_binomial_lpmf(n[r] | m[i], p[i]*theta , (1-p[i])*theta) + log(pH[r]);
          }
          target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          pos=pos+s[i];
        }
      }
    }

Optimised model:

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance, version 2. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N, 1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      simplex[k] expMixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      vector[L] log_pH = log(pH);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower= -10, upper=10 > bj; // log fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
    }

    model {
      // include transformed parameters of no interest
      vector[A] p; // ASE proportion
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[k] lps; // help for mixed gaussians

      // nb lik stuff
      real ebj = exp(bj); // avoid repeating same calculation
      real debj = ebj / (1 + ebj);
      real l1pebj = log1p(ebj) - log(2);
      vector[N] intercept = rep_vector(0, N); // the genetic effect

      profile("priors") {
        // Priors
        theta ~ gamma(1, 0.1); //  mean 10 
        phi ~ gamma(1, 0.1);  // mean 10
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        betas[2:K] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      // Likelihood
      profile("likelihood") {
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj;
          }
          if (g[i] == 2) {
            intercept[i] = bj;
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);
        
        pos = 1;
        for(i in 1:A) { // ASE
          p[i] = gase[i] ==  1 ?     debj : 0.5;
          p[i] = gase[i] == -1 ? 1 - debj : p[i];  // haplotype swap
          if (s[i] == 1) {
            n[pos] ~ beta_binomial(m[i], p[i] * theta, (1 - p[i]) * theta);
          } else {
            for (r in pos:(pos+s[i]-1)) {
              ltmp[r] = beta_binomial_lpmf(
                n[r] | m[i], p[i] * theta, (1 - p[i]) * theta
              ) + log_pH[r];
              // ltmp[r] = beta_binomial_lpmf(
              //   n[r] | m[i], p[i] * theta, (1 - p[i]) * theta
              // );
            }
            // target += log_mix(pH[pos:(pos+s[i]-1)], ltmp[pos:(pos+s[i]-1)]);
            target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          }
          pos = pos + s[i];
        }
      }
    }
