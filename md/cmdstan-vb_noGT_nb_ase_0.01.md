VB with noGT\_nb\_ase, tolerance: 0.01
======================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

![](md/cmdstan-vb_noGT_nb_ase_0.01_files/figure-markdown_strict/plot-draws-1.png)![](md/cmdstan-vb_noGT_nb_ase_0.01_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_noGT_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_noGT_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139837720295232</td>
<td style="text-align: right;">1.3194100</td>
<td style="text-align: right;">1.2328300</td>
<td style="text-align: right;">0.0865800</td>
<td style="text-align: right;">12118652</td>
<td style="text-align: right;">3075800</td>
<td style="text-align: right;">30758</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139837720295232</td>
<td style="text-align: right;">0.0130526</td>
<td style="text-align: right;">0.0106659</td>
<td style="text-align: right;">0.0023867</td>
<td style="text-align: right;">276822</td>
<td style="text-align: right;">30758</td>
<td style="text-align: right;">30758</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140334440236864</td>
<td style="text-align: right;">0.3498710</td>
<td style="text-align: right;">0.3374190</td>
<td style="text-align: right;">0.0124522</td>
<td style="text-align: right;">1773394</td>
<td style="text-align: right;">450100</td>
<td style="text-align: right;">4501</td>
<td style="text-align: right;">5801</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140334440236864</td>
<td style="text-align: right;">0.0041289</td>
<td style="text-align: right;">0.0037639</td>
<td style="text-align: right;">0.0003650</td>
<td style="text-align: right;">40509</td>
<td style="text-align: right;">4501</td>
<td style="text-align: right;">4501</td>
<td style="text-align: right;">5801</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140553475295040</td>
<td style="text-align: right;">0.7869920</td>
<td style="text-align: right;">0.755522</td>
<td style="text-align: right;">0.0314692</td>
<td style="text-align: right;">4216194</td>
<td style="text-align: right;">1070100</td>
<td style="text-align: right;">10701</td>
<td style="text-align: right;">12002</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140553475295040</td>
<td style="text-align: right;">0.0090886</td>
<td style="text-align: right;">0.008167</td>
<td style="text-align: right;">0.0009217</td>
<td style="text-align: right;">96309</td>
<td style="text-align: right;">10701</td>
<td style="text-align: right;">10701</td>
<td style="text-align: right;">12002</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Prior as with GT
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N, 1+K] cov;
      int ASEi[N, 2]; // index to link NB with ASE, first col is 1 the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
      vector[L] log_pH = log(pH);
      vector[G] log_pNB = log(pNB);
      vector[G] abs_gNB = fabs(gNB);
    }


    parameters {
      vector[K] betas; // regression param

      real <lower=-20,upper=20> bj; // log fold change ASE
      real<lower=1e-5> phi; //overdipersion param for neg binom
      real<lower=1e-5> theta; //the overdispersion parameter for beta binomial

      // real bj; // log fold change ASE
      // real<lower=0> phi; //overdipersion param for neg binom
      // real<lower=0> theta; //the overdispersion parameter for beta binomial

    }

    model {
      int pos;
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebjd; // reduce computation
      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        theta ~ gamma(1, 0.1); //  based on stan code example
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }
        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
     
      // transformed parameters of no interest
      pos = 1;
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0, Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g

      real ebj = exp(bj); // avoid repeating same calculation
      real l1pebj = log1p(ebj) - log(2);
      ebjd = ebj * inv(ebj + 1);
      lmu1 = cov[, 2:cols(cov)] * betas;

      profile("likelihood") {

        for(i in 1:N) { // ind level
          
          for (r in pos:(pos+sNB[i]-1)) { 
            lmu = abs_gNB[r] == 1 ? lmu1[i] + l1pebj : lmu1[i];
            lmu = gNB[r] == 2 ? lmu + bj : lmu;
            ltmp[r] = neg_binomial_2_log_lpmf(
              Y[i] | lmu, phi
            ) + log_pNB[r];
            
            if (ASEi[i, 1] == 1) {//  ASE

              for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g

                p = gase[posl]==1 ? ebjd : 0.5;
                p = gase[posl]==-1 ? 1-ebjd : p;  // haplotype swap
                ase[x] = beta_binomial_lpmf(
                  n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                ) + log_pH[posl];

                posl += 1;
              }
              sAse = log_sum_exp(ase[1:h2g[r]]);
              target +=  log_sum_exp(ltmp[r], sAse);
            }
          }
          if(ASEi[i, 1] == 0) { // NO ASE, only NB terms for this ind
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          }
          pos = pos + sNB[i];
        }
      }
    }
