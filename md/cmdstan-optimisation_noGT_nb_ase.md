Optimising noGT\_nb\_ase
========================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

    ## Warning: Chain 2 finished unexpectedly!

    ## Warning: 1 chain(s) finished unexpectedly!

    ## Warning: The returned fit object will only read in results of successful
    ## chains. Please use read_cmdstan_csv() to read the results of the failed chains
    ## separately.Use the $output(chain_id) method for more output of the failed
    ## chains.

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_noGT_nb_ase_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7259822</td>
<td style="text-align: right;">6.7255200</td>
<td style="text-align: right;">0.0492885</td>
<td style="text-align: right;">0.0483179</td>
<td style="text-align: right;">6.6457480</td>
<td style="text-align: right;">6.8073725</td>
<td style="text-align: right;">0.9999656</td>
<td style="text-align: right;">23232.06</td>
<td style="text-align: right;">15824.02</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2113320</td>
<td style="text-align: right;">0.2106655</td>
<td style="text-align: right;">0.0480825</td>
<td style="text-align: right;">0.0472097</td>
<td style="text-align: right;">0.1328312</td>
<td style="text-align: right;">0.2905739</td>
<td style="text-align: right;">1.0002298</td>
<td style="text-align: right;">21166.13</td>
<td style="text-align: right;">15340.92</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0112216</td>
<td style="text-align: right;">0.0109544</td>
<td style="text-align: right;">0.0269477</td>
<td style="text-align: right;">0.0268007</td>
<td style="text-align: right;">-0.0328013</td>
<td style="text-align: right;">0.0552920</td>
<td style="text-align: right;">1.0000188</td>
<td style="text-align: right;">23608.42</td>
<td style="text-align: right;">16239.64</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.4705171</td>
<td style="text-align: right;">8.3513750</td>
<td style="text-align: right;">1.6532556</td>
<td style="text-align: right;">1.6181541</td>
<td style="text-align: right;">5.9673000</td>
<td style="text-align: right;">11.3763250</td>
<td style="text-align: right;">1.0002332</td>
<td style="text-align: right;">21666.51</td>
<td style="text-align: right;">15117.68</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">60.8862540</td>
<td style="text-align: right;">58.7819500</td>
<td style="text-align: right;">17.7765734</td>
<td style="text-align: right;">16.8647974</td>
<td style="text-align: right;">35.6755000</td>
<td style="text-align: right;">93.1144400</td>
<td style="text-align: right;">0.9999477</td>
<td style="text-align: right;">23148.88</td>
<td style="text-align: right;">15171.96</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">betas[1]</td>
<td style="text-align: right;">6.7259288</td>
<td style="text-align: right;">6.7255600</td>
<td style="text-align: right;">0.0484537</td>
<td style="text-align: right;">0.0474284</td>
<td style="text-align: right;">6.6467595</td>
<td style="text-align: right;">6.8071905</td>
<td style="text-align: right;">1.000327</td>
<td style="text-align: right;">18527.55</td>
<td style="text-align: right;">10797.89</td>
</tr>
<tr class="even">
<td style="text-align: left;">betas[2]</td>
<td style="text-align: right;">0.2113424</td>
<td style="text-align: right;">0.2107240</td>
<td style="text-align: right;">0.0478986</td>
<td style="text-align: right;">0.0470318</td>
<td style="text-align: right;">0.1331686</td>
<td style="text-align: right;">0.2914968</td>
<td style="text-align: right;">1.000027</td>
<td style="text-align: right;">18461.44</td>
<td style="text-align: right;">11074.48</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bj</td>
<td style="text-align: right;">0.0109426</td>
<td style="text-align: right;">0.0106725</td>
<td style="text-align: right;">0.0269469</td>
<td style="text-align: right;">0.0264248</td>
<td style="text-align: right;">-0.0330959</td>
<td style="text-align: right;">0.0555221</td>
<td style="text-align: right;">1.000085</td>
<td style="text-align: right;">19238.31</td>
<td style="text-align: right;">11301.05</td>
</tr>
<tr class="even">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">8.4745554</td>
<td style="text-align: right;">8.3769550</td>
<td style="text-align: right;">1.6222411</td>
<td style="text-align: right;">1.6024460</td>
<td style="text-align: right;">5.9866380</td>
<td style="text-align: right;">11.3082400</td>
<td style="text-align: right;">1.000274</td>
<td style="text-align: right;">17458.05</td>
<td style="text-align: right;">10977.76</td>
</tr>
<tr class="odd">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">60.4116151</td>
<td style="text-align: right;">58.5957000</td>
<td style="text-align: right;">17.3321314</td>
<td style="text-align: right;">16.7275828</td>
<td style="text-align: right;">35.8320150</td>
<td style="text-align: right;">91.6722100</td>
<td style="text-align: right;">1.000116</td>
<td style="text-align: right;">19028.45</td>
<td style="text-align: right;">11073.63</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139713895753536</td>
<td style="text-align: right;">10.4173000</td>
<td style="text-align: right;">9.9034100</td>
<td style="text-align: right;">0.5138530</td>
<td style="text-align: right;">31999960</td>
<td style="text-align: right;">5839404</td>
<td style="text-align: right;">58394</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001784</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139713895753536</td>
<td style="text-align: right;">0.0700213</td>
<td style="text-align: right;">0.0618468</td>
<td style="text-align: right;">0.0081745</td>
<td style="text-align: right;">525600</td>
<td style="text-align: right;">58400</td>
<td style="text-align: right;">58394</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000012</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139868814321472</td>
<td style="text-align: right;">11.0538000</td>
<td style="text-align: right;">10.4856000</td>
<td style="text-align: right;">0.5682500</td>
<td style="text-align: right;">32283817</td>
<td style="text-align: right;">5891203</td>
<td style="text-align: right;">58912</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001876</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139868814321472</td>
<td style="text-align: right;">0.1599560</td>
<td style="text-align: right;">0.1368470</td>
<td style="text-align: right;">0.0231090</td>
<td style="text-align: right;">530262</td>
<td style="text-align: right;">58918</td>
<td style="text-align: right;">58912</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000027</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140441490388800</td>
<td style="text-align: right;">9.8711300</td>
<td style="text-align: right;">9.2553400</td>
<td style="text-align: right;">0.6157870</td>
<td style="text-align: right;">32303417</td>
<td style="text-align: right;">5894760</td>
<td style="text-align: right;">58947</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001675</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140441490388800</td>
<td style="text-align: right;">0.1128840</td>
<td style="text-align: right;">0.1036760</td>
<td style="text-align: right;">0.0092079</td>
<td style="text-align: right;">530586</td>
<td style="text-align: right;">58954</td>
<td style="text-align: right;">58947</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000019</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139850618558272</td>
<td style="text-align: right;">10.7285000</td>
<td style="text-align: right;">9.9635000</td>
<td style="text-align: right;">0.7649520</td>
<td style="text-align: right;">31518114</td>
<td style="text-align: right;">5751457</td>
<td style="text-align: right;">57514</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001865</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139850618558272</td>
<td style="text-align: right;">0.0920071</td>
<td style="text-align: right;">0.0835044</td>
<td style="text-align: right;">0.0085027</td>
<td style="text-align: right;">517689</td>
<td style="text-align: right;">57521</td>
<td style="text-align: right;">57514</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000016</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140338691733312</td>
<td style="text-align: right;">12.276400</td>
<td style="text-align: right;">11.6385000</td>
<td style="text-align: right;">0.6378880</td>
<td style="text-align: right;">27237220</td>
<td style="text-align: right;">6913000</td>
<td style="text-align: right;">69130</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001776</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140338691733312</td>
<td style="text-align: right;">0.132885</td>
<td style="text-align: right;">0.1170480</td>
<td style="text-align: right;">0.0158369</td>
<td style="text-align: right;">622170</td>
<td style="text-align: right;">69130</td>
<td style="text-align: right;">69130</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000019</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140625147569984</td>
<td style="text-align: right;">12.897300</td>
<td style="text-align: right;">12.1365000</td>
<td style="text-align: right;">0.7607940</td>
<td style="text-align: right;">27780940</td>
<td style="text-align: right;">7051000</td>
<td style="text-align: right;">70510</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001829</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140625147569984</td>
<td style="text-align: right;">0.161005</td>
<td style="text-align: right;">0.1466370</td>
<td style="text-align: right;">0.0143677</td>
<td style="text-align: right;">634590</td>
<td style="text-align: right;">70510</td>
<td style="text-align: right;">70510</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000023</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140283852609344</td>
<td style="text-align: right;">12.662800</td>
<td style="text-align: right;">11.9290000</td>
<td style="text-align: right;">0.7338570</td>
<td style="text-align: right;">27522082</td>
<td style="text-align: right;">6985300</td>
<td style="text-align: right;">69853</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0001813</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140283852609344</td>
<td style="text-align: right;">0.131709</td>
<td style="text-align: right;">0.0775397</td>
<td style="text-align: right;">0.0541693</td>
<td style="text-align: right;">628677</td>
<td style="text-align: right;">69853</td>
<td style="text-align: right;">69853</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000019</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001825</td>
<td style="text-align: right;">10.5729000</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000018</td>
<td style="text-align: right;">0.1024456</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0001813</td>
<td style="text-align: right;">12.662800</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000019</td>
<td style="text-align: right;">0.132885</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Prior as with GT
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior

    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
    }


    parameters {
      vector[K] betas; // regression param
      real bj; // log fold change ASE
      real<lower=0> phi; //overdipersion param for neg binom
      real<lower=0> theta; //the overdispersion parameter for beta binomial
    }

    model {
      int pos;
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebj; // reduce computation
      real ebjd; // reduce computation
      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        betas[1] ~ normal(6,4); // stan normal is mean and sd
        for(i in 2:K){
          betas[i] ~ cauchy(0,2.5);//prior for the slopes following Gelman 2008   
        }
        // mixture of gaussians for bj:
        for(i in 1:k){
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
      
      // transformed parameters of no interest
      pos = 1;
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0, Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g
      ebj = exp(bj);
      ebjd = ebj * inv(ebj + 1);
      lmu1 = cov[, 2:cols(cov)]*betas;

      profile("likelihood") {
        for(i in 1:N) { // ind level
          
          for (r in pos:(pos+sNB[i]-1)) { 
            lmu = fabs(gNB[r])==1 ? lmu1[i] + log1p(ebj)-log(2) : lmu1[i];

            lmu = gNB[r]==2 ? lmu + bj : lmu;

            ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu), phi) + log(pNB[r]);
            
            if (ASEi[i,1] == 1){//  ASE

              for (x in 1:h2g[r]){  // look at the haps compatibles with Gi=g

                p= gase[posl]==1 ? ebjd : 0.5;
                p= gase[posl]==-1 ? 1-ebjd : p;  // haplotype swap
                ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);

                posl += 1;
              }
              sAse = log_sum_exp(ase[1:h2g[r]]);
              target +=  log_sum_exp(ltmp[r] , sAse );
            }
          }
          if(ASEi[i, 1] == 0) { // NO ASE, only NB terms for this ind
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          }
          pos=pos+sNB[i];
        }
      }
    }

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error. Version 2 correcting likelihood so for each individual and each genotype only those hap compatible with g are considered. Prior as with GT
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N, 1+K] cov;
      int ASEi[N, 2]; // index to link NB with ASE, first col is 1 the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
      vector[L] log_pH = log(pH);
      vector[G] log_pNB = log(pNB);
      vector[G] abs_gNB = fabs(gNB);
    }


    parameters {
      vector[K] betas; // regression param

      real <lower=-20,upper=20> bj; // log fold change ASE
      real <lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real <lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
    }

    model {
      int pos;
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      real lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood
      real ebjd; // reduce computation
      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      vector[k] lps; // help for mixed gaussians

      profile("priors") {
        //priors
        theta ~ gamma(1, 0.1); //  based on stan code example
        phi ~ gamma(1, 0.1);
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        for(i in 2:K) {
          betas[i] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        }
        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }
     
      // transformed parameters of no interest
      pos = 1;
      posl = 1; // to advance on ASE terms
      ase = rep_vector(0, Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g

      real ebj = exp(bj); // avoid repeating same calculation
      real l1pebj = log1p(ebj) - log(2);
      ebjd = ebj * inv(ebj + 1);
      lmu1 = cov[, 2:cols(cov)] * betas;

      profile("likelihood") {

        for(i in 1:N) { // ind level
          
          for (r in pos:(pos+sNB[i]-1)) { 
            lmu = abs_gNB[r] == 1 ? lmu1[i] + l1pebj : lmu1[i];
            lmu = gNB[r] == 2 ? lmu + bj : lmu;
            ltmp[r] = neg_binomial_2_log_lpmf(
              Y[i] | lmu, phi
            ) + log_pNB[r];
            
            if (ASEi[i, 1] == 1) {//  ASE

              for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g

                p = gase[posl]==1 ? ebjd : 0.5;
                p = gase[posl]==-1 ? 1-ebjd : p;  // haplotype swap
                ase[x] = beta_binomial_lpmf(
                  n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                ) + log_pH[posl];

                posl += 1;
              }
              sAse = log_sum_exp(ase[1:h2g[r]]);
              target +=  log_sum_exp(ltmp[r], sAse);
            }
          }
          if(ASEi[i, 1] == 0) { // NO ASE, only NB terms for this ind
            target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
          }
          pos = pos + sNB[i];
        }
      }
    }
