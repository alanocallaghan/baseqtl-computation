Optimising noGT2T\_nb\_ase\_refbias
===================================

I want to test out a couple of ways to speed up fitting BaseQTL in
various configurations. The first idea is to have a look at the Stan
code and see if there’s any ways to optimise the model without changing
the inference engine or reparameterising. Stan really likes
vectorisation (especially in recent versions). The `*_glm` functions are
also much preferred for stability & efficiency, so I’ve switched the NB
likelihood to use that.

I also saw a lot of warnings that `ebj is 8.68913e+10` or some other
ludicrous value. Not sure exactly why, but `bj` was getting large
proposals, and the `log1p(exp(bj))` was then also exploding. Adding hard
constraints to +-20 seems reasonable - I’m not familiar with the range
of expected eQTL effects but in general if I see a logFC of 20 I assume
a mistake.

For good measure the dispersion parameter has to be positive finite, so
I’m constraining that to be &gt; 1e-5. Anything below that is very
unstable in the NB likelihood anyway.

This is also likely to help when it does happen, as rejecting values is
relatively expensive for Stan. It’s also likely to make ADVI fail quite
often.

I’ll use [cmdstanr](https://mc-stan.org/cmdstanr/) because:

1.  tt’s faster to iterate with,
2.  it allows profiling, and
3.  it gives access to newer versions of the Stan C++ library that still
    aren’t in Rstan (soon, I’m told).

<!-- -->

    library("cmdstanr")

    ## This is cmdstanr version 0.5.2

    ## - CmdStanR documentation and vignettes: mc-stan.org/cmdstanr

    ## - CmdStan path: /home/alan/.cmdstan/cmdstan-2.29.2

    ## - CmdStan version: 2.29.2

    ## 
    ## A newer version of CmdStan is available. See ?install_cmdstan() to install it.
    ## To disable this check set option or environment variable CMDSTANR_NO_VER_CHECK=TRUE.

    library("ggplot2")
    library("ggdist")
    library("reshape2")
    library("dplyr")

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library("knitr")
    source("functions.R")

    ## This is loo version 2.5.1

    ## - Online documentation and vignettes at mc-stan.org/loo

    ## - As of v2.0.0 loo defaults to 1 core but we recommend using as many as possible. Use the 'cores' argument or set options(mc.cores = NUM_CORES) for an entire session.

    ## 
    ## Attaching package: 'matrixStats'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     count

    theme_set(theme_bw())
    set.seed(42)

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    model1 <- cmdstan_model(model_file)
    fit1 <- model1$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

Then fit one model with the same `data` and a mildly optimised version:

    model2 <- cmdstan_model(model_file_opt)
    fit2 <- model2$sample(data = data, chains = 4, refresh = 0, iter_sampling = n, iter_warmup = n)

The results are (basically) equivalent, within reasonable Monte Carlo
error.

    vars <- fit1$summary()$variable
    if ("ba" %in% vars) {
        vars <- intersect(vars, c("ba", "bd", "bp", "bn", "phi", "theta"))
    } else {
        vars <- setdiff(vars, "lp__")
    }
    s1 <- fit1$summary(variables = vars)
    s2 <- fit2$summary(variables = vars)

    plot_draws(list(fit1, fit2), names = c("Original", "Optimised"))

![](md/cmdstan-optimisation_noGT2T_nb_ase_refbias_files/figure-markdown_strict/plots-1.png)

Original model:

    knitr::kable(s1)

<table style="width:100%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">ba</td>
<td style="text-align: right;">-0.0860933</td>
<td style="text-align: right;">-0.0846432</td>
<td style="text-align: right;">0.0299903</td>
<td style="text-align: right;">0.0281143</td>
<td style="text-align: right;">-0.1370808</td>
<td style="text-align: right;">-0.0400341</td>
<td style="text-align: right;">1.0000031</td>
<td style="text-align: right;">23412.70</td>
<td style="text-align: right;">11076.22</td>
</tr>
<tr class="even">
<td style="text-align: left;">bd</td>
<td style="text-align: right;">0.0503520</td>
<td style="text-align: right;">0.0500836</td>
<td style="text-align: right;">0.0267073</td>
<td style="text-align: right;">0.0262810</td>
<td style="text-align: right;">0.0063979</td>
<td style="text-align: right;">0.0949806</td>
<td style="text-align: right;">1.0000214</td>
<td style="text-align: right;">26747.28</td>
<td style="text-align: right;">14252.94</td>
</tr>
<tr class="odd">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">3.8284904</td>
<td style="text-align: right;">3.7913600</td>
<td style="text-align: right;">0.5859538</td>
<td style="text-align: right;">0.5754341</td>
<td style="text-align: right;">2.9299040</td>
<td style="text-align: right;">4.8529295</td>
<td style="text-align: right;">1.0003835</td>
<td style="text-align: right;">28459.93</td>
<td style="text-align: right;">14090.48</td>
</tr>
<tr class="even">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">62.4630994</td>
<td style="text-align: right;">62.2106000</td>
<td style="text-align: right;">6.0968201</td>
<td style="text-align: right;">6.0597568</td>
<td style="text-align: right;">52.9041250</td>
<td style="text-align: right;">72.8816250</td>
<td style="text-align: right;">0.9999511</td>
<td style="text-align: right;">26229.00</td>
<td style="text-align: right;">14942.78</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bp</td>
<td style="text-align: right;">-0.0357413</td>
<td style="text-align: right;">-0.0344794</td>
<td style="text-align: right;">0.0432768</td>
<td style="text-align: right;">0.0421833</td>
<td style="text-align: right;">-0.1079918</td>
<td style="text-align: right;">0.0331594</td>
<td style="text-align: right;">1.0000644</td>
<td style="text-align: right;">21431.84</td>
<td style="text-align: right;">13084.51</td>
</tr>
<tr class="even">
<td style="text-align: left;">bn</td>
<td style="text-align: right;">-0.1364453</td>
<td style="text-align: right;">-0.1358115</td>
<td style="text-align: right;">0.0367765</td>
<td style="text-align: right;">0.0357270</td>
<td style="text-align: right;">-0.1981531</td>
<td style="text-align: right;">-0.0770144</td>
<td style="text-align: right;">1.0001490</td>
<td style="text-align: right;">28611.95</td>
<td style="text-align: right;">13338.47</td>
</tr>
</tbody>
</table>

Original model:

    knitr::kable(s2)

<table style="width:100%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: right;">mean</th>
<th style="text-align: right;">median</th>
<th style="text-align: right;">sd</th>
<th style="text-align: right;">mad</th>
<th style="text-align: right;">q5</th>
<th style="text-align: right;">q95</th>
<th style="text-align: right;">rhat</th>
<th style="text-align: right;">ess_bulk</th>
<th style="text-align: right;">ess_tail</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">ba</td>
<td style="text-align: right;">-0.0839430</td>
<td style="text-align: right;">-0.0824988</td>
<td style="text-align: right;">0.0295941</td>
<td style="text-align: right;">0.0279592</td>
<td style="text-align: right;">-0.1339282</td>
<td style="text-align: right;">-0.0382211</td>
<td style="text-align: right;">1.000836</td>
<td style="text-align: right;">6504.587</td>
<td style="text-align: right;">6192.284</td>
</tr>
<tr class="even">
<td style="text-align: left;">bd</td>
<td style="text-align: right;">0.0500614</td>
<td style="text-align: right;">0.0499416</td>
<td style="text-align: right;">0.0268602</td>
<td style="text-align: right;">0.0264912</td>
<td style="text-align: right;">0.0062315</td>
<td style="text-align: right;">0.0945139</td>
<td style="text-align: right;">1.000317</td>
<td style="text-align: right;">7399.688</td>
<td style="text-align: right;">9024.489</td>
</tr>
<tr class="odd">
<td style="text-align: left;">phi</td>
<td style="text-align: right;">3.5949521</td>
<td style="text-align: right;">3.5722200</td>
<td style="text-align: right;">0.5253619</td>
<td style="text-align: right;">0.5228760</td>
<td style="text-align: right;">2.7720255</td>
<td style="text-align: right;">4.4760875</td>
<td style="text-align: right;">1.000499</td>
<td style="text-align: right;">1653.510</td>
<td style="text-align: right;">3059.666</td>
</tr>
<tr class="even">
<td style="text-align: left;">theta</td>
<td style="text-align: right;">62.7181570</td>
<td style="text-align: right;">62.4586500</td>
<td style="text-align: right;">6.1392909</td>
<td style="text-align: right;">6.0698385</td>
<td style="text-align: right;">52.9461450</td>
<td style="text-align: right;">73.2281750</td>
<td style="text-align: right;">1.001748</td>
<td style="text-align: right;">1405.018</td>
<td style="text-align: right;">2263.837</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bp</td>
<td style="text-align: right;">-0.0338816</td>
<td style="text-align: right;">-0.0330058</td>
<td style="text-align: right;">0.0425558</td>
<td style="text-align: right;">0.0411456</td>
<td style="text-align: right;">-0.1051862</td>
<td style="text-align: right;">0.0348452</td>
<td style="text-align: right;">1.000214</td>
<td style="text-align: right;">6332.804</td>
<td style="text-align: right;">7723.487</td>
</tr>
<tr class="even">
<td style="text-align: left;">bn</td>
<td style="text-align: right;">-0.1340044</td>
<td style="text-align: right;">-0.1337975</td>
<td style="text-align: right;">0.0371964</td>
<td style="text-align: right;">0.0363637</td>
<td style="text-align: right;">-0.1954230</td>
<td style="text-align: right;">-0.0739172</td>
<td style="text-align: right;">1.000660</td>
<td style="text-align: right;">7230.855</td>
<td style="text-align: right;">8615.291</td>
</tr>
</tbody>
</table>

Profiling lets you inspect what’s taking up time and stack space during
each of the profile blocks. From what I understand (admittedly not a
lot) you want to reduce the autodiff calls and the chain stack, because
autodiff calls means calculating gradients, and more objects in the
stack means more expensive gradients.

What’s cool is that adding a bit of vectorisation seems to be almost a
50% improvement on the time taken evaluating the likelihood:

    p1 <- do.call(rbind, fit1$profiles())
    p1$per_gradient_timing <- p1[["total_time"]]/p1[["autodiff_calls"]]
    knitr::kable(p1)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139860279027520</td>
<td style="text-align: right;">274.2330</td>
<td style="text-align: right;">249.46600</td>
<td style="text-align: right;">24.76740</td>
<td style="text-align: right;">1379006155</td>
<td style="text-align: right;">117548763</td>
<td style="text-align: right;">165090</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0016611</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139860279027520</td>
<td style="text-align: right;">11.6452</td>
<td style="text-align: right;">9.70198</td>
<td style="text-align: right;">1.94326</td>
<td style="text-align: right;">105335714</td>
<td style="text-align: right;">330206</td>
<td style="text-align: right;">165090</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000705</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139833440962368</td>
<td style="text-align: right;">274.0380</td>
<td style="text-align: right;">249.02900</td>
<td style="text-align: right;">25.00980</td>
<td style="text-align: right;">1355800257</td>
<td style="text-align: right;">115570773</td>
<td style="text-align: right;">162312</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0016883</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139833440962368</td>
<td style="text-align: right;">11.2200</td>
<td style="text-align: right;">9.25178</td>
<td style="text-align: right;">1.96825</td>
<td style="text-align: right;">103563350</td>
<td style="text-align: right;">324650</td>
<td style="text-align: right;">162312</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000691</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139928797173568</td>
<td style="text-align: right;">277.6520</td>
<td style="text-align: right;">252.90100</td>
<td style="text-align: right;">24.75110</td>
<td style="text-align: right;">1391283844</td>
<td style="text-align: right;">118595351</td>
<td style="text-align: right;">166560</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0016670</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139928797173568</td>
<td style="text-align: right;">11.3285</td>
<td style="text-align: right;">9.59061</td>
<td style="text-align: right;">1.73793</td>
<td style="text-align: right;">106273574</td>
<td style="text-align: right;">333146</td>
<td style="text-align: right;">166560</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000680</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140592415262528</td>
<td style="text-align: right;">289.7920</td>
<td style="text-align: right;">263.55700</td>
<td style="text-align: right;">26.23450</td>
<td style="text-align: right;">1510004994</td>
<td style="text-align: right;">128715006</td>
<td style="text-align: right;">180773</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0016031</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140592415262528</td>
<td style="text-align: right;">11.8296</td>
<td style="text-align: right;">9.64517</td>
<td style="text-align: right;">2.18448</td>
<td style="text-align: right;">115341468</td>
<td style="text-align: right;">361572</td>
<td style="text-align: right;">180773</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000654</td>
</tr>
</tbody>
</table>

    p2 <- do.call(rbind, fit2$profiles())
    p2$per_gradient_timing <- p2[["total_time"]]/p2[["autodiff_calls"]]
    knitr::kable(p2)

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 11%" />
<col style="width: 7%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: left;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
<th style="text-align: right;">per_gradient_timing</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139767474915136</td>
<td style="text-align: right;">187.51000</td>
<td style="text-align: right;">169.13900</td>
<td style="text-align: right;">18.37090</td>
<td style="text-align: left;">1298945531</td>
<td style="text-align: right;">120911128</td>
<td style="text-align: right;">169819</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0011042</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139767474915136</td>
<td style="text-align: right;">8.20359</td>
<td style="text-align: right;">6.58561</td>
<td style="text-align: right;">1.61798</td>
<td style="text-align: left;">108344522</td>
<td style="text-align: right;">339638</td>
<td style="text-align: right;">169819</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000483</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140411366807360</td>
<td style="text-align: right;">180.84000</td>
<td style="text-align: right;">163.32800</td>
<td style="text-align: right;">17.51180</td>
<td style="text-align: left;">1267148638</td>
<td style="text-align: right;">117951344</td>
<td style="text-align: right;">165662</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0010916</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140411366807360</td>
<td style="text-align: right;">8.23346</td>
<td style="text-align: right;">6.57408</td>
<td style="text-align: right;">1.65938</td>
<td style="text-align: left;">105692356</td>
<td style="text-align: right;">331324</td>
<td style="text-align: right;">165662</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000497</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140345848399680</td>
<td style="text-align: right;">368.52200</td>
<td style="text-align: right;">329.12300</td>
<td style="text-align: right;">39.39920</td>
<td style="text-align: left;">2676928179</td>
<td style="text-align: right;">249179352</td>
<td style="text-align: right;">349971</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0010530</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140345848399680</td>
<td style="text-align: right;">17.93020</td>
<td style="text-align: right;">13.94450</td>
<td style="text-align: right;">3.98575</td>
<td style="text-align: left;">223281498</td>
<td style="text-align: right;">699942</td>
<td style="text-align: right;">349971</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000512</td>
</tr>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139720358934336</td>
<td style="text-align: right;">268.51200</td>
<td style="text-align: right;">241.57200</td>
<td style="text-align: right;">26.94030</td>
<td style="text-align: left;">1806043635</td>
<td style="text-align: right;">168113880</td>
<td style="text-align: right;">236115</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0011372</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139720358934336</td>
<td style="text-align: right;">12.45170</td>
<td style="text-align: right;">9.87722</td>
<td style="text-align: right;">2.57446</td>
<td style="text-align: left;">150641370</td>
<td style="text-align: right;">472230</td>
<td style="text-align: right;">236115</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000527</td>
</tr>
</tbody>
</table>

    p1 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0016640</td>
<td style="text-align: right;">275.94250</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000686</td>
<td style="text-align: right;">11.48685</td>
</tr>
</tbody>
</table>

    p2 %>%
      group_by(name) %>%
      summarize(per_grad_time=median(per_gradient_timing), total_time=median(total_time)) %>%
      knitr::kable()

<table>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: right;">per_grad_time</th>
<th style="text-align: right;">total_time</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: right;">0.0010979</td>
<td style="text-align: right;">228.01100</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: right;">0.0000505</td>
<td style="text-align: right;">10.34258</td>
</tr>
</tbody>
</table>

There’s some information on the columns
[here](https://mc-stan.org/docs/2_26/cmdstan-guide/stan-csv.html#profiling-csv-output-file).

Models
======

Original model (mostly):

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error, allowing for interaction term normal psoriasis, with or without covariates and ref bias correction version 2. Updated likelihood and mixed prior.
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) and ai0 
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype for each sample
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample, log scale
      //int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      int I[N]; //indicator for skin all samples: 1=pso, 0=normal
      //int IA[A]; // indicator for skin in ASE samples: 1=pso, 0=normal
      
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
    }


    parameters {
      real anorm; // mean expression normal tissue
      real apso; // mean expression pso tissues
      real ba; // log average-fold change ASE
      real bd; // log difference-fold change ASE
      real<lower=0> phi; //overdipersion param for neg binom
      real<lower=0> theta; //the overdispersion parameter for beta binomial
      vector[K-1] betas; // regression parameters 
      vector[L] rai0; // random intercept AI
      
      }

    transformed parameters {
      real bp; // parameter of interest for psoriasis
      real bn; // parameter of interest for normal skin
        
      bp = ba + bd;
      bn = ba -bd;
        
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      vector[G] lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood

      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      real esum; // reduce computation inverse logit (rai0 + bp/bn)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lpsa; // help for mixed gaussians for ba
      vector[k] lpsd; // help for mixed gaussians for bd
      
      /* vector[L] p; // ase proportion */
      /* vector[L] ase; //beta-binom term */
      vector[K] betasN; //regression parameters for normal skin
      vector[K] betasP; //regression parameters for pso skin
      vector[N] lmuN; //help linear pred normal skin
      vector[N] lmuP; //help linear pred pso skin
      /* real esum; // reduce computation inverse logit (la0 + bj) */
      
      profile("priors") {
        //priors
        theta ~ gamma(1,0.1); //  based on stan code example
        phi ~ gamma(1,0.1);
        anorm ~ normal(6,4); // mean expression normal skin, stan normal is mean and sd
        apso ~ normal(6,4); // mean expression pso skin
        for(i in 1:(K-1)){
          betas[i] ~ cauchy(0,2.5);//prior for the covariates slopes following Gelman 2008
        }
      
        // allelic imbalance priors
        for(i in 1:L) {
          rai0[i] ~ normal(ai0[i], sdai0[i]);
        }

        // mixture of gaussians for ba and bd:
        for(i in 1:k){
          lpsa[i] = normal_lpdf(ba | aveP[i], sdP[i]) + mixP[i];
          lpsd[i] = normal_lpdf(bd | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lpsa);
        target += log_sum_exp(lpsd);
      }

      profile("likelihood") {
        // transformed parameters of no interest
        pos = 1; // to advance on NB terms
        posl = 1; // to advance on ASE terms
        ase = rep_vector(0,Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */

        betasN=append_row(anorm, betas); //betas for normal inds
        betasP=append_row(apso, betas); // betas for pso inds
        lmuN=cov[,2:cols(cov)]*betasN; // will be used for normal inds (based on indicator)
        lmuP=cov[,2:cols(cov)]*betasP; // for pso inds
        
        esum0 = inv_logit(rai0);
        
        for(i in 1:N){ // lmu for each individual
          // check skin first
          if (I[i] == 1) { // psoriasis

            for (r in pos:(pos+sNB[i]-1)){ // then genotype

              lmu[r] = lmuP[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(1+exp(bp))-log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bp : lmu[r];
              ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu[r]), phi) + log(pNB[r]);

              if (ASEi[i,1] == 1) {  // ASE info
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                  esum = inv_logit(rai0[posl] + bp);
                  p = gase[posl]==1 ? esum: esum0[posl];
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);
                target +=  log_sum_exp(ltmp[r] , sAse );
              }
            }
            if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
            
          } else {
            for (r in pos:(pos+sNB[i]-1)) { //normal skin
              
              lmu[r] = lmuN[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(1+exp(bn))-log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bn : lmu[r];
              ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu[r]), phi) + log(pNB[r]);

              if (ASEi[i,1] == 1) {  // ASE info
          
                for (x in 1:h2g[r]){  // look at the haps compatibles with Gi=g
                  
                  esum = inv_logit(rai0[posl] + bn);
                  p = gase[posl]==1 ? esum : esum0[posl];
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  ase[x] = beta_binomial_lpmf(n[posl] | m[ASEi[i,2]], p*theta, (1-p)*theta) + log(pH[posl]);
                  
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);   
                target +=  log_sum_exp(ltmp[r] , sAse );      
              }
            }
            
            if(ASEi[i,1] == 0){ // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
          }   
        }
      }
    }

Optimised model:

    // negative binomial and  ASE eQTL with unkwown rsnp genotype but fixed fsnps genotypes allowing haplotype error, allowing for interaction term normal psoriasis, with or without covariates and ref bias correction version 2. Updated likelihood and mixed prior.
                      
    data {
      int<lower=0> N; // number  individuals with NB info
      int<lower=0> G; // number of total genotypes for all individuals NB
      int<lower=0> A; // number of individuals ASE info
      int<lower=0> L; // length of vectors with n counts, gase p(H) and ai0 
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int sNB[N]; //  number of possible genotypes NB for each individual
      vector[G] gNB; // each geno NB
      vector[G] pNB; // prob for each geno NB
      int gase[L]; // genotype rsnp ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      vector[L] ai0; // allelic imbalance estimate for each haplotype for each sample
      vector[L] sdai0; // standard deviation for allelic imbalance estimate for each haplotype for each sample, log scale
      //int s[A]; // number of haplotypes per individual
      matrix[N,1+K] cov;
      int ASEi[N,2]; // index to link NB with ASE, first col is 1 if the individual has NB and ASE info, 0 otherwise. Second col gives index of ASE individual to relate NB with ASE
      int h2g[G]; // number of haps per genotype for ASE inds, 0 when ASE is not available
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      int I[N]; //indicator for skin all samples: 1=pso, 0=normal
      //int IA[A]; // indicator for skin in ASE samples: 1=pso, 0=normal
      
    }

    transformed data {
      int Max; // maximun number of elements in h2g
      Max = max(h2g);
      vector[G] abs_gNB = fabs(gNB);
      vector[G] log_pNB = log(pNB);
      vector[L] log_pH = log(pH);
    }


    parameters {
      real anorm; // mean expression normal tissue
      real apso; // mean expression pso tissues
      real <lower=-10, upper=10> ba; // log average-fold change ASE
      real <lower=-10, upper=10> bd; // log difference-fold change ASE
      real <lower=1e-5,upper=1e5> phi; //overdipersion param for neg binom
      real <lower=1e-5,upper=1e5> theta; //the overdispersion parameter for beta binomial
      vector [K-1] betas; // regression parameters 
      vector <lower=-10,upper=10> [L] rai0; // random intercept AI
    }

    transformed parameters {
      real <lower=-20, upper=20> bp; // parameter of interest for psoriasis
      real <lower=-20, upper=20> bn; // parameter of interest for normal skin    

      bp = ba + bd;
      bn = ba -bd;
    }

    model {
      int pos; // to advance through NB terms (1-G)
      int posl; // to advance through ASE terms (1-L)
      vector[N] lmu1; // help to construct linear pred
      vector[G] lmu; // linear predictor log scale
      vector[G] ltmp; //  log NB likelihood

      real p; // ase proportion
      vector[Max] ase; //beta-binom terms
      real sAse; // sums beta-binom terms for haplotypes compatible with Gi=g
      real esum; // reduce computation inverse logit (rai0 + bp/bn)
      vector[L] esum0; // allelic imbalance proportion under the null
      vector[k] lpsa; // help for mixed gaussians for ba
      vector[k] lpsd; // help for mixed gaussians for bd
      
      /* vector[L] p; // ase proportion */
      /* vector[L] ase; //beta-binom term */
      vector[K] betasN; //regression parameters for normal skin
      vector[K] betasP; //regression parameters for pso skin
      vector[N] lmuN; //help linear pred normal skin
      vector[N] lmuP; //help linear pred pso skin
      /* real esum; // reduce computation inverse logit (la0 + bj) */
      
      profile("priors") {
        //priors
        theta ~ gamma(1, 0.1); //  based on stan code example
        phi ~ gamma(1, 0.1);
        anorm ~ normal(6, 4); // mean expression normal skin, stan normal is mean and sd
        apso ~ normal(6, 4); // mean expression pso skin
        for(i in 1:(K-1)) {
          betas[i] ~ cauchy(0, 2.5);//prior for the covariates slopes following Gelman 2008
        }
      
        // allelic imbalance priors
        for(i in 1:L) {
          rai0[i] ~ normal(ai0[i], sdai0[i]);
        }

        // mixture of gaussians for ba and bd:
        for(i in 1:k) {
          lpsa[i] = normal_lpdf(ba | aveP[i], sdP[i]) + mixP[i];
          lpsd[i] = normal_lpdf(bd | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lpsa);
        target += log_sum_exp(lpsd);
      }

      profile("likelihood") {
        // transformed parameters of no interest
        pos = 1; // to advance on NB terms
        posl = 1; // to advance on ASE terms
        ase = rep_vector(0, Max);  // initialize ase vector to 0s to collect ase termns for each hap pair compatible with Gi=g */

        betasN = append_row(anorm, betas); //betas for normal inds
        betasP = append_row(apso, betas); // betas for pso inds
        lmuN = cov[, 2:cols(cov)] * betasN; // will be used for normal inds (based on indicator)
        lmuP = cov[, 2:cols(cov)] * betasP; // for pso inds
        
        esum0 = inv_logit(rai0);
        
        for (i in 1:N) { // lmu for each individual
          // check skin first
          if (I[i] == 1) { // psoriasis

            for (r in pos:(pos+sNB[i]-1)){ // then genotype

              lmu[r] = lmuP[i]; // G = 0
              lmu[r] = abs_gNB[r] == 1 ? lmu[r] + log1p(exp(bp)) - log(2) : lmu[r];
              lmu[r] = gNB[r] == 2 ? lmu[r] + bp : lmu[r];
              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                  esum = inv_logit(rai0[posl] + bp);
                  p = gase[posl]==1 ? esum: esum0[posl];
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);
                target +=  log_sum_exp(ltmp[r], sAse);
              }
            }
            if(ASEi[i,1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
            
          } else {
            for (r in pos:(pos+sNB[i]-1)) { //normal skin
              
              lmu[r] = lmuN[i]; // G = 0
              lmu[r] = fabs(gNB[r])==1 ? lmu[r] + log1p(exp(bn)) - log(2) : lmu[r];
              lmu[r] = gNB[r]==2 ? lmu[r] + bn : lmu[r];
              ltmp[r] = neg_binomial_2_log_lpmf(
                Y[i] | lmu[r], phi
              ) + log_pNB[r];

              if (ASEi[i,1] == 1) {  // ASE info
          
                for (x in 1:h2g[r]) {  // look at the haps compatibles with Gi=g
                  
                  esum = inv_logit(rai0[posl] + bn);
                  p = gase[posl]==1 ? esum : esum0[posl];
                  p = gase[posl]==-1 ? 1-esum : p;  // haplotype swap
                  ase[x] = beta_binomial_lpmf(
                    n[posl] | m[ASEi[i, 2]], p * theta, (1 - p) * theta
                  ) + log_pH[posl];
                  
                  posl += 1;
                }
                sAse = log_sum_exp(ase[1:h2g[r]]);   
                target += log_sum_exp(ltmp[r], sAse);
              }
            }
            
            if (ASEi[i, 1] == 0) { // NO ASE, only NB terms for this ind
              target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
            }

            pos += sNB[i];
          }   
        }
      }
    }
