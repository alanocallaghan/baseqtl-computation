VB with GT\_nb\_ase, tolerance: 0.01
====================================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

    ## This is cmdstanr version 0.4.0.9000

    ## - Online documentation and vignettes at mc-stan.org/cmdstanr

    ## - CmdStan path set to: /home/alan/.cmdstan/cmdstan-2.28.2

    ## - Use set_cmdstan_path() to change the path

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

Comparing posteriors
====================

Without fullrank, then with.

![](md/cmdstan-vb_GT_nb_ase_0.01_files/figure-markdown_strict/plot-draws-1.png)![](md/cmdstan-vb_GT_nb_ase_0.01_files/figure-markdown_strict/plot-draws-2.png)

Convergence
===========

The fullrank algorithm seems really weird when it comes to convergence.

![](md/cmdstan-vb_GT_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-1.png)![](md/cmdstan-vb_GT_nb_ase_0.01_files/figure-markdown_strict/unnamed-chunk-4-2.png)

Timing
======

The difference in time is not crazy, especially with possible
convergence problems.

Sampling
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139970547111744</td>
<td style="text-align: right;">0.4595610</td>
<td style="text-align: right;">0.4353060</td>
<td style="text-align: right;">0.0242553</td>
<td style="text-align: right;">3671902</td>
<td style="text-align: right;">639238</td>
<td style="text-align: right;">14866</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139970547111744</td>
<td style="text-align: right;">0.0090396</td>
<td style="text-align: right;">0.0076218</td>
<td style="text-align: right;">0.0014178</td>
<td style="text-align: right;">133794</td>
<td style="text-align: right;">14866</td>
<td style="text-align: right;">14866</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Meanfield
---------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">139767126693696</td>
<td style="text-align: right;">0.9782000</td>
<td style="text-align: right;">0.9496840</td>
<td style="text-align: right;">0.0285161</td>
<td style="text-align: right;">4927897</td>
<td style="text-align: right;">857893</td>
<td style="text-align: right;">19951</td>
<td style="text-align: right;">21201</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">139767126693696</td>
<td style="text-align: right;">0.0195614</td>
<td style="text-align: right;">0.0176639</td>
<td style="text-align: right;">0.0018975</td>
<td style="text-align: right;">179559</td>
<td style="text-align: right;">19951</td>
<td style="text-align: right;">19951</td>
<td style="text-align: right;">21201</td>
</tr>
</tbody>
</table>

Fullrank
--------

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">name</th>
<th style="text-align: left;">thread_id</th>
<th style="text-align: right;">total_time</th>
<th style="text-align: right;">forward_time</th>
<th style="text-align: right;">reverse_time</th>
<th style="text-align: right;">chain_stack</th>
<th style="text-align: right;">no_chain_stack</th>
<th style="text-align: right;">autodiff_calls</th>
<th style="text-align: right;">no_autodiff_calls</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">likelihood</td>
<td style="text-align: left;">140287560968000</td>
<td style="text-align: right;">0.2129250</td>
<td style="text-align: right;">0.2069750</td>
<td style="text-align: right;">0.0059506</td>
<td style="text-align: right;">772616</td>
<td style="text-align: right;">134504</td>
<td style="text-align: right;">3128</td>
<td style="text-align: right;">4401</td>
</tr>
<tr class="even">
<td style="text-align: left;">priors</td>
<td style="text-align: left;">140287560968000</td>
<td style="text-align: right;">0.0039038</td>
<td style="text-align: right;">0.0035427</td>
<td style="text-align: right;">0.0003611</td>
<td style="text-align: right;">28152</td>
<td style="text-align: right;">3128</td>
<td style="text-align: right;">3128</td>
<td style="text-align: right;">4401</td>
</tr>
</tbody>
</table>

Model
=====

Optimised model:

    // negative and beta binomial for ASE eQTL with fixed genotypes but haplotype error accommodating complete allelic imbalance, version 2. Allows for any mixture of gaussians for bj prior (eQTL effect).
     
    data {
      int<lower=0> N; // number  individuals
      int<lower=0> A; // # of individuals with ASE
      int<lower=0> L; // length of vectors with n counts and p(H)
      int<lower=0> K; // number of covariates
      int<lower=0> k; // number of Gaussians for eQTL effect prior
      int Y[N]; // total gene counts
      int g[N]; // rnsp geno for all individuals
      int gase[A]; // genotype ASE individuals
      int m[A]; // total ase counts
      int n[L]; //n counts for ASE ind
      vector[L] pH; //p(H) for ASE ind
      int s[A]; // number of haplotypes per individual
      matrix[N, 1+K] cov;
      vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
      vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
      vector[k] mixP; // log of mixing proportions for eQTL effect prior
      simplex[k] expMixP; // log of mixing proportions for eQTL effect prior
    }

    transformed data {
      vector[L] log_pH = log(pH);
    }

    parameters {
      vector[K] betas; // regression param
      real<lower= -10, upper=10 > bj; // log fold change ASE
      real<lower=1e-5, upper=1e5> phi; //overdipersion param for neg binom
      real<lower=1e-5, upper=1e5> theta; //the overdispersion parameter for beta binomial
    }

    model {
      // include transformed parameters of no interest
      vector[A] p; // ASE proportion
      int pos; // to loop over haplotypes for each individual
      vector[L] ltmp; //  log BB likelihood
      vector[k] lps; // help for mixed gaussians

      // nb lik stuff
      real ebj = exp(bj); // avoid repeating same calculation
      real debj = ebj / (1 + ebj);
      real l1pebj = log1p(ebj) - log(2);
      vector[N] intercept = rep_vector(0, N); // the genetic effect

      profile("priors") {
        // Priors
        theta ~ gamma(1, 0.1); //  mean 10 
        phi ~ gamma(1, 0.1);  // mean 10
        betas[1] ~ normal(6, 4); // stan normal is mean and sd
        betas[2:K] ~ cauchy(0, 2.5);//prior for the slopes following Gelman 2008   
        // mixture of gaussians for bj:
        for(i in 1:k) {
          lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
        }
        target += log_sum_exp(lps);
      }

      // Likelihood
      profile("likelihood") {
        for (i in 1:N) { // log1p(exp(b_j)) - log(2) if het or bj if hom
          if (fabs(g[i]) == 1) {
            intercept[i] = l1pebj;
          }
          if (g[i] == 2) {
            intercept[i] = bj;
          }
        }
        Y ~ neg_binomial_2_log_glm(cov[, 2:cols(cov)], intercept, betas, phi);
        
        pos = 1;
        for(i in 1:A) { // ASE
          p[i] = gase[i] ==  1 ?     debj : 0.5;
          p[i] = gase[i] == -1 ? 1 - debj : p[i];  // haplotype swap
          if (s[i] == 1) {
            n[pos] ~ beta_binomial(m[i], p[i] * theta, (1 - p[i]) * theta);
          } else {
            for (r in pos:(pos+s[i]-1)) {
              ltmp[r] = beta_binomial_lpmf(
                n[r] | m[i], p[i] * theta, (1 - p[i]) * theta
              ) + log_pH[r];
              // ltmp[r] = beta_binomial_lpmf(
              //   n[r] | m[i], p[i] * theta, (1 - p[i]) * theta
              // );
            }
            // target += log_mix(pH[pos:(pos+s[i]-1)], ltmp[pos:(pos+s[i]-1)]);
            target += log_sum_exp(ltmp[pos:(pos+s[i]-1)]);
          }
          pos = pos + s[i];
        }
      }
    }
