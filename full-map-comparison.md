    ## ℹ Loading baseqtl

GT
==

Posteriors
----------

![](full-map-comparison_files/figure-markdown_strict/GT-plot-1.png)![](full-map-comparison_files/figure-markdown_strict/GT-plot-2.png)

Confusion matrix
----------------

    ##      MAP
    ## HMC   yes no
    ##   yes   8  0
    ##   no    1  0

Timing
------

### Sampling

    ##    user  system elapsed 
    ##  65.843   1.516  17.082

### MAP

    ##    user  system elapsed 
    ##   4.829   3.239   2.870

No GT
=====

Posteriors
----------

![](full-map-comparison_files/figure-markdown_strict/noGT-plot-1.png)![](full-map-comparison_files/figure-markdown_strict/noGT-plot-2.png)

Confusion matrix
----------------

    ##      MAP
    ## HMC   yes no
    ##   yes   2  0
    ##   no    0  0

Timing
------

### Sampling

    ##    user  system elapsed 
    ##  34.917   0.998  29.195

### MAP

    ##    user  system elapsed 
    ##   3.431   1.597   2.282

GT, paired
==========

Posteriors
----------

Confusion matrices
------------------

### Add

### Diff

### T1

### T1

Timing
------

### Sampling

### MAP

No GT, paired
=============

Posteriors
==========

![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-1.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-2.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-3.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-4.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-5.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-6.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-7.png)![](full-map-comparison_files/figure-markdown_strict/noGT-paired-plot-8.png)

Confusion matrices
------------------

### Add

    ##      MAP
    ## HMC   yes no
    ##   yes   8  1
    ##   no    0  0

### Diff

    ##      MAP
    ## HMC   yes no
    ##   yes   7  0
    ##   no    1  1

### Pso

    ##      MAP
    ## HMC   yes no
    ##   yes   0  0
    ##   no    0  0

### Norm

    ##      MAP
    ## HMC   yes no
    ##   yes   0  0
    ##   no    0  0

Timing
------

### Sampling

    ##     user   system  elapsed 
    ## 1185.839    5.352  612.444

### MAP

    ##    user  system elapsed 
    ##  30.795   7.185   9.992
