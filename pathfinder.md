VB with GT\_nb, tolerance: 1e-3
===============================

It’s time to test out variational inference. Doing this with baseqtl is
next, but I want to see what the posterior estimates for all of the
parameters are like, and if there’s any merit to using fullrank over
meanfield.

I’ll fit one model with `data` (this is just a dump of one of the
example genes) using the (mostly) unmodified model for the genotypes
case:

    ## Warning in optimx.run(par, optcfg$ufn, optcfg$ugr, optcfg$uhess, lower, : Hessian is reported non-symmetric with asymmetry ratio
    ## 6.43988506654005e-08

    ## Warning: Hessian forced symmetric

Comparing posteriors
====================

Pairwise log densities with Pathfinder and HMC.

![](pathfinder_files/figure-markdown_strict/plot-draws-1.png)
